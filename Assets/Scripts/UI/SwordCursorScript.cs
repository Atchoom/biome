﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwordCursorScript : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    [SerializeField]
    GameObject swordCursor;

    GameObject sword;

    bool alreadySpawned = false;

    // Start is called before the first frame update
   // Start is called before the first frame update
    void Start()
    {
        if(sword == null)
        {
            sword = this.gameObject.transform.Find("swordcursorprefab").gameObject;
            //sword.gameObject.transform.localPosition.x += 6;
            
            sword.SetActive(false);
        }
    }

    void Awake()
    {
        if(sword == null)
        {
            sword = this.gameObject.transform.Find("swordcursorprefab").gameObject;
            sword.SetActive(false);
        }
    }

    public void OnSelect(BaseEventData data)
    {
        sword.SetActive(true);
    }

    public void OnDeselect(BaseEventData data)
    {
        sword.SetActive(false);
    }
    
    public void OnDisable()
    {
        if (sword == null)
            return;
        sword.SetActive(false);
    }

    public void OnEnable()
    {
        if (EventSystem.current == null)
            return;
        if(gameObject == EventSystem.current.currentSelectedGameObject)
        {
            sword.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*if(gameObject == EventSystem.current.currentSelectedGameObject && !alreadySpawned)
        {
            alreadySpawned = true;
            sword.SetActive(true);
        }
        
        if(gameObject != EventSystem.current.currentSelectedGameObject && alreadySpawned )
        {
            sword.SetActive(false);
            alreadySpawned = false;
        }*/
    }
}
