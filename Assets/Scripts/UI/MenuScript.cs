using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    // the whole UICanvas object
    [SerializeField]
    GameObject canvas;

    // menu for adjusting parameters, like sound
    [SerializeField]
    GameObject optionsSubMenu;

    // first button need to be selectable
    [SerializeField]
    GameObject backButton;

    [SerializeField]
    GameObject optionsButton;

    /*events*/
    public event EventHandler<FOVArgs> FOVEvent; /* field of view */
    public event EventHandler<RestartArgs> RestartEvent;
    public event EventHandler<QuitArgs> QuitEvent;
    public event EventHandler<SoundArgs> SoundEvent; /* volume */
    public event EventHandler<SensitivityArgs> SensitivityEvent; /* mouse sensitivity */
    public event EventHandler<DrawArgs> DrawEvent; /* draw distance */


    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        RestartEvent += GameManager.Instance.OnRestart;
        optionsSubMenu.SetActive(false);
    }

    /**
    * Simply turn off the menu to return to the game.
    */
    public void Resume()
    {
        optionsSubMenu.SetActive(false);
        canvas.GetComponent<UIScript>().TurnOffMenu();
        Time.timeScale = 1;
        Cursor.visible = false;
    }

    /**
    * Game reset.
    */
    public void NewStart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Reset the player, but not the environment
    /// </summary>
    public void Restart()
    {
        Resume();
        RestartEvent?.Invoke(this, new RestartArgs(this));
    }

    public void Quit()
    {
        QuitEvent?.Invoke(this, new QuitArgs(this));
        //Debug.Log("! not implemented! Quit the game");
    }

    public void Options()
    {
        gameObject.SetActive(false);
        optionsSubMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(backButton);
    }
    //////Submenu functions/////////
    public void ReturnFromSubMenu()
    {
        gameObject.SetActive(true);
        optionsSubMenu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(optionsButton);
    }

    public void setSound(float sound)
    {
        SoundEvent?.Invoke(this, new SoundArgs(this,sound));
        //Debug.Log("! not implemented! Set the sound to "+sound);
    }

    public void setFOV(float fov)
    {
        FOVEvent?.Invoke(this, new FOVArgs(this,fov));

        //Debug.Log("! not implemented! field of view set to "+fov);
    }

    public void setMouseSensitivity(float sensitivity)
    {
        SensitivityEvent?.Invoke(this, new SensitivityArgs(this,sensitivity));
        //Debug.Log("! not implemented! mouse sensitivity set to "+ sensitivity);
    }

    public void setDrawDistance(float distance)
    {
        Debug.Log("! not implemented! draw distance set to" +distance);
    }
}
