using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/**
*
*   Main scrpt for UI. Also manages I/O currently :/
*   such as "M" key to pull up menu.
*
*/

public class UIScript : MonoBehaviour
{
    [SerializeField]
    GameObject loading_group;

    [SerializeField]
    GameObject overlay_group;

    // health and currency are children of overlay_group
    [SerializeField]
    GameObject health_group;

    [SerializeField]
    GameObject currency_group;

    [SerializeField]
    GameObject menu_group;

    [SerializeField]
    GameObject sub_menu;

    [SerializeField]
    GameObject lose_group;

    [SerializeField]
    GameObject win_group;

    [SerializeField]
    GameObject title;

    [SerializeField]
    GameObject black_background;

    [SerializeField]
    GameObject hint_group;

    [SerializeField]
    GameObject returnButton;

    [SerializeField]
    GameObject restartButton;

    int heart_count = 10;

    [SerializeField]
    string[] hints = {"Flèches/ZQSD: Mouvements","Espace: Esquiver","Clic gauche: Attaque","E: Lâcher/Récupérer l'épée","M: Menu",""};

    [SerializeField] string loadingText = "Chargement...";


    // Hide loading screen so it's less obstructive
    [SerializeField]
    bool showTitleScreen = false;

    bool menuOn = false;

    bool continueLoadScreen = false;

    int currency = 0;

    private GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.Instance;
        if (GameObject.Find("win_group") != null)
            win_group = GameObject.Find("win_group");
        SetHintText("");
        // hide hearts at start of game
        foreach (Transform heart in health_group.transform)
        {
            Color currColor = heart.gameObject.GetComponent<RawImage>().color;
            currColor.a = 0.0f;
            heart.gameObject.GetComponent<RawImage>().color = currColor;
        }

        CleanUI();
        SetHeartCount(heart_count);
        if(showTitleScreen)
        {
            title.SetActive(true);
            StartLoadingScreen();
            //StartCoroutine("RunLoadingScreen"); // delay hearts fade in
        }
        else
        {
            StartCoroutine("HeartFadeInCoroutine"); // fade in hearts immediately
        }


    }

    // Wait for Player to enter input and Hide BIOME title screen
    // ! fadeing in hints / hearts at this time is messy
    public IEnumerator WaitForInput()
    {
        title.SetActive(true);

        bool input = false;
        while(!input)
        {
            if (gameManager.DebugInput != null)
            {
                input = Input.anyKey && !Input.GetKey(gameManager.DebugInput.TogglePauseKey)
                                     && !Input.GetKey(gameManager.DebugInput.HideLogChat)
                                     && !Input.GetKey(gameManager.DebugInput.Invincibility);
            }
            else
            {
                input = Input.anyKey;
            }

            yield return new WaitForEndOfFrame();
        }
        StartCoroutine("FadeOutTitleScreen");
        StartCoroutine("HeartFadeInCoroutine");
        StartCoroutine("CycleHints");
    }


    public void setContinuLoadScreen(bool cont)
    {
        this.continueLoadScreen = cont;
    }

    // Fade Out BIOME title screen at the beginning of the game.
    public IEnumerator FadeOutTitleScreen()
    {
        float opacity = 184.0f/255.0f;
        while(opacity > 0.0f)
        {
            opacity -= 0.01f;
            Color currColor = title.gameObject.GetComponent<Text>().color;
            currColor.a = opacity;
            title.gameObject.GetComponent<Text>().color = currColor;
            yield return new WaitForSeconds(0.05f * Time.deltaTime);
        }
        title.SetActive(false);
    }

    // Simulates Loading
    public IEnumerator RunLoadingScreen()
    {
        black_background.SetActive(true);
        Color c = black_background.GetComponent<RawImage>().color;
        c.a = 1.0f;
        black_background.GetComponent<RawImage>().color = c;
        loading_group.SetActive(true);
        while(continueLoadScreen)
        {
            yield return new WaitForSeconds(2.0f);
        }
        SetLoadingText("Chargement...");
        black_background.SetActive(false);
        loading_group.SetActive(false);
    }

    /// <summary>
    /// Summons loading screen (black background, title and loading text.
    /// </summary>
    private void StartLoadingScreen()
    {
        black_background.SetActive(true);
        Color c = black_background.GetComponent<RawImage>().color;
        c.a = 1.0f;
        black_background.GetComponent<RawImage>().color = c;
        loading_group.SetActive(true);
        SetLoadingText(loadingText);
    }

    /// <summary>
    /// This event is sent by the heightmap when the initial generation is complete
    /// </summary>
    /// <param name="sender">Event sender, is not used</param>
    /// <param name="args">EventArgs, is not used here</param>
    public void OnFirstGenerationDone(object sender, EventArgs args)
    {
        black_background.SetActive(false);
        loading_group.SetActive(false);
        StartCoroutine("WaitForInput");
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.M))
        {
            CleanUI();
            menuOn = !menuOn;
            if(menuOn)
            {
                Time.timeScale = 0;
                Cursor.visible = true;
                Color c = black_background.GetComponent<RawImage>().color;
                c.a = 0.02f;
                black_background.GetComponent<RawImage>().color = c;
                black_background.SetActive(true);
                menu_group.SetActive(true);
                EventSystem.current.SetSelectedGameObject(returnButton);
            }
            else
            {

                Time.timeScale = 1;
                Cursor.visible = false;
            }
        }
    }

    public void TurnOffMenu()
    {
        CleanUI();
        EventSystem.current.SetSelectedGameObject(returnButton);
        sub_menu.SetActive(false);
        menu_group.SetActive(false);
        menuOn = false;
        Time.timeScale = 1;
        Cursor.visible = false;
    }

    // Deactivate All UI elements then reactivate OVERLAY
    public void CleanUI()
    {
        foreach (Transform group in gameObject.transform)
        {
            group.gameObject.SetActive(false);
        }
        overlay_group.SetActive(true);
    }

    public void ShowLoseGroup()
    {
        CleanUI();
        Cursor.visible = true;
        Color c = black_background.GetComponent<RawImage>().color;
        c.a = 0.3f;
        black_background.GetComponent<RawImage>().color = c;
        black_background.SetActive(true);
        lose_group.SetActive(true);
        EventSystem.current.SetSelectedGameObject(restartButton);
    }

    public IEnumerator HeartFadeInCoroutine()
    {
        float opacity = 0.0f;
        while(opacity < 1.0f)
        {
            opacity += 0.05f;
            foreach (Transform heart in health_group.transform)
            {
                Color currColor = heart.gameObject.GetComponent<RawImage>().color;
                currColor.a = opacity;
                heart.gameObject.GetComponent<RawImage>().color = currColor;
            }
            yield return new WaitForSeconds(0.2f * Time.deltaTime);
        }
    }


    public IEnumerator CycleHints()
    {
        yield return new WaitForSeconds(5.0f * Time.deltaTime);
        int i = 0;
        SetHintText(hints[i]);
        bool nextlock = true;
        while(nextlock)
        {
            //if(Input.GetKey(KeyCode.UpArrow)) { nextlock = false; }
            if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0 || Mathf.Abs(Input.GetAxis("Vertical")) > 0)
            {
                nextlock = false;
            }
            yield return new WaitForFixedUpdate();
        }
        i++;
        SetHintText(hints[i]);
        nextlock = true;
        while(nextlock)
        {
            if(Input.GetKey(KeyCode.Space)) { nextlock = false; }
            yield return new WaitForFixedUpdate();
        }
        i++;
        SetHintText(hints[i]);
        nextlock = true;
        while(nextlock)
        {
            if(Input.GetKey(KeyCode.Mouse0)) { nextlock = false; } // mouse click
            yield return new WaitForFixedUpdate();
        }
        i++;
        SetHintText(hints[i]);
        nextlock = true;
        while(nextlock)
        {
            if(Input.GetKey(KeyCode.E)) { nextlock = false; }
            yield return new WaitForFixedUpdate();
        }
        i++;
        SetHintText(hints[i]);
        nextlock = true;
        while(nextlock)
        {
            if(Input.GetKey(KeyCode.M)) { nextlock = false; }
            yield return new WaitForFixedUpdate();
        }
        i++;
        SetHintText(hints[i]);
    }

    public void SetHintText(string t)
    {
        hint_group.GetComponentInChildren<Text>().text = t;
    }

    public void SetCurrency(int c)
    {
        currency_group.GetComponent<Text>().text = ""+c;
    }

    public void RemoveHeart(int healthLeft)
    {
        SetHeartCount(heart_count - 1);
    }

    public void OnPlayerDamages(object sender, CharacterDamagesEventArgs args)
    {
        RemoveHeart(args.healthLeft);
    }

    public void OnPlayerDeath(object sender, CharacterDeathEventArgs args)
    {
        ShowLoseGroup();
    }

    /****
    *
    *   Other events that would be good:
    *
    *   win event, player is too far from sword (notify them they are too far?)
    *
    *****/

   public void  SetHeartCount(int count)
   {
        this.heart_count = count;
        int i = 0;
        foreach (Transform heart in health_group.transform)
        {
            i++;
            if(i > heart_count) { heart.gameObject.SetActive(false); }
            else {  heart.gameObject.SetActive(true); }
        }
   }

    public void SetLoadingText(string text)
    {
        loading_group.transform.Find("loading_text").gameObject.GetComponent<Text>().text = text;
    }

    public GameObject getMenuGroup()
    {
        return this.menu_group;
    }

    public void OnPlayerHealthChangeEvent(object sender, PlayerHealthChangeArgs args)
    {
        SetHeartCount(args.totalHealth/10);
    }

    public void OnPlayerWin(object sender, EventArgs args)
    {
        win_group.SetActive(true);
        Cursor.visible = true;
        EventSystem.current.SetSelectedGameObject(restartButton);
    }
}
