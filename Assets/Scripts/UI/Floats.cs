﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
*
*       Make the UI sword cursor float a little
*
*/

public class Floats : MonoBehaviour
{
    Vector3 initialpos;

    [SerializeField]
    float floatspeed = 30.0f;

    int direction;

    // Start is called before the first frame update
    void Start()
    {
        initialpos = gameObject.transform.localPosition;
        direction = 1;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x+floatspeed*Time.deltaTime*direction,
            gameObject.transform.localPosition.y,
            gameObject.transform.localPosition.z);

        if(gameObject.transform.localPosition.x > initialpos.x+6.0f 
            || gameObject.transform.localPosition.x < initialpos.x-6.0f )
        {
            direction = direction * -1;
        }
    }
}
