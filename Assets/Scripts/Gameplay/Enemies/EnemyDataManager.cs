﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Struct containing all the informations of an Enemy. This data is sent to the DataManager of the GameManager at the destruction of the enemy.
/// </summary>
[Serializable]
public struct EnemyData
{
    public EnemyID id;
    public int biomeID;
    public float attackDamage;
    public bool hasBeenSeen;
    public bool hasAggroPlayer;
    public bool hasFoughtPlayer;
    public float damageDealt;
    public float damageReceived;
    public int numberOfAttack;

    public EnemyData(EnemyID id, int biome, int attackDamage, bool seen = false, bool aggro = false, bool fought = false, float dealt = 0, float received = 0, int att = 0)
    {
        biomeID = biome;
        this.id = id;
        this.attackDamage = attackDamage;
        hasBeenSeen = seen;
        hasAggroPlayer = aggro;
        hasFoughtPlayer = fought;
        damageDealt = dealt;
        damageReceived = received;
        numberOfAttack = 0;
    }
}

public class EnemyDataManager : MonoBehaviour
{
    //The "generation" of this enemy.
    public int numberOfAdaptation = 0;
    public EnemyData data;

    private EnemyController enemyController;
    private Renderer rend;

    /// <summary>
    /// The list of all the movement type which are considered as Aggressive movement.
    /// </summary>
    public MovementType[] AggroMovement;
    /// <summary>
    /// The list of all the movement type which are considered as Fight movement.
    /// </summary>
    public MovementType[] CombatMovement;
    // Start is called before the first frame update
    void Start()
    {
        enemyController = GetComponent<EnemyController>();
        enemyController.SwitchMoveActionEvent += OnSwitchMoveAction;
        rend = GetComponentInChildren<Renderer>();
        data = new EnemyData();
        data.attackDamage = enemyController.DamageDealt;
    }

    // Update is called once per frame
    void Update()
    {
        //Check if the enemy has been seen by the player.
        if(!data.hasBeenSeen)
        {
            data.hasBeenSeen = rend.isVisible;
        }
        if(data.hasBeenSeen && data.hasAggroPlayer && data.hasFoughtPlayer)
            enemyController.SwitchMoveActionEvent -= OnSwitchMoveAction;
    }

    /// <summary>
    /// Check if the MovementType passed as argument is an Aggressive movement.
    /// </summary>
    /// <param name="type"> The MovementType to check</param>
    /// <returns></returns>
    private bool CheckIfAggroMovement(MovementType type)
    {
        foreach(MovementType i in AggroMovement)
        {
            if (type == i)
                return true;
        }
        return false;
    }

    /// <summary>
    /// Check if the MovementType passed as argument is an Fighting movement.
    /// </summary>
    /// <param name="type"> The MovementType to check</param>
    /// <returns></returns>
    private bool CheckIfCombatMovement(MovementType type)
    {
        foreach (MovementType i in CombatMovement)
        {
            if (type == i)
                return true;
        }
        return false;
    }

    /// <summary>
    /// Handler of EnemyDataManager for SwitchMoveAction event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private void OnSwitchMoveAction(object sender, SwitchMoveActionEventArgs args)
    {
        //Check if the new movement is an AggroMovement
        if(CheckIfAggroMovement(args.CurrentMoveAction.moveType))
        {
            data.hasAggroPlayer = true;
        }
        //Check if the new movement is a CombatMovement
        if (CheckIfCombatMovement(args.CurrentMoveAction.moveType))
        {
            data.hasFoughtPlayer = true;
        }
    }

    private void OnDestroy()
    {
        if (enemyController != null)
            enemyController.SwitchMoveActionEvent -= OnSwitchMoveAction;
    }
}
