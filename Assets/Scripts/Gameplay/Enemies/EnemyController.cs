﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// All the movements an enemy can have.
/// </summary>
public enum MovementType
{
    MoveFromPlayer,
    MoveTowardsPlayer,
    MoveLeft,
    MoveRight,
    MoveForward,
    MoveBackward,
    RandomMove,
    Stop,
    Attack
}

/// <summary>
/// All the condition that can be used between two state of an enemy in his movement pattern.
/// </summary>
public enum ActionCondition
{
    None,
    AfterAttack,
    PlayerLessThan,
    PlayerMoreThan,
    DurationMoreThan,
    DetectPlayer
}

/// <summary>
/// A condition between two state of the enemy's pattern.
/// </summary>
[Serializable]
public struct MoveCondition
{
    /// <summary>
    /// The type of condition
    /// </summary>
    [SerializeField] public ActionCondition condition;
    /// <summary>
    /// The value of the condition. It can be useless for several types of condition.
    /// </summary>
    [SerializeField] public float conditionValue;
    /// <summary>
    /// The value of the next state of the enemy if the condition is verified.
    /// </summary>
    [SerializeField] public int conditionTrue;
    /// <summary>
    /// The value of the next state of the enemy if the condition isn't verified.
    /// </summary>
    [SerializeField] public int conditionFalse;
}

/// <summary>
/// A moveAction which represent a state of the enemy's move Pattern.
/// </summary>
[Serializable]
public struct MoveAction
{
    /// <summary>
    /// The type of movement of this state.
    /// </summary>
    [SerializeField] public MovementType moveType;
    /// <summary>
    /// The duration of this state. It may be unused.
    /// </summary>
    [SerializeField] public float duration;
    /// <summary>
    /// The speed factor of the animation associated to this state.
    /// </summary>
    [SerializeField] public float speedCoef;
    /// <summary>
    /// The list of the conditions for leaving this state.
    /// </summary>
    [SerializeField] public MoveCondition[] conditions;
}

public class EnemyController : CharacterController
{
    [Header("EnemyController")]
    /// <summary>
    /// The id of the enemy.
    /// </summary>
    public EnemyID id;
    /// <summary>
    /// The biome's id of the enemy.
    /// </summary>
    public int biome;
    /// <summary>
    /// All the states of the enemy's movement.
    /// </summary>
    [SerializeField] private MoveAction[] movePattern;
    [Header("Enemy parameters")]
    /// <summary>
    /// Rotation speed in radian by second
    /// </summary>
    [SerializeField] private float maxRotationSpeed = 6;
    public float MaxRotationSpeed
    {
        get => maxRotationSpeed;
        set => maxRotationSpeed = value;
    }
    /// <summary>
    /// The radius of detection of the enemy for the player.
    /// </summary>
    [SerializeField] private float detectRadius;
    private float currentMoveDuration;
    /// <summary>
    /// When the attack deals damage in the animation of attack. The value has to be between 0 and 1.
    /// </summary>
    [SerializeField] private float whenAttackDealsDamageCoef = 0.5f;
    /// <summary>
    /// The timer for looking at the attack damage.
    /// </summary>
    private float whenAttackDealsDamageTime = 0;
    private bool isAttacking = false;
    private bool hasBeenHit = false;
    private bool isStunned = false;
    [SerializeField] private float invincibilityDurationAfterHit = 2;
    /// <summary>
    /// The timer for looking at invincibility after hit.
    /// </summary>
    private float timerWhenHit = 0;
    private bool inRandomMove = false;
    /// <summary>
    /// The timer for looking at randomMove state.
    /// </summary>
    private float randomTimer = 0;
    /// <summary>
    /// The duration of a random move before a change of direction.
    /// </summary>
    [SerializeField] private float randomMoveDuration = 5;
    [Header("Debug")]
    /// <summary>
    /// A boolean for debug stop
    /// </summary>
    [SerializeField] private bool stopEnemy = false;
    public bool StopEnemy
    {
        get => stopEnemy;
        set => stopEnemy = value;
    }
    [SerializeField] private int currentMoveActionIndex;
    public int CurrentMoveActionIndex
    {
        get => currentMoveActionIndex;
        set => currentMoveActionIndex = value;
    }


    /// <summary>
    /// The enemy's DataManager.
    /// </summary>
    private EnemyDataManager enemyDataManager;

    private GameObject debugTextAbove;
    private TextMesh debugTextAboveMesh;

    /** Events */
    public event EventHandler<CharacterDeathEventArgs> EnemyDeathEvent;
    public event EventHandler<CharacterDamagesEventArgs> EnemyDamagesEvent;
    public event EventHandler<SwitchMoveActionEventArgs> SwitchMoveActionEvent;
    public event EventHandler<SendEnemyDataEventArgs> sendDataEvent;

    protected override void Awake()
    {
        SetUp();
        currentMoveActionIndex = 0;
        currentMoveDuration = 0;

        enemyDataManager = GetComponent<EnemyDataManager>();
    }

    protected override void Start()
    {
        base.Start();
        debugTextAbove = transform.Find("DebugTextAbove").gameObject;
        if (debugTextAbove != null)
        {
            debugTextAboveMesh = debugTextAbove.GetComponent<TextMesh>();
            SetDebugText();
        }

        if (gameManager.IsDebugActive)
        {
            if (debugTextAbove != null)
                debugTextAbove.SetActive(true);
            SetDebugText();
        }
        else
        {
            if (debugTextAbove != null)
                debugTextAbove.SetActive(false);
        }

        //Event handlers
        gameManager.ToggleEnemyDebugUI += OnToggleDebugText;
        EnemyDeathEvent += gameManager.OnEnemyDeath;
        EnemyDamagesEvent += gameManager.OnEnemyDamages;
        SwitchMoveActionEvent += gameManager.OnSwitchMoveAction;
        
        //Send initial state to AnimatorController.
        SwitchMoveActionEvent?.Invoke(this,
    new SwitchMoveActionEventArgs(movePattern[currentMoveActionIndex], isStunned));
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //Debug Stop
        if (stopEnemy)
            return;
        lastAttackTime += Time.deltaTime;
        //Update special status like stun.
        UpdateStatus();
        currentMoveDuration += Time.deltaTime;
        //Check hit timer
        if(hasBeenHit)
        {
            timerWhenHit += Time.deltaTime;
            if(timerWhenHit >= invincibilityDurationAfterHit)
            {
                hasBeenHit = false;
                timerWhenHit = 0;
            }
        }

        SetColor();

        //Update movement
        if (canMove)
        {
            Move();
        }
        if(movePattern.Length > 0 && movePattern[currentMoveActionIndex].moveType != MovementType.Attack)
        {
            hasCharacterAttacked = false;
        }

        // we rotate the text displayed above towards the player
        if (debugTextAbove != null && debugTextAbove.activeSelf)
        {
            Vector3 playerPosition = gameManager.PlayerController.gameObject.transform.position;
            debugTextAbove.transform.rotation = Quaternion.LookRotation(transform.position - playerPosition);

        }
    }

    /// <summary>
    /// Increase Enemy's speed
    /// </summary>
    /// <param name="add"> The speed to add to current speed </param>
    public void addToSpeed(int add)
    {
        maxSpeed += add;
        if(maxSpeed < 1) { MaxSpeed = 1; }
    }

    /// <summary>
    /// Increase Enemy's attack speed
    /// </summary>
    /// <param name="add"> The attack speed to add to current attack speed </param>
    public void addToAttackSpeed(float add)
    {
        AttackSpeed += add;
        if(AttackSpeed < 0.2f ) { AttackSpeed = 0.2f; }
    }

    /// <summary>
    /// Increase Enemy's rotation speed
    /// </summary>
    /// <param name="add"> The speed to add to current rotation speed </param>
    public void addToRotationSpeed(float add)
    {
        MaxRotationSpeed += add;
        if(MaxRotationSpeed < 0.2f ) { MaxRotationSpeed = 0.2f; }
    }

    /// <summary>
    /// Increase Enemy's range of detection.
    /// </summary>
    /// <param name="add"> The value to add to current range </param>
    public void addToRange(float add)
    {
        detectRadius += add;
        if(detectRadius < 4) { detectRadius = 4; }
    }

    /// <summary>
    /// Function to send SwitchMoveAction Event.
    /// </summary>
    public void SendEvent()
    {
        SwitchMoveActionEvent?.Invoke(this,
new SwitchMoveActionEventArgs(movePattern[currentMoveActionIndex], isStunned));
    }

    /// <summary>
    /// Move function. It's the main function for moving Ennemy.
    /// </summary>
    protected void Move()
    {
        // we check if the Enemy can move
        foreach (var statusEffect in statusEffects)
        {
            if (statusEffect.effect == StatusList.Stun)
            {
                if (!isStunned)
                {
                    isStunned = true;
                    currentMoveActionIndex = 0;
                    currentMoveDuration = 0f;
                    SwitchMoveActionEvent?.Invoke(this,
                        new SwitchMoveActionEventArgs(movePattern[currentMoveActionIndex],isStunned));
                }
                return;
            }
        }

        if(isStunned) isStunned = false;

        //Check if a condition is fullfiled to change of state.
        int actionIndex = SwitchMoveAction();

        if (actionIndex != -1)
        {
            if (actionIndex < 0 || actionIndex > movePattern.Length)
            {
                Debug.LogError("Action index out of range : " + actionIndex);
            }
            else
            {
                // the move state has changed
                currentMoveActionIndex = actionIndex;
                currentMoveDuration = 0f;
                //If movement is Attack, the StartAttack() function will launch the event
                if (movePattern[currentMoveActionIndex].moveType != MovementType.Attack)
                    SwitchMoveActionEvent?.Invoke(this,
                    new SwitchMoveActionEventArgs(movePattern[currentMoveActionIndex], isStunned));
            }
        }
        //Apply current state's Move action
        switch (movePattern[currentMoveActionIndex].moveType)
        {
            case MovementType.Stop: break;
            case MovementType.MoveTowardsPlayer: MoveTowardsPlayer(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                break;
            case MovementType.MoveFromPlayer: MoveFromPlayer(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                break;
            case MovementType.MoveForward: MoveForward(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                break;
            case MovementType.MoveBackward: MoveBackward(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                break;
            case MovementType.MoveLeft: MoveLeft(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                break;
            case MovementType.MoveRight: MoveRight(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                break;
            case MovementType.Attack: StartAttack();
                break;
            case MovementType.RandomMove: if (!inRandomMove) StartMoveRandom(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                else MoveRandom(movePattern[currentMoveActionIndex].speedCoef * MaxSpeed);
                break;
            default: Debug.LogError("MoveType unknown");
                break;
        }
    }

    /// <summary>
    /// Move the enemy away from the player.
    /// </summary>
    /// <param name="speed"> The speed of the movement</param>
    private void MoveFromPlayer(float speed)
    {
        GameObject player = gameManager.PlayerController.gameObject;
        Vector3 directionFromPlayer = transform.position - player.transform.position;
        directionFromPlayer.Normalize();
        Transform thisTransform = transform;
        thisTransform.forward = Vector3.RotateTowards(thisTransform.forward,
            new Vector3(directionFromPlayer.x, 0, directionFromPlayer.z),
            maxRotationSpeed * Time.deltaTime, 1).normalized;
        thisTransform.position += speed * Time.deltaTime * thisTransform.forward;
    }

    /// <summary>
    /// Move the enemy towards the player.
    /// </summary>
    /// <param name="speed"> The speed of the movement</param>
    private void MoveTowardsPlayer(float speed)
    {
        GameObject player = gameManager.PlayerController.gameObject;
        Vector3 directionToPlayer = player.transform.position - transform.position;
        directionToPlayer.Normalize();
        Transform thisTransform = transform;
        thisTransform.forward = Vector3.RotateTowards(thisTransform.forward,
            new Vector3(directionToPlayer.x, 0, directionToPlayer.z),
            maxRotationSpeed * Time.deltaTime, 1).normalized;
        thisTransform.position += speed * Time.deltaTime * thisTransform.forward;
    }

    /// <summary>
    /// Move the enemy to the left.
    /// </summary>
    /// <param name="speed"> The movement's speed</param>
    private void MoveLeft(float speed)
    {
        transform.position += speed * Time.deltaTime * (- transform.right);
    }

    /// <summary>
    /// Move the enemy to the right.
    /// </summary>
    /// <param name="speed"> The movement's speed</param>
    private void MoveRight(float speed)
    {
        transform.position += speed * Time.deltaTime * transform.right;
    }

    /// <summary>
    /// Move the enemy forward.
    /// </summary>
    /// <param name="speed"> The movement's speed</param>
    private void MoveForward(float speed)
    {
        transform.position += speed * Time.deltaTime * transform.forward;
    }

    /// <summary>
    /// Move the enemy backward.
    /// </summary>
    /// <param name="speed"> The movement's speed</param>
    private void MoveBackward(float speed)
    {
        transform.position += speed * Time.deltaTime * (- transform.forward);
    }

    /// <summary>
    /// Function which start a random move for the enemy. It starts a coroutine which rotate the enemy in a random direction.
    /// </summary>
    /// <param name="speed"> The random Move's speed</param>
    private void StartMoveRandom(float speed)
    {
        inRandomMove = true;
        Vector3 randomDirection = new Vector3(0,UnityEngine.Random.Range(-180,180),0);
        StartCoroutine(RotateObject(Quaternion.Euler(randomDirection),2f));
        transform.position += speed * Time.deltaTime * transform.forward;
    }

    /// <summary>
    /// Coroutine for smoothly rotating the enemy.
    /// </summary>
    /// <param name="rotation"> The new rotation </param>
    /// <param name="lerpTime"> The lerp factor </param>
    /// <returns></returns>
    private IEnumerator RotateObject(Quaternion rotation, float lerpTime)
    {
        float elapsedTime = 0f;

        while (elapsedTime <= lerpTime)
        {
            gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, rotation, elapsedTime / lerpTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
    /// <summary>
    /// Move the enemy forward during randomMoveDuration.
    /// </summary>
    /// <param name="speed"></param>
    private void MoveRandom(float speed)
    {
        randomTimer += Time.deltaTime;
        if(randomTimer >= randomMoveDuration)
        {
            randomTimer = 0;
            inRandomMove = false;
        }
        transform.position += speed * Time.deltaTime * transform.forward;
    }

    /// <summary>
    /// Change enemy's color with his health percentage as paramater.
    /// </summary>
    protected void SetColor()
    {
        Color lowHealthColor = new Color(1, 0, 0); // <= 0%
        Color mediumHealthColor = new Color(1, 1, 0); // > 33%
        Color highHealthColor = new Color(0, 1, 0); // > 66%
        Color fullHealthColor = new Color(0, 0, 1); // 100%

        Color shownColor;
        float percentHealth = (float)health / (float)maxHealth;
        if (percentHealth == 1)
        {
            shownColor = fullHealthColor;
        }
        else if (percentHealth > 0.66)
        {
            shownColor = highHealthColor;
        }
        else if (percentHealth > 0.33)
        {
            shownColor = mediumHealthColor;
        }
        else
        {
            shownColor = lowHealthColor;
        }
        MeshRenderer meshRenderer = this.gameObject.GetComponent<MeshRenderer>();
        if(meshRenderer != null)
        {
            meshRenderer.material.SetColor("_BaseColor", shownColor);
        }

    }

    /// <summary>
    /// Function which check each condition of the enemy's current state and send the new state index or -1.
    /// </summary>
    /// <returns></returns>
    private int SwitchMoveAction()
    {
        int returnValue = -1;
        bool conditionResult = false;
        foreach (MoveCondition condition in movePattern[currentMoveActionIndex].conditions)
        {
            switch (condition.condition)
            {
                case ActionCondition.None: break;
                case ActionCondition.AfterAttack:
                    conditionResult = hasCharacterAttacked;
                    break;
                case ActionCondition.PlayerLessThan:
                    conditionResult = (gameManager.PlayerController.transform.position - transform.position).magnitude
                        <= condition.conditionValue;
                    break;
                case ActionCondition.PlayerMoreThan:
                    conditionResult = (gameManager.PlayerController.transform.position - transform.position).magnitude
                                            >= condition.conditionValue;
                    break;
                case ActionCondition.DurationMoreThan:
                    conditionResult = currentMoveDuration >= condition.conditionValue;
                    break;
                case ActionCondition.DetectPlayer:
                    conditionResult = (gameManager.PlayerController.transform.position - transform.position).magnitude
                        <= detectRadius;
                    break;
                default:
                    Debug.LogError("Action condition not found !");
                    break;
            }

            if (conditionResult)
            {
                returnValue = condition.conditionTrue;
            }
            else
            {
                returnValue = condition.conditionFalse;
            }

            if (returnValue != -1)
                return returnValue;
        }

        return returnValue;
    }

    /// <summary>
    /// Start an attack action. It enables the damage collider and rotate the enemy towards the player.
    /// </summary>
    protected void StartAttack()
    {
        if (isAttacking)
            return;
        foreach (var statusEffect in statusEffects)
        {
            if (statusEffect.effect == StatusList.Stun)
                return;
        }
        if (lastAttackTime < attackCooldown)
            return;
        setDamageCollider(true);
        isAttacking = true;
        enemyDataManager.data.numberOfAttack++;
        GameObject player = gameManager.PlayerController.gameObject;
        transform.LookAt(player.transform);
        SwitchMoveActionEvent?.Invoke(this,new SwitchMoveActionEventArgs(movePattern[currentMoveActionIndex], isStunned));
    }

    /// <summary>
    /// End the attack action of the enemy. It disables the damage collider.
    /// </summary>
    public void EndAttack()
    {
        lastAttackTime = 0;
        setDamageCollider(false);
        isAttacking = false;
        hasCharacterAttacked = true;
    }

    /// <summary>
    /// Function called when the enemy get damaged by the player.
    /// </summary>
    /// <param name="attackDamages"> the damages deals by the player </param>
    /// <param name="attackStatuses"> the statuses's list of the attack </param>
    public override void GetDamages(int attackDamages, List<StatusEffect> attackStatuses = null)
    {
        //Check if he has already been hit by the player recently
        if (!hasBeenHit)
            hasBeenHit = true;
        else
            return;
        health -= attackDamages;
        totalDamageReceived += attackDamages;
        EnemyDamagesEvent?.Invoke(this, new CharacterDamagesEventArgs(this, attackDamages, health,
            attackStatuses));
        //Apply statuses
        if (attackStatuses != null)
        {
            foreach (var attackStatus in attackStatuses)
            {
                statusEffects.Add(attackStatus);
                //Debug.Log("Enemy stunned !");
            }
        }
        //If dead
        if (!isInvincible && health <= 0)
        {
            gameObject.SetActive(false);
            gameManager.ToggleEnemyDebugUI -= OnToggleDebugText;
            EnemyDeathEvent?.Invoke(this, new CharacterDeathEventArgs(this));
        }
        SetDebugText();
    }

    /// <summary>
    /// Function called when the player is destroyed. Send data to the DataManager.
    /// </summary>
    private void OnDestroy()
    {
        //Create data.
        EnemyData data = enemyDataManager.data;
        data.biomeID = biome;
        data.id = id;
        data.damageDealt = totalDamageDealt;
        data.damageReceived = totalDamageReceived;
        //Send event
        sendDataEvent?.Invoke(this, new SendEnemyDataEventArgs(data, enemyDataManager.numberOfAdaptation));
        EnemyDeathEvent -= gameManager.OnEnemyDeath;
        EnemyDamagesEvent -= gameManager.OnEnemyDamages;
        SwitchMoveActionEvent -= gameManager.OnSwitchMoveAction;
        if(GameManager.Instance != null)
            sendDataEvent -= gameManager.GetComponent<DataManager>().onEnemyDataReceived;
    }

    /// <summary>
    /// When and other collider enter the damage collider, it deals damage to it.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            totalDamageDealt += other.gameObject.GetComponent<PlayerController>().GetDamages(damageDealt);
            setDamageCollider(false);
        }
    }

    /// <summary>
    /// Changes the text displayed above an enemy
    /// </summary>
    private void SetDebugText()
    {
        if (debugTextAboveMesh != null)
            debugTextAboveMesh.text = "Health: " + health + "/" + maxHealth + "\n"
                                      + "Max rotation speed: " + maxRotationSpeed.ToString("F2") + " rad/s" + "\n"
                                      + "Speed: " + maxSpeed + "\n"
                                      + "Attack speed: " + attackSpeed + "\n"
                                      + "Detection range: " + detectRadius;
    }

    /// <summary>
    /// Called when toggling the text displayed above an enemy is needed
    /// </summary>
    public void OnToggleDebugText(object sender, EventArgs args)
    {
        if (debugTextAbove != null)
            debugTextAbove.SetActive(gameManager.IsDebugActive);
        if (gameManager.IsDebugActive)
            SetDebugText();
    }
}
