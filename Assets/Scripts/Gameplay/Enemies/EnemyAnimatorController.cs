﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for handling Enemy's Animator.
/// </summary>
public class EnemyAnimatorController : MonoBehaviour
{
    private Animator animator;
    private EnemyController enemyController;

    enum Direction
    {
        Right,
        Left,
        Back,
        Front,
    }

    // Start is called before the first frame update
    void Awake()
    {
        InitAnimator();
        if (enemyController != null)
        {
            enemyController.SwitchMoveActionEvent += OnUpdateState;
            enemyController.EnemyDamagesEvent += OnAttacked;
        }
            
    }

    /// <summary>
    /// On Destroy, delete animator events handler.
    /// </summary>
    private void OnDestroy()
    {
        if (enemyController != null)
        {
            enemyController.SwitchMoveActionEvent -= OnUpdateState;
            enemyController.EnemyDamagesEvent -= OnAttacked;
        }
    }

    /// <summary>
    /// Initialize class's components : the animator and the EnemyController.
    /// </summary>
    private void InitAnimator()
    {
        animator = GetComponent<Animator>();
        if (animator == null)
            Debug.Log("No Animator found");
        animator.applyRootMotion = false;

        enemyController = GetComponent<EnemyController>();
        if (enemyController == null)
            Debug.Log("EnemyAnimatorController : EnemyController not found");
    }
    
    /// <summary>
    /// Handler for SwitchMoveActionEvent
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    void OnUpdateState(object sender, SwitchMoveActionEventArgs args)
    {
        UpdateState(args.CurrentMoveAction);
    }

    /// <summary>
    /// Handler for CharacterDamagesEvent
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    void OnAttacked(object sender, CharacterDamagesEventArgs args)
    {
        setTriggerAttacked();
    }

    /// <summary>
    /// Function which update animator's state of enemy.
    /// </summary>
    /// <param name="newState"> The new State of the enemy </param>
    public void UpdateState(MoveAction newState)
    {
        //set the speed of the enemy's next animation
        float moveSpeed = newState.speedCoef * enemyController.MaxSpeed;
        //Change animation state
        switch (newState.moveType)
        {
            case MovementType.MoveFromPlayer:
                break;
            case MovementType.MoveForward:
            case MovementType.MoveTowardsPlayer:
                UpdateDirection(Direction.Front);
                break;
            case MovementType.MoveLeft:
                UpdateDirection(Direction.Left);
                break;
            case MovementType.MoveRight:
                UpdateDirection(Direction.Right);
                break;
            case MovementType.MoveBackward:
                UpdateDirection(Direction.Back);
                break;
            case MovementType.Attack:
                setTriggerAttack(enemyController.AttackSpeed);
                moveSpeed = 0f;
                break;
            case MovementType.RandomMove:
                break;
            default:
                break;
        }
        //Send speed to animator
        UpdateSpeed(moveSpeed);
    }

    /// <summary>
    /// Set speed of enemy for the animator
    /// </summary>
    /// <param name="newSpeed"></param>
    void UpdateSpeed(float newSpeed)
    {
        animator.SetFloat("speed", newSpeed);
    }

    void UpdateDirection(Direction direction)
    {

    }

    /// <summary>
    /// Set attack's trigger of Animator and change animator's speed.
    /// </summary>
    /// <param name="s"> Speed of animation </param>
    void setTriggerAttack(float s)
    {
        SetSpeedOfAnimation(s);
        animator.SetTrigger("attacking");
    }
    
    /// <summary>
    /// Set isAttacked's trigger.
    /// </summary>
    void setTriggerAttacked()
    {
        animator.SetTrigger("isAttacked");
    }

    /// <summary>
    /// Set speed of animator.
    /// </summary>
    /// <param name="speed"> The new speed of Animator. </param>
    private void SetSpeedOfAnimation(float speed)
    {
        animator.speed = speed;
    }
}
