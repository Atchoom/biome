﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachineBehaviour for handling the end of an enemy's attack.
/// </summary>
public class stopAttack : StateMachineBehaviour
{
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EnemyController enemyController = animator.gameObject.GetComponent<EnemyController>();
        if (enemyController != null)
        {
            enemyController.EndAttack();
            animator.speed = 1;
        }
    }
}
