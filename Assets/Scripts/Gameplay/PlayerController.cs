﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// The controller for the player, which manage the player life and attack.
/// </summary>
public class PlayerController : CharacterController
{
    public static PlayerController Instance { get; private set; }
    private WeaponController weaponController;
    public WeaponController WeaponController => weaponController;

    private InputController inputController;
    private Vector3 centerOfAttack;
    private List<StatusEffect> statusEffects;

    public int Health => health;
    [Header("Player Controller")]
    [SerializeField] private float stunDuration = 1;
    public float StunDuration
    {
        get => stunDuration;
        set => stunDuration = value;
    }

    [SerializeField]
    private int devise_count;
    public int Devise_count
    {
        get { return devise_count; }
        set { devise_count = value; }
    }

    public bool IsInvincible
    {
        get { return isInvincible; }
        set { isInvincible = value; }
    }


    /** Events */
    public event EventHandler<CharacterDeathEventArgs> PlayerDeathEvent;
    public event EventHandler<CharacterDamagesEventArgs> PlayerDamagesEvent;
    public event EventHandler<PlayerHealthChangeArgs> PlayerHealthChangeEvent;

    protected override void Awake()
    {
        SetUp();
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        gameManager = GameManager.Instance;
        inputController = this.gameObject.GetComponent<InputController>();
        weaponController = WeaponController.Instance;
        damageCollider = weaponController.Sword.GetComponentInChildren<CapsuleCollider>();
    }

    // Update is called once per frame
    protected void Update()
    {
        // this position may be too much forward, and thus it can create problems
        centerOfAttack = transform.position;//TODO changed Collider + gameObject.GetComponent<CapsuleCollider>().center;
        Debug.DrawRay(centerOfAttack, transform.forward);
    }

    /// <summary>
    /// Active the damage collider of the player.
    /// </summary>
    public void Attack()
    {
        setDamageCollider(true);
    }

    /// <summary>
    /// Set the player invincible or not. 
    /// </summary>
    /// <param name="isInv"></param>
    public void SetInvincible(bool isInv)
    {
        isInvincible = isInv;
        this.GetComponentInChildren<SkinnedMeshRenderer>().material.color = isInv ? Color.red : Color.white;
    }

    /// <summary>
    /// Function called when the player is damaged.
    /// </summary>
    /// <param name="attackDamages"> Damage received </param>
    /// <param name="attackStatuses"> Statuses received </param>
    /// <returns> The number of damage received </returns>
    public new float GetDamages(int attackDamages, List<StatusEffect> attackStatuses = null)
    {
        if (isInvincible)
            return 0;
        health -= attackDamages;
        totalDamageReceived += attackDamages;
        StateManager.Instance.hasBeenHit = true;
        IsInvincible = true;
        if (attackStatuses != null)
        {
            foreach (var attackStatus in attackStatuses)
            {
                statusEffects.Add(attackStatus);
            }
        }
        if (health <= 0)
        {
            PlayerDeathEvent?.Invoke(this, new CharacterDeathEventArgs(this));
        }
        PlayerDamagesEvent?.Invoke(this, new CharacterDamagesEventArgs(this, attackDamages, health,
            attackStatuses));
        return attackDamages;
    }

    /// <summary>
    /// Handle function for ModifyPlayerHealth event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public void OnModifyHealth(object sender, ModifyPlayerHealthArgs args)
    {
        if (health + args.relativeHealthAdded >= 0 && health + args.relativeHealthAdded <= maxHealth)
        {
            health += args.relativeHealthAdded;
            PlayerHealthChangeEvent?.Invoke(this, new PlayerHealthChangeArgs(args.relativeHealthAdded, health));
        }

        if (health <= 0)
            PlayerDeathEvent?.Invoke(this, new CharacterDeathEventArgs(this));
    }

    /// <summary>
    /// Heal the player of an amount of life. It can't exced his maxHealth.
    /// </summary>
    /// <param name="amount"></param>
    public void Heal(int amount)
    {
        health = Math.Min(maxHealth, health + amount);
        PlayerHealthChangeEvent?.Invoke(this, new PlayerHealthChangeArgs(amount, health));
    }

    /// <summary>
    /// Restore the player to his initial state.
    /// </summary>
    public void ResetStats()
    {
        health = maxHealth;
        devise_count = 0;
        totalDamageDealt = 0;
        totalDamageReceived = 0;
        PlayerHealthChangeEvent?.Invoke(this, new PlayerHealthChangeArgs(0, health));
        isInvincible = false;
    }

}
