﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rollingBehaviour : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        StateManager.Instance.IsRolling = true;
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.speed = 1;
        StateManager.Instance.IsRolling = false;
    }
}
