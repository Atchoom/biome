﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Behaviour pour  la gestion des animations du PickUp de l'épée.
 **/

public class PickUpSwordBehaviour : StateMachineBehaviour
{
    private WeaponController weaponController;
    private StateManager states;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("interruptedPickUp", false);
        weaponController = (WeaponController)animator.GetComponentInParent(typeof(WeaponController));
        if (weaponController == null)
        {
            Debug.Log("WeaponController not found in PickUpSwordBehaviour");
        }
        states = (StateManager)animator.GetComponentInParent(typeof(StateManager));
        if(states == null)
        {
            Debug.Log("StateManager not found in PickUpSwordBehaviour");
        } else
        {
            states.IsPickingUpSword = true;
        }
        
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        states.IsPickingUpSword = false;
        if(!animator.GetBool("interruptedPickUp"))
        {
            weaponController.TakeSword(false);
        } else
        {
            animator.SetBool("interruptedPickUp", false);
        }

    }
}
