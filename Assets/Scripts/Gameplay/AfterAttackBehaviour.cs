﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterAttackBehaviour : StateMachineBehaviour
{
    private StateManager states;

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        states = (StateManager)animator.GetComponentInParent(typeof(StateManager));
        if (states == null)
        {
            Debug.Log("StateManager not found in PickUpSwordBehaviour");
            return;
        }
        if(!states.IsPickingUpSword)
            animator.SetBool("interruptedPickUp", true);
        states.GetComponent<PlayerController>().setDamageCollider(false);
    }
}

