﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller for the point of interest. It heals the player when he is near.
/// </summary>
public class PIController : MonoBehaviour
{
    [SerializeField] private int healAmount = 5;
    [SerializeField] private float healCooldown = 5;
    private float healTimer = 0;
    private bool playerIn = false;

    // Update is called once per frame
    void Update()
    {
        healTimer += Time.deltaTime;
        if(healTimer >= healCooldown)
        {
            healTimer = 0;
            HealPlayer();
        }
    }

    private void HealPlayer()
    {
        if(playerIn)
        {
            PlayerController.Instance.Heal(healAmount);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            playerIn = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerIn = false;
        }
    }
}
