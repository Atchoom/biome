﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Classe permettant de mettre à jour le booléen canMove dans l'animator.
**/


public class canMoveUpdate : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("canMove", true);
        StateManager.Instance.IsRolling = false;
    }

}
