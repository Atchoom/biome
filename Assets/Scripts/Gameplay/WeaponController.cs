﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    // Start is called before the first frame update
    public static WeaponController Instance { get; private set; }

    [SerializeField]
    private GameObject sword;
    public GameObject Sword
    {
        get
        {
            return this.sword;
        }
        set
        {
            this.sword = value;
        }
    }

    [SerializeField]
    private GameObject hand;

    [SerializeField]
    private float swordScale;

    private InputController inputController;
    private Rigidbody rgbody;
    private Collider col;
    private StateManager stateManager;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        SetUp();
        TakeSword(false);
    }

    // Update is called once per frame
    void Update()
    {
        //A déplacer dans l'InputController, c'est temporaire
        if(Input.GetKeyDown(KeyCode.A))
        {
            sword.GetComponent<SwordClass>().StartMoving(this.transform.position + new Vector3(0,this.transform.lossyScale.y/2,0));
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            sword.GetComponent<SwordClass>().StopMoving();
        }
    }

    /// <summary>
    /// Function for getting all the components.
    /// </summary>
    private void SetUp()
    {
        inputController = this.gameObject.GetComponent<InputController>();
        stateManager = StateManager.Instance;
        rgbody = sword.GetComponent<Rigidbody>();
        col = sword.GetComponent<Collider>();
        if(inputController == null || stateManager == null || rgbody == null || col == null)
        {
            Debug.Log("Weaponcontroller's components not loaded");
        }
    }

    /// <summary>
    /// Function for taking the sword. If withDistance is true, it checks before if the sword is near the player.
    /// </summary>
    /// <param name="withDistance"> If we use a distance for taking the sword </param>
    public void TakeSword(bool withDistance)
    {
        if(sword.transform.parent == null)
        {
            if (withDistance)
                if (!SearchSword(this.transform.position,stateManager.RadiusForSwordSearch))
                    return;
            SwordToHand();
        }
    }

    /*Function which attach the sword to the player's hand*/
    /// <summary>
    /// Function which attach the sword to the player's hand
    /// </summary>
    public void SwordToHand()
    {
        sword.transform.SetParent(hand.transform, false);
        // For the old model : sword.transform.localPosition = new Vector3(0.0059f, 0.0059f, 0.0195f);
        sword.transform.localPosition = new Vector3(0.072f, -0.038f, -0.009f);
        // For the old model : sword.transform.localPosition = Vector3.zero;
        //  For the old model : sword.transform.localRotation = Quaternion.Euler(0f, 23.5f, 81f); //-32.734 98.80701 28.761
        sword.transform.localRotation = Quaternion.Euler(-32.734f, 98.80701f, 28.761f);
        var lossyScale = sword.transform.parent.lossyScale;
        Vector3 scale = new Vector3(swordScale / lossyScale.x,
            swordScale / lossyScale.y,
            swordScale / lossyScale.z);
        sword.transform.localScale = scale;
        if (rgbody == null)
            SetUp();
        rgbody.isKinematic = true;
        col.isTrigger = true;
        stateManager.HasSword = true;
        sword.GetComponent<SwordClass>().isAttached = true;
    }

    /// <summary>
    /// Drop sword to current position.
    /// </summary>
    public void DropSword()
    {
        sword.transform.parent = null;
        sword.transform.localScale = swordScale * Vector3.one;
        rgbody.isKinematic = true;
        col.isTrigger = false;
        stateManager.HasSword = false;
        sword.GetComponent<SwordClass>().isAttached = false;
        PlayerController.Instance.setDamageCollider(false);
        StickSwordIntoGround();
    }

    /// <summary>
    /// Function which search for sword around a position for a given radius
    /// </summary>
    /// <param name="centerForSearch"></param>
    /// <param name="radiusForSearch"></param>
    /// <returns></returns>
    public bool SearchSword(Vector3 centerForSearch, float radiusForSearch)
    {
        Collider[] colliders = Physics.OverlapSphere(centerForSearch,radiusForSearch);
        if(colliders.Length > 0)
        {
            for(int i = 0; i < colliders.Length; i++)
            {
                if(colliders[i].CompareTag("Sword"))
                    return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Function to stick the sword into the ground.
    /// </summary>
    private void StickSwordIntoGround()
    {
        RaycastHit hit;
        Vector3 swordRotation = sword.transform.rotation.eulerAngles;
        Vector3 swordPosition = sword.transform.position;
        sword.transform.rotation = Quaternion.Euler(90,swordRotation.y, swordRotation.z);
        LayerMask layerMask = 1 << 12;
        if(Physics.Raycast(sword.transform.position, Vector3.down, out hit, 25f,layerMask))
        {
            sword.transform.position = new Vector3(swordPosition.x, hit.point.y + 1f, swordPosition.z);
        }
    }
}
