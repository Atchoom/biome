﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/**
 * Classe InputController, qui gère tous les inputs du joueur pour le contrôle du personnage
**/

public class InputController : MonoBehaviour
{
    private float horizontal = 0;
    private float vertical = 0;
    private bool sprintPressed = false;
    private bool rollPressed = false;

    private bool attackPressed = false;
    private bool dropWeaponPressed = false;
    private bool takeWeaponPressed = false;
    private bool weaponActionPressed = false;
    private bool attractWeaponPressed = false;
    private bool pickupWeaponPressed = false;

    public bool WeaponActionPressed => weaponActionPressed;
    public bool DropWeaponPressed => dropWeaponPressed;
    public bool TakeWeaponPressed => takeWeaponPressed;
    public bool AttackPressed => attackPressed;
    public bool AttractWeaponPressed => attractWeaponPressed;
    public bool PickupWeaponPressed => pickupWeaponPressed;

    private float takeWeaponTimer = 0f;
    private float takeWeaponTimeLimit = 1.2f;
    private string takeDropWeaponInput = "WeaponDropTake";
    private float timeLimit = 5f;
    private float timeCurr = 0;

    private StateManager states;
    private CameraController cameraController;
    private CameraManager cameraManager;

    private float deltaT; // le Time.delta générale des programmes.
    /*Initialisation du composant*/
    void Start()
    {
        states = StateManager.Instance;
        if(states == null)
        {
            Debug.Log("Couldn't load StateManager in InputController");
        }

        cameraController = CameraController.singleton;
        if(cameraController == null)
        {
            Debug.Log("Can't get CameraController");
        }
        cameraController.Init(this.transform);

        cameraManager = CameraManager.Instance;
    }

    /*Recuperation des Inputs et deplacement*/
    void Update()
    {
        deltaT = Time.deltaTime;
        if (cameraManager.CurrentCamera != CameraType.DEBUG)
        {
            GetInput();
        } else
        {
            ResetInput();
        }
        //attractWeaponPressed = Input.GetKeyDown(KeyCode.F);
        dropWeaponPressed = false;
        pickupWeaponPressed = false;
        if (weaponActionPressed)
        {
            if (states.HasSword)
            { //Drop weapon
                dropWeaponPressed = true;
            } else
            {//Start taking weapon
                pickupWeaponPressed = true;
            }
        }

        UpdateStates();
    }
    //Move camera
    private void FixedUpdate()
    {
        deltaT = Time.deltaTime;
        if (cameraManager.CurrentCamera != CameraType.DEBUG)
        {
            cameraController.CameraUpdate(deltaT);
        }
    }

    /// <summary>
    /// Get all the Inputs.
    /// </summary>
    void GetInput()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        sprintPressed = Input.GetButtonDown("Sprint");
        attackPressed = !EventSystem.current.IsPointerOverGameObject() && states.HasSword && Input.GetButtonDown("Fire1");
        rollPressed = Input.GetButtonDown("Jump");
        weaponActionPressed = Input.GetButtonDown(takeDropWeaponInput);
        attractWeaponPressed = Input.GetKeyDown(KeyCode.A);
    }

    /// <summary>
    /// Reset the booleans for Input's capture.
    /// </summary>
    void ResetInput()
    {
        horizontal = 0;
        vertical = 0;
        sprintPressed = false;
        attackPressed = false;
        rollPressed = false;
        weaponActionPressed = false;
        attractWeaponPressed = false;
    }

    /// <summary>
    /// Update the StateManager with the Inputs.
    /// </summary>
    void UpdateStates()
    {
        states.Horizontal = horizontal;
        states.Vertical = vertical;

        Vector3 v = vertical * cameraController.transform.forward;
        Vector3 h = horizontal * cameraController.transform.right;

        states.MoveDirection = (v + h).normalized;

        if (sprintPressed)
            states.Sprint = !states.Sprint;

        states.Rolling = rollPressed;

        states.Attacking = attackPressed;

        states.PickUpSword = PickupWeaponPressed;

        states.DropSword = dropWeaponPressed;

        states.UpdatePlayer();

    }

    bool InputKeepDown(float t,ref float currentTimer, float actionTimer, string inputName, ref bool startCheck)
    {
        if(Input.GetButton(inputName))
        {
            currentTimer += t;
            if (currentTimer >= actionTimer)
            {
                currentTimer = 0;
                return true;
            }
            return false;
        }
        startCheck = false;
        return false;
    }
}
