﻿
public enum StatusList {
    Stun,
    Slowness
}

public class StatusEffect
{
    public StatusList effect;
    public float totalDuration;
    public float remainingDuration;
    public float value1;

    public StatusEffect(StatusList status, float totalDuration, float value1 = 0f)
    {
        effect = status;
        this.totalDuration = totalDuration;
        remainingDuration = totalDuration;
        this.value1 = value1;
    }

    public void UpdateTime(float time)
    {
        remainingDuration -= time;
    }
}

