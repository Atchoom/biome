﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamagedBehavior : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        StateManager.Instance.isDamaged = false;
        StateManager.Instance.hasBeenHit = false;
        PlayerController.Instance.setDamageCollider(false);
        PlayerController.Instance.IsInvincible = false;
    }
}
