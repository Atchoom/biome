﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Classe AnimatorFollow
 * Permet à l'objet personnage de suivre le déplacement des animations d'action.
**/
public class AnimatorFollow : MonoBehaviour
{
    StateManager states;
    Animator animator;

    public void Init(StateManager s)
    {
        states = s;
        animator = s.Anim;
    }
    /*
    public void OnAnimatorMove()
    {
        if (animator.GetBool("canMove") || Time.timeScale == 0)
            return;

        Vector3 deltaPosition = animator.deltaPosition;
        deltaPosition.y = 0;
        Vector3 v = (deltaPosition) / Time.deltaTime;
        if(float.IsNaN(v.x) || float.IsNaN(v.z))
        {
            v.x = states.Rgbody.velocity.x;
            v.z = states.Rgbody.velocity.z;
        }
        v.y = states.Rgbody.velocity.y;
        states.Rgbody.velocity = v;
    }*/
}
