﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for managing the damage collider of the sword.
/// </summary>
public class swordDoDamage : MonoBehaviour
{
    CapsuleCollider damageCollider;
    // Start is called before the first frame update
    void Start()
    {
        damageCollider = GetComponent<CapsuleCollider>();
        if (damageCollider == null)
            Debug.Log("Sword has no damage collider");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            PlayerController player = GameManager.Instance.PlayerController;
            List<StatusEffect> statusEffectsGiven = new List<StatusEffect>();
            statusEffectsGiven.Add(new StatusEffect(StatusList.Stun, player.StunDuration));
            other.GetComponent<EnemyController>().GetDamages(player.DamageDealt, statusEffectsGiven);
        }
    }
}
