﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterController : MonoBehaviour
{
    [Header("Info")]
    [SerializeField] protected bool canMove = true;
    [SerializeField] protected bool canAttack = true;
    [SerializeField] protected bool isInvincible = false;
    [Header("Stats")]
    [SerializeField] protected int maxHealth = 1;
    [SerializeField] protected int health;
    [SerializeField] protected int maxSpeed = 7;
    public int MaxSpeed
    {
        get => maxSpeed;
        set => maxSpeed = value;
    }
    [SerializeField] protected float attackSpeed = 1;
    public float AttackSpeed
    {
        get => attackSpeed;
        set => attackSpeed = value;
    }
    [SerializeField] protected int damageDealt = 1;
    public int DamageDealt
    {
        get => damageDealt;
        set => damageDealt = value;
    }
    [SerializeField] protected float attackRadius = 3;
    [SerializeField] protected float attackCooldown = 1; // minimum time between two attacks, in s
    public float AttackCooldown {
        get => attackCooldown;
        set => attackCooldown = value;
    }
    protected float lastAttackTime;
    protected bool hasCharacterAttacked;

    protected List<StatusEffect> statusEffects;

    protected float totalDamageDealt = 0;
    protected float totalDamageReceived = 0;

    protected GameManager gameManager;
    [SerializeField] protected Collider damageCollider;

    // Start is called before the first frame update
    protected virtual void Awake()
    {
        SetUp();
    }

    protected void SetUp()
    {
        health = maxHealth;
        lastAttackTime = 0f;
        hasCharacterAttacked = false;
        statusEffects = new List<StatusEffect>();
        if(damageCollider == null)
        {
            damageCollider = GetComponentInChildren<Collider>();
        }
    }

    protected virtual void Start()
    {
        gameManager = GameManager.Instance;
    }

    protected virtual void UpdateStatus()
    {
        foreach (var statusEffect in statusEffects)
        {
            statusEffect.UpdateTime(Time.deltaTime);
        }
        statusEffects.RemoveAll(effect => effect.remainingDuration <= 0f);
    }

    public virtual void GetDamages(int attackDamages, List<StatusEffect> attackStatuses = null)
    {
        if (isInvincible)
            return;
        health -= attackDamages;
        totalDamageReceived += attackDamages;
        if (attackStatuses != null)
        {
            foreach (var attackStatus in attackStatuses)
            {
                statusEffects.Add(attackStatus);
                //Debug.Log("Enemy stunned !");
            }
        }
        if (health <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Enable or disable the damage collider of the enemy.
    /// </summary>
    /// <param name="enable"></param>
    public void setDamageCollider(bool enable)
    {
        damageCollider.enabled = enable;
    }

    public float getAttackCooldown()
    {
        return attackCooldown;
    }
}
