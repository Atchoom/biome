﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordClass : MonoBehaviour
{
    [SerializeField] //Speed for sword movement when attracted
    private float speed = 2f;

    [SerializeField] //Target of sword movement
    private Vector3 target;

    [SerializeField] //If the sword is moving or not (when attracted)
    private bool isMoving = false;

    //If the sword is attached to player
    public bool isAttached = false;

    private Rigidbody rgbody;
    private Collider cld;

    // Start is called before the first frame update
    void Start()
    {
        rgbody = GetComponent<Rigidbody>();
        cld = GetComponent<Collider>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isMoving && !isAttached)
            goToPosition(target);
    }

    /// <summary>
    /// Move the sword to a position.
    /// </summary>
    /// <param name="targetPosition"></param>
    void goToPosition(Vector3 targetPosition)
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed*Time.deltaTime);
    }

    /// <summary>
    /// Update the target of the sword.
    /// </summary>
    /// <param name="newTarget"></param>
    public void changeTarget(Vector3 newTarget)
    {
        target = newTarget;
    }

    /// <summary>
    /// Start moving the sword to the player.
    /// </summary>
    /// <param name="player"></param>
    public void StartMoving(Vector3 player)
    {
        changeTarget(player);
        isMoving = true;
        rgbody.useGravity = false;
        cld.isTrigger = true;
        GetComponentInChildren<CapsuleCollider>().enabled = true;
    }

    /// <summary>
    /// Stop moving the sword.
    /// </summary>
    public void StopMoving()
    {
        isMoving = false;
        rgbody.useGravity = true;
        if (!isAttached)
            cld.isTrigger = false;
        GetComponentInChildren<CapsuleCollider>().enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isMoving) //If sword is not moving, player can't take it.
            return;
        if (other.CompareTag("Player"))
        {
            StopMoving();
            other.GetComponent<WeaponController>().SwordToHand();
        }
    }
}
