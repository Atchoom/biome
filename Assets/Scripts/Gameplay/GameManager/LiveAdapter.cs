﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// The data for adapting and enemy. They are added to an enemy at its creation.
/// </summary>
[Serializable]
public struct EnemyAdaptData
{
    public int numberOfAdaptation;
    public int adaptSpeed;
    public float adaptRotationSpeed;
    public float adaptAttack;
    public float adaptRange;
}

/// <summary>
/// The LiveAdapter, which adapt the enemies and their spawn from the informations gathered by the DataManager.
/// </summary>
public class LiveAdapter : MonoBehaviour
{
    DataManager dataManager;

    [SerializeField] private float timeBetweenAdaptation = 60f;
    [Header("Movement Speed")]
    [SerializeField] private int adaptSpeedCoef = 1;
    [SerializeField] private int adaptSpeedMin = -10;
    [SerializeField] private int adaptSpeedMax = 10;
    [Header("Rotation Speed")]
    // values in rad/s
    [SerializeField] private float adaptRotationSpeedCoef = 1;
    [SerializeField] private float adaptRotationSpeedMin = -20;
    [SerializeField] private float adaptRotationSpeedMax = 20;
    [Header("Attack Speed")]
    [SerializeField] private float adaptAttackSpeedCoef = 0.2f;
    [SerializeField] private float adaptAttackSpeedMin = -1;
    [SerializeField] private float adaptAttackSpeedMax = 1;
    [Header("Range")]
    [SerializeField] private float adaptRangeCoef = 2;
    [SerializeField] private float adaptRangeMin = -10;
    [SerializeField] private float adaptRangeMax = 10;

    private float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(dataManager == null)
        {
            dataManager = GetComponent<DataManager>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= timeBetweenAdaptation)
        {
            AdaptSpawnEnemies();
            AdaptEnemiesType();
            timer = 0;
        }
    }

    /// <summary>
    /// Handler for ForceAdapt event. It forces an adaptation.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public void OnForceAdapt(object sender, EventArgs args)
    {
        Debug.Log("Forcing adaptation");
        AdaptSpawnEnemies();
        AdaptEnemiesType();
    }

    /// <summary>
    /// Function which adapt the spawn of enemies. It relies on the ratio of death.
    /// </summary>
    void AdaptSpawnEnemies()
    {
        //Get the WaveRing concerned
        WaveRing currentBiome = GameManager.Instance.GetPlayerBiome();
        if (currentBiome == null || currentBiome.enemyTypes.Length == 0)
            return;
        int currentBiomeID = currentBiome.biomeID;
        if (dataManager.biomes.Length == 0)
            return;
        //Get the Data of the biome
        DataBiome currentBiomeData = dataManager.biomes[currentBiomeID];
        int size_types = currentBiomeData.listEnemies.Length;
        if (size_types == 1)
            return;
        if (currentBiomeData.numberOfDeadEnemiesSinceAdaptation == 0)
        { return; } else {
            dataManager.biomes[currentBiomeID].numberOfDeadEnemiesSinceAdaptation = 0;
        }
        int sumDeaths = 0;
        foreach(int i in currentBiomeData.deadEnemies)
        {
            sumDeaths += i;
        }
        if (sumDeaths == 0)
            return;
        //Get the percentage of dead enemies by type.
        float[] ratio = new float[size_types];
        for (int i = 0; i < size_types; i++)
        {
            ratio[i] = currentBiomeData.deadEnemies[i] / sumDeaths;
        }
        int min_index = 0;
        int max_index = 0;
        float max_ratio = ratio[0];
        float min_ratio = ratio[0];
        //Get the max and the min ratio
        for(int i = 1; i < size_types; i++)
        {
            if(ratio[i] > max_ratio)
            {
                max_index = i;
                max_ratio = ratio[i];
            } else if (ratio[i] < min_ratio)
            {
                min_index = i;
                min_ratio = ratio[i];
            }
        }
        //Update them
        int new_min = (int)(currentBiome.enemyTypes[max_index].getSpawnProbability() / 1.5);
        if (new_min == 0)
            new_min = 1;
        currentBiome.enemyTypes[max_index].setSpawnProbability(new_min);
        currentBiome.enemyTypes[min_index].setSpawnProbability((int)(currentBiome.enemyTypes[min_index].getSpawnProbability() * 1.5));
    }

    /// <summary>
    /// Functio which adapt the parameters of the enemies in the biome.
    /// </summary>
    void AdaptEnemiesType()
    {
        //Get the WaveRing
        WaveRing currentBiome = GameManager.Instance.GetPlayerBiome();
        if (currentBiome == null || currentBiome.enemyTypes.Length == 0 || dataManager.biomes.Length == 0)
            return;
        int currentBiomeID = currentBiome.biomeID;
        //Get the biome's data
        DataBiome currentBiomeData = dataManager.biomes[currentBiomeID];
        int size_types = currentBiomeData.listEnemies.Length;
        if (size_types == 0)
            return;
        //For eaach type, adapt the enemy
        for(int i = 0; i < size_types; i++)
        {
            AdaptEnemyType(currentBiomeData, i);
        }
    }

    /// <summary>
    /// Adapt the parameters of an enemy. It modifies the EnemyAdaptData stored in the DataBiome.
    /// </summary>
    /// <param name="data"> The biome's Data</param>
    /// <param name="index">The index of the enemy</param>
    void AdaptEnemyType(DataBiome data,int index)
    {
        bool isAdapted = false;
        //Adapt speed and rotation speed
        if(data.aggroEnemies[index] != 0)
        {
            float ratio = data.foughtEnemies[index] / data.aggroEnemies[index];
            if (ratio < 0.5f)
            {
                data.listAdaptData[index].adaptSpeed = Mathf.Min(adaptSpeedMax,
                    data.listAdaptData[index].adaptSpeed + adaptSpeedCoef);
                data.listAdaptData[index].adaptRotationSpeed = Mathf.Min(adaptRotationSpeedMax,
                    data.listAdaptData[index].adaptRotationSpeed + adaptRotationSpeedCoef);
                    isAdapted = true;
            } else if (ratio > 0.8f)
            {
                data.listAdaptData[index].adaptSpeed = Mathf.Max(adaptSpeedMin,
                    data.listAdaptData[index].adaptSpeed - adaptSpeedCoef);
                data.listAdaptData[index].adaptRotationSpeed = Mathf.Max(adaptRotationSpeedMin,
                    data.listAdaptData[index].adaptRotationSpeed - adaptRotationSpeedCoef);
                    isAdapted = true;
            }
        }
        // Adapt enemy attack speed
        if(data.numberOfAttack[index] != 0 && data.attackDamages[index] != 0)
        {
            int numberOfAttackWithDamage = (int)(data.totalDamageDealt[index] / data.attackDamages[index]);
            float ratioAttackSpeed = numberOfAttackWithDamage / data.numberOfAttack[index];
            if (ratioAttackSpeed < .2f)
            {
                data.listAdaptData[index].adaptAttack = Mathf.Min(adaptAttackSpeedMax,
                    data.listAdaptData[index].adaptAttack + adaptAttackSpeedCoef);
                isAdapted = true;
            }
            else if (ratioAttackSpeed > .6f)
            {
                data.listAdaptData[index].adaptAttack = Mathf.Max(adaptAttackSpeedMin,
                    data.listAdaptData[index].adaptAttack - adaptAttackSpeedCoef);
                isAdapted = true;
            }
        }

        // Adapt enemy detection range
        if(data.seenEnemies[index] != 0)
        {
            float ratioDetection = data.aggroEnemies[index] / data.seenEnemies[index];
            if(ratioDetection < .33f)
            {
                data.listAdaptData[index].adaptRange = Mathf.Min(adaptRangeMax,
                data.listAdaptData[index].adaptRange + adaptRangeCoef);
                isAdapted = true;
            } else if (ratioDetection > 0.9f)
            {
                data.listAdaptData[index].adaptRange = Mathf.Max(adaptRangeMin,
                data.listAdaptData[index].adaptRange - adaptRangeCoef);
                isAdapted = true;
            }
        }

        //If is adapted, clear data in the DataManager.
        if (isAdapted)
        {
            data.listAdaptData[index].numberOfAdaptation++;
            dataManager.Clear(data.biomeID,index);
        }
    }
}
