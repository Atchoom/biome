﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// DataBiome : a structure containing all informations gathered in a biome, in order to use them to adapt the biome.
/// </summary>
[Serializable]
public struct DataBiome
{
    public int biomeID;// the biome ID of this Data
    public EnemyID[] listEnemies;// an array of EnemyID, corresponding to the IDs of all enemis which may spawn in the biome.
    public float[] attackDamages;
    public int[] spawnedEnemies;// an array of int with the number of spawned enemies for each type. The EnemyID of the nth of this array is the nth of the listEnemies array.
    public int[] deadEnemies;// an array of int with the number of dead enemies for each type. The EnemyID of the nth of this array is the nth of the listEnemies array.
    public int[] aggroEnemies;// an array of int with the number of "aggro-ed" enemies.
    public int[] seenEnemies;// an array of int with the number of seen enemies.
    public int[] foughtEnemies;// an array of int with the number of fought enemies.
    public float[] totalDamageDealt; // an array of float with the damage dealt by type.
    public float[] totalDamageReceived;// an array of float with the damage received by type.
    public int[] numberOfAttack;//an array of int with the number of attack made by type.
    public EnemyAdaptData[] listAdaptData; // an array of data for the adaptation of each type.
    public int numberOfDeadEnemiesSinceAdaptation;

    /// <summary>
    /// Create a DataBiome from a Wavering.
    /// </summary>
    /// <param name="wave"> The WaveRing of the biome</param>
    public DataBiome(WaveRing wave)
    {
        biomeID = wave.biomeID;
        EnemyBiomeData[] data = wave.enemyTypes;
        int size = data.Length;
        listEnemies = new EnemyID[size];
        for(int i = 0; i < size; i++)
        {
            listEnemies[i] = data[i].getType();
        }
        attackDamages = new float[size];
        spawnedEnemies = new int[size];
        deadEnemies = new int[size];
        aggroEnemies = new int[size];
        foughtEnemies = new int[size];
        seenEnemies = new int[size];
        totalDamageDealt = new float[size];
        totalDamageReceived = new float[size];
        numberOfAttack = new int[size];
        listAdaptData = new EnemyAdaptData[size];
        numberOfDeadEnemiesSinceAdaptation = 0;
    }

    /// <summary>
    /// Function to get the enemy's index in the listEnemies array from his ID.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public int getEnemyIndexFromID(EnemyID id)
    {
        int size = listEnemies.Length;
        for(int i = 0; i < size; i++)
        {
            if (listEnemies[i] == id)
                return i;
        }
        return -1;
    }
}

/// <summary>
/// The DataManager of the GameManager. It gathers all the biome's data.
/// </summary>
public class DataManager : MonoBehaviour
{
    /// <summary>
    /// The array of DataBiome, one for each biome.
    /// </summary>
    public DataBiome[] biomes;

    /// <summary>
    /// Handler for FirstGenerationDone Event. It initialize Data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public void OnFirstGenerationDone(object sender, EventArgs args)
    {

        InitData();
    }

    /// <summary>
    /// Initialize biome's data of the DataManager, with the Heightmap.
    /// </summary>
    void InitData()
    {
        HexagonHeightmap map = GameManager.Instance.Heightmap;
        int size = map.waveRings.Length;
        biomes = new DataBiome[size];
        for (int i = 0; i < size ; i++)
        {
            biomes[i] = new DataBiome(map.waveRings[i]);
        }
    }

    /// <summary>
    /// Function which get the index of a Databiome from a biome ID.
    /// </summary>
    /// <param name="biomeID"></param>
    /// <returns></returns>
    int getBiomeIndexFromID(int biomeID)
    {
        int size = biomes.Length;
        for (int i = 0; i < size; i ++)
        {
            if(biomes[i].biomeID == biomeID)
            {
                return i;
            }
        }
        return -1;
    }

    /// <summary>
    /// Clear function, which clear the data of an enemy type.
    /// </summary>
    /// <param name="biomeID"> The biome ID of the enemy</param>
    /// <param name="enemyIndex"> The enemy index in the list of enemy of the biome</param>
    public void Clear(int biomeID, int enemyIndex)
    {
        int index = getBiomeIndexFromID(biomeID);
        biomes[index].spawnedEnemies[enemyIndex] = 0;
        biomes[index].deadEnemies[enemyIndex] = 0;
        biomes[index].aggroEnemies[enemyIndex] = 0;
        biomes[index].foughtEnemies[enemyIndex] = 0;
        biomes[index].seenEnemies[enemyIndex] = 0;
        biomes[index].totalDamageDealt[enemyIndex] = 0;
        biomes[index].totalDamageReceived[enemyIndex] = 0;
        biomes[index].numberOfAttack[enemyIndex] = 0;
    }

    /// <summary>
    /// Update the biomes's data after the death of an enemy.
    /// </summary>
    /// <param name="id"> The enemy ID</param>
    /// <param name="biomeOfEnemy">The biome ID</param>
    void incrEnemyDeath(EnemyID id, int biomeOfEnemy)
    {
        int index = getBiomeIndexFromID(biomeOfEnemy);
        for(int j = 0; j < biomes[index].listEnemies.Length; j++ )
        {
            if (biomes[index].listEnemies[j] == id)
            {
                biomes[index].deadEnemies[j]++;
                biomes[index].numberOfDeadEnemiesSinceAdaptation++;
            }
        }
    }

    /// <summary>
    /// Update the biome's data after the destruction of an enemy. If it's an old generation, the data are not used.
    /// </summary>
    /// <param name="data"> The enemyData</param>
    /// <param name="numberOfAdaptation"> The numberOfAdaptation of the enemy</param>
    void incrEnemyData(EnemyData data, int numberOfAdaptation)
    {
        int i = getBiomeIndexFromID(data.biomeID);
        for (int j = 0; j < biomes[i].listEnemies.Length; j++)
        {
            if (biomes[i].listEnemies[j] == data.id)
            {
                if (biomes[i].listAdaptData[j].numberOfAdaptation < numberOfAdaptation)
                    return;
                biomes[i].attackDamages[j] = data.attackDamage;
                biomes[i].numberOfAttack[j] += data.numberOfAttack;
                if(data.hasBeenSeen)
                    biomes[i].seenEnemies[j]++;
                if (data.hasAggroPlayer)
                    biomes[i].aggroEnemies[j]++;
                if (data.hasFoughtPlayer)
                {
                    biomes[i].foughtEnemies[j]++;
                    biomes[i].totalDamageDealt[j] += data.damageDealt;
                    biomes[i].totalDamageReceived[j] += data.damageReceived;
                }
                return;
            }
        }
    }

    /// <summary>
    /// Handle function for EnemyDeath event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public void OnEnemyDeath(object sender, CharacterDeathEventArgs args)
    {
        EnemyController enemy = (EnemyController)args.characterController;
        incrEnemyDeath(enemy.id, enemy.biome);
    }

    /// <summary>
    /// Handle function for SendEnemyData event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public void onEnemyDataReceived(object sender, SendEnemyDataEventArgs args)
    {
        incrEnemyData(args.data,args.numberOfAdaptation);
    }
}
