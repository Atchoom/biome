﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enumerate with all the types of enemy.
/// If you want to add a new enemy, you have to add his ID here.
/// </summary>
[Serializable]
public enum EnemyID
{
    Skeleton,
    Dog,
    Diablotin,
    Cochon,
}

/// <summary>
/// Enemy struct which contains a GameObject corresponding to an enemy Prefab.
/// The Enemies class store a list of Enemy struct, containing all enemies in the biomes.
/// </summary>
[Serializable]
public struct Enemy
{
    [SerializeField] GameObject enemyPrefab;

    public GameObject getPrefab()
    { return enemyPrefab; }
}

/// <summary>
/// EnemyBiomeData is a structure with an EnemyID and his spawn probability, spawnWeight.
/// </summary>
[Serializable]
public struct EnemyBiomeData
{
    [SerializeField] EnemyID type;
    [SerializeField] int spawnWeight;

    public int getSpawnProbability()
    { return spawnWeight; }

    public void setSpawnProbability(int n)
    {
        if (n >= 0)
            spawnWeight = n;
    }

    public EnemyID getType()
    { return type; }
}

/// <summary>
/// Enemies class, which is contains an array of Enemy, that you have to fill in Inspector.
/// </summary>
public class Enemies : MonoBehaviour
{
    public Enemy[] enemies;

    public Enemy? findEnemyFromType(EnemyID type)
    {
        int size = enemies.Length;
        for(int i = 0; i < size; i++)
        {
            if (type == enemies[i].getPrefab().GetComponent<EnemyController>().id)
                return enemies[i];
        }
        return null;
    }
}
