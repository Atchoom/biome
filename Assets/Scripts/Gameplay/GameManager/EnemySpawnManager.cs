﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The class which takes care of the spawn of the enemies. They spawn around the player, and are destroyed if they are too far away.
/// The number of enemies around him is fixed.
/// </summary>
public class EnemySpawnManager : MonoBehaviour
{
    /// <summary>
    /// The list of current enemies.
    /// </summary>
    [SerializeField] public List<EnemyController> spawnedEnemies;
    public float despawnDistance = 20f;
    public float minDistanceForSpawnAroundPlayer = 5f;
    public float maxDistanceForSpawnAroundPlayer = 15f;
    public int numberOfEnemyAroundPlayer = 3;

    private float timeBeforeLastClear = 0;
    private float timeBeforeLastSpawn = 0;
    public float timeBetweenClear = 25f;
    public float timeBetweenSpawn = 100f;

    private DataManager dataManager;
    private GameManager gameManager;

    private bool enemiesAreStopped = false;

    private void Start()
    {
        dataManager = GameManager.Instance.GetComponent<DataManager>();
        gameManager = GameManager.Instance;
    }

    void Update()
    {
        //Wait for generation before spawn
        if (!gameManager.IsFirstGenerationDone)
            return;

        timeBeforeLastSpawn += Time.deltaTime;
        timeBeforeLastClear += Time.deltaTime;
        if(timeBeforeLastClear >= timeBetweenClear)
        { ClearEnemies(); timeBeforeLastClear = 0; }
        if(timeBeforeLastSpawn >= timeBetweenSpawn)
        {
            if (spawnedEnemies.Count < numberOfEnemyAroundPlayer)
                SpawnNewEnemies();
            timeBeforeLastSpawn = 0;
        }
    }

    /// <summary>
    /// Return the distance between this enemy and the player.
    /// </summary>
    /// <param name="enemy"></param>
    /// <returns></returns>
    float distanceEnemyToPlayer(EnemyController enemy)
    {
        Transform playerPosition = GameManager.Instance.PlayerController.transform;
        return Mathf.Sqrt(Mathf.Pow(playerPosition.position.x - enemy.transform.position.x, 2) + Mathf.Pow(playerPosition.position.z - enemy.transform.position.z, 2));
    }

    void clearEventsEnemy(EnemyController enemy)
    {
        enemy.EnemyDeathEvent -= OnEnemyDeath;
        enemy.EnemyDeathEvent -= dataManager.OnEnemyDeath;
    }

    /// <summary>
    /// Add an enemy to the list of current enemies.
    /// </summary>
    /// <param name="enemy"></param>
    void AddEnemy(EnemyController enemy)
    {
        enemy.sendDataEvent += dataManager.onEnemyDataReceived;
        enemy.EnemyDeathEvent += dataManager.OnEnemyDeath;
        enemy.EnemyDeathEvent += OnEnemyDeath;
        enemy.EnemyDeathEvent += GameManager.Instance.GetComponent<DataManager>().OnEnemyDeath;
        spawnedEnemies.Add(enemy);
    }

    /// <summary>
    /// Check all the spawned enemies's distance to player. If it's more than the despawnDistance, it destroys the enemy.
    /// </summary>
    void ClearEnemies()
    {
        int size =spawnedEnemies.Count;
         for (int i = size - 1; i >= 0; i--)
         {
             if (distanceEnemyToPlayer(spawnedEnemies[i]) >= despawnDistance)
             {
                 GameObject enemyToDestroy = spawnedEnemies[i].gameObject;
                 clearEventsEnemy(spawnedEnemies[i]);
                 spawnedEnemies.RemoveAt(i);
                 Destroy(enemyToDestroy);
             }
         }
    }

    /// <summary>
    /// Remove an enemy from the list of spawned enemies.
    /// </summary>
    /// <param name="enemy"></param>
    void RemoveEnemy(EnemyController enemy)
    {
        GameObject enemyGO = enemy.gameObject;
        int size = spawnedEnemies.Count;
        for (int i = 0; i < size; i++)
        {
            if(enemyGO == spawnedEnemies[i].gameObject)
            {
                spawnedEnemies.RemoveAt(i);
                clearEventsEnemy(enemy);
                enemy.EnemyDeathEvent -= OnEnemyDeath;
                enemy.EnemyDeathEvent -= GameManager.Instance.GetComponent<DataManager>().OnEnemyDeath;
                Destroy(enemyGO);
                return;
            }
        }
    }

    /// <summary>
    /// Apply the adaptation's parameter of this  enemy.
    /// </summary>
    /// <param name="enemy"></param>
    void AdaptEnemyBeforeSpawn(ref EnemyController enemy)
    {
        DataBiome dataBiome = dataManager.biomes[enemy.biome];
        int index = dataBiome.getEnemyIndexFromID(enemy.id);
        if (index == -1)
            return;
        EnemyAdaptData data = dataBiome.listAdaptData[index];
        enemy.GetComponent<EnemyDataManager>().numberOfAdaptation = data.numberOfAdaptation;
        enemy.addToSpeed(data.adaptSpeed);
        enemy.addToAttackSpeed(data.adaptAttack);
        enemy.addToRange(data.adaptRange);
        enemy.addToRotationSpeed(data.adaptRotationSpeed);
    }

    /// <summary>
    /// Spawn random enemies around the player, based on the current number of spawned enemies.
    /// </summary>
    void SpawnNewEnemies()
    {
        System.Random rand = new System.Random();
        int newEnemiesCount = numberOfEnemyAroundPlayer - spawnedEnemies.Count;
        if (newEnemiesCount == 0)
            return;
        int iterator = 0;
        int numberSpawned = 0;
        //If the spawn failed, it tries again until all the enemies are spawned or the iterator is > 30.
        while(numberSpawned < newEnemiesCount && iterator <= 30)
        {
            if (SpawnEnemy(rand))
                numberSpawned++;
            iterator++;
        }

    }

    /// <summary>
    /// Spawn a new random enemy in a random position around the player.
    /// </summary>
    /// <param name="rand"> The random generator </param>
    /// <returns> True if the spawn is successfull or false if not</returns>
    bool SpawnEnemy(System.Random rand)
    {
        //Get a random position around player.
        Vector3 playerPosition = GameManager.Instance.PlayerController.transform.position;
        float r = (float)rand.NextDouble() * (maxDistanceForSpawnAroundPlayer - minDistanceForSpawnAroundPlayer) + minDistanceForSpawnAroundPlayer;
        float angle = (float)rand.NextDouble() * 360;
        Vector3 position = new Vector3(playerPosition.x + r * Mathf.Cos(angle), 10f, playerPosition.z + r * Mathf.Sin(angle));
        WaveRing currentBiome = GameManager.Instance.GetBiomeFromPosition(position);
        if (currentBiome == null)
        {
            return true; }//on renvoie vrai quand c'est dans un l'océan
        //Get a random type for the enemy based on the biome.
        EnemyBiomeData[] enemiesType = currentBiome.enemyTypes;
        if (enemiesType.Length == 0)
        {
            return true;//on renvoie vrai quand c'est dans un biome safe
        }
        EnemyID choosenType = selectTypeFromBiome(enemiesType, rand);
        Enemy? choosenEnemy = GameManager.Instance.enemyList.findEnemyFromType(choosenType);
        if (choosenEnemy == null)
        {
            Debug.Log("No prefab for enemySpawn");
            return true;
        }
        //Check the spawn's position. It has to be on ground.
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down, out hit, 100f))
        {
            if(!hit.collider.CompareTag("Terrain"))
            {
                return false;
            }
            GameObject newEnemy = Instantiate(choosenEnemy.Value.getPrefab(), hit.point,Quaternion.identity);
            EnemyController ec = newEnemy.GetComponent<EnemyController>();
            ec.StopEnemy = enemiesAreStopped;
            ec.biome = currentBiome.biomeID;
            AdaptEnemyBeforeSpawn(ref ec);
            AddEnemy(ec);
            return true;
        } else
        {
            return false;
        }
    }

    /// <summary>
    /// Get a random type of enemy based on the spawn and the different spawn probability.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="rand"></param>
    /// <returns></returns>
    EnemyID selectTypeFromBiome(EnemyBiomeData[] data, System.Random rand)
    {
        int size = data.Length;
        int totalWeight = 0;
        for(int i = 0; i < size; i++)
        {
            totalWeight += data[i].getSpawnProbability();
        }
        int weightValue = rand.Next(0,totalWeight);
        totalWeight = 0;
        for (int i = 0; i < size; i++)
        {
            totalWeight += data[i].getSpawnProbability();
            if (totalWeight > weightValue)
                return data[i].getType();
        }
        return data[size-1].getType();
    }

    void OnEnemyDeath(object sender, CharacterDeathEventArgs args)
    {
        EnemyController enemy = (EnemyController)args.characterController;
        RemoveEnemy(enemy);
    }

    /// <summary>
    /// Stop all enemies in the scene and the new ones.
    /// </summary>
    public void stopAllEnemies()
    {
        enemiesAreStopped = !enemiesAreStopped;
        foreach(EnemyController e in spawnedEnemies)
        {
            e.StopEnemy = !e.StopEnemy;
        }
    }

    /// <summary>
    /// Force clear and spawn.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void OnForceSpawn(object sender, EventArgs e)
    {
        ClearEnemies();
        SpawnNewEnemies();
    }
}
