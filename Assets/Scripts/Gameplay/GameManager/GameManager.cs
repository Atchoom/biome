﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The GameManager
/// </summary>
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField] private Transform userInterfacePrefab;
    private GameObject uiCanvas;
    private UIScript uiScript;
    private MenuScript menuScript;
    [SerializeField] private GameObject debugUiCanvasPrefab;
    private GameObject debugUiCanvas;
    private DebugInfoManager debugInfoManager;
    private DebugInput debugInput;
    public DebugInput DebugInput => debugInput;
    private LiveAdapter liveAdapter;
    private EnemySpawnManager enemySpawnManager;
    private PlayerController playerController;
    public PlayerController PlayerController => playerController;
    public HexagonHeightmap Heightmap;
    public Enemies enemyList;
    private GameObject spawnPoint;

    [SerializeField] public bool debug = true;
    private bool isDebugActive = true;
    public bool IsDebugActive => isDebugActive;
    private float distanceForWin;
    private bool isFirstGenerationDone = false;
    public bool IsFirstGenerationDone => isFirstGenerationDone;

    public event EventHandler ToggleEnemyDebugUI;
    public event EventHandler PlayerWinEvent;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        isDebugActive = debug;
        if(enemyList == null)
        {
            enemyList = GetComponent<Enemies>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if(Heightmap == null)
            Heightmap = HexagonHeightmap.GetInstance();
        Heightmap.FirstGenerationDone += OnFirstGenerationDone;
        Heightmap.FirstGenerationDone += GetComponent<DataManager>().OnFirstGenerationDone;

        playerController = PlayerController.Instance;

        playerController.PlayerDamagesEvent += OnPlayerDamages;
        playerController.PlayerDeathEvent += OnPlayerDeath;

        uiScript = FindObjectOfType<UIScript>();
        if (uiScript == null && userInterfacePrefab != null)
        {
            Transform userInterface = Instantiate(userInterfacePrefab);
            uiCanvas = userInterface.gameObject;
            uiScript = uiCanvas.GetComponent<UIScript>();
        }
        playerController.PlayerDamagesEvent += uiScript.OnPlayerDamages;
        playerController.PlayerDeathEvent += uiScript.OnPlayerDeath;
        playerController.PlayerHealthChangeEvent += uiScript.OnPlayerHealthChangeEvent;
        PlayerWinEvent += uiScript.OnPlayerWin;
        menuScript = uiScript.getMenuGroup().GetComponent<MenuScript>();
        menuScript.QuitEvent += OnQuit;
        menuScript.RestartEvent += OnRestart;
        menuScript.FOVEvent += OnFOV;
        menuScript.SoundEvent += OnSound;
        menuScript.DrawEvent += OnDraw;
        menuScript.SensitivityEvent += OnSensitivity;

        uiScript.SetHeartCount(playerController.Health/10);

        Heightmap.FirstGenerationDone += uiScript.OnFirstGenerationDone;

        liveAdapter = gameObject.GetComponent<LiveAdapter>();
        enemySpawnManager = gameObject.GetComponent<EnemySpawnManager>();

        if (debugUiCanvasPrefab != null)
        {
            debugUiCanvas = Instantiate(debugUiCanvasPrefab);
            debugInfoManager = debugUiCanvas.GetComponent<DebugInfoManager>();
        }
        else
        {
            // may be null
            debugUiCanvas = GameObject.Find("Debug Canvas");
            debugInfoManager = debugUiCanvas.GetComponent<DebugInfoManager>();
        }

        debugInput = GetComponent<DebugInput>();

        debugInfoManager.ModifyHealth += playerController.OnModifyHealth;
        if (liveAdapter == null)
            Debug.Log("LiveAdapter is null");
        debugInfoManager.ForceAdaptEvent += liveAdapter.OnForceAdapt;
        debugInfoManager.ForceSpawnEvent += enemySpawnManager.OnForceSpawn;

        if (debugUiCanvas == null)
            Debug.Log("debugUICanvas is null.");
        if (debugInfoManager == null)
            Debug.Log("debugInfoManager is null.");
        Cursor.visible = isDebugActive;
        playerController.gameObject.SetActive(false);
        distanceForWin = Heightmap.IslandRadius;
        Debug.Log("GameManager Start finished.");
    }

    private void Update()
    {
        debugUiCanvas.SetActive(isDebugActive);
        float distanceFromCenter = Mathf.Sqrt(Mathf.Pow(playerController.transform.position.x, 2) + Mathf.Pow(playerController.transform.position.z, 2));
        if(distanceFromCenter >= distanceForWin)
        {
            playerController.IsInvincible = true;
            PlayerWinEvent?.Invoke(this, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Handler for FirstGeneration event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private void OnFirstGenerationDone(object sender, EventArgs args)
    {
        isFirstGenerationDone = true;
        playerController.gameObject.SetActive(true);
        RespawnPlayer();
        if(playerController != null)
        {
            if(playerController.WeaponController != null)
            {
                playerController.WeaponController.SwordToHand();
            } else
            {
                Debug.Log("WeaponController null in playerController");
            }
        } else
        {
            Debug.Log("playerController null");
        }
        DebugLog("Player spawned.");
    }

    /// <summary>
    /// Respawns the player at a location given by the gameobject spawnPoint
    /// </summary>
    public void RespawnPlayer()
    {
        if(playerController != null)
        {
            Transform player = playerController.gameObject.transform;
            float spawnHeight = Heightmap.GetTileAt(AxialCoordinates.Zero).GetLocalHex(AxialCoordinates.Zero).Height;
            player.position = Vector3.up * spawnHeight;
            player.GetComponent<Rigidbody>().velocity = new Vector3();
        }
        Cursor.visible = isDebugActive;
    }

    /// <summary>
    /// Function for checking the altitude of the spawn point
    /// </summary>
    void CheckSpawnPointPosition()
    {
        RaycastHit hit;
        if(Physics.Raycast(spawnPoint.transform.position, Vector3.down, out hit, 30f) || Physics.Raycast(spawnPoint.transform.position, Vector3.up, out hit, 30f))
        {
            spawnPoint.transform.position = hit.point;
        } else
        {
            Debug.Log("SpawnPoint not in right place");
        }
    }

    /// <summary>
    /// Function for printing in DebugInfo.
    /// </summary>
    /// <param name="text"></param>
    public void DebugLog(string text)
    {
        if (debugInfoManager != null)
            debugInfoManager.AddToLog(text);
    }

    /// <summary>
    /// This activate or deactivate the Debug Canvas if it exists.
    /// </summary>
    public void ToggleDebugCanvas()
    {
        if (debugUiCanvas != null)
        {
            debugUiCanvas.SetActive(isDebugActive);
            Cursor.visible = isDebugActive;
        }
    }

    /// <summary>
    /// Function which return in which biome a position is.
    /// </summary>
    /// <param name="pos"> The position </param>
    /// <returns> The biome (WaveRing)</returns>
    public WaveRing GetBiomeFromPosition(Vector3 pos)
    {
        float distanceFromCenter = Mathf.Sqrt(Mathf.Pow(pos.x, 2) + Mathf.Pow(pos.z, 2));
        int size = Heightmap.waveRings.Length;
        float sumOfDistance = 0;
        for (int i = 0; i < size; i++)
        {
            sumOfDistance += Heightmap.waveRings[i].ringWidth;
            if (distanceFromCenter < sumOfDistance)
                return Heightmap.waveRings[i];
        }
        return null;
    }

    /// <summary>
    /// Return the player's current biome.
    /// </summary>
    /// <returns> The biome (WaveRing)</returns>
    public WaveRing GetPlayerBiome()
    {
        Transform playerPosition = playerController.transform;
        return GetBiomeFromPosition(playerPosition.position);
    }

    public void OnEnemyDeath(object sender, CharacterDeathEventArgs args)
    {
        DebugLog("Enemy "+args.characterController.name+" died");
        // add here to make a new enemy respawn
    }

    public void OnEnemyDamages(object sender, CharacterDamagesEventArgs args)
    {
        DebugLog("Enemy "+args.characterController.name+" has taken "+args.attackDamages+" damages. "
                 +args.healthLeft+" health left.");
    }

    public void OnPlayerDeath(object sender, CharacterDeathEventArgs args)
    {
        DebugLog("Player has died ");
        PlayerController.gameObject.SetActive(false);
        Cursor.visible = true;
    }

    public void OnPlayerDamages(object sender, CharacterDamagesEventArgs args)
    {
        DebugLog("Player has taken "+args.attackDamages+" damages. "+args.healthLeft+" health left.");
    }

    public void OnSwitchMoveAction(object sender, SwitchMoveActionEventArgs args)
    {
        switch (args.CurrentMoveAction.moveType)
        {
            case MovementType.Stop: DebugLog("Enemy stops"); break;
            case MovementType.MoveTowardsPlayer: DebugLog("Enemy moves towards player");
                break;
            case MovementType.MoveFromPlayer: DebugLog("Enemy moves away from player");
                break;
            case MovementType.MoveForward: DebugLog("Enemy moves forward");
                break;
            case MovementType.MoveBackward: DebugLog("Enemy moves backward");
                break;
            case MovementType.MoveLeft: DebugLog("Enemy moves left");
                break;
            case MovementType.MoveRight: DebugLog("Enemy moves right");
                break;
            case MovementType.Attack: DebugLog("Enemy Attack");
                break;
            case MovementType.RandomMove: DebugLog("Random move");
                break;
            default: Debug.LogError("MoveType unknown");
                break;
        }
    }

    public void OnToggleDebugUI(object sender, EventArgs args)
    {
        isDebugActive = !isDebugActive;
        ToggleDebugCanvas();
        ToggleEnemyDebugUI?.Invoke(this, EventArgs.Empty);
    }

    /**
    *
    *   Events sent by UI menu
    *
    */
    public void OnQuit(object sender, QuitArgs args)
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }

    public void OnRestart(object sender, RestartArgs args)
    {
        playerController.gameObject.SetActive(true);
        playerController.ResetStats();
        RespawnPlayer();
        playerController.WeaponController.SwordToHand();
    }

    public void OnFOV(object sender, FOVArgs args) /* field of view */
    {
    }

    public void OnSensitivity(object sender, SensitivityArgs args) /* mouse sensitivity */
    {
    }

    public void OnDraw(object sender, DrawArgs args) /* draw distance */
    {
    }

    public void OnSound(object sender, SoundArgs args) /*  volume */
    {
    }
}
