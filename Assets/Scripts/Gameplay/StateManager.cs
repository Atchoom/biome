﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateManager is the main class of the player, it controls the state of action of the player.
/// </summary>
public class StateManager : MonoBehaviour
{
    public static StateManager Instance { get; private set; }

    [Header("Base Player stats")]
    [SerializeField]
    private float playerSpeed = 3f;
    [SerializeField]
    private float sprintSpeed = 7f;
    [SerializeField]
    private float crossFadeFact = 0.0f;
    [SerializeField]
    private float characterRotationFact = 0.8f;
    private Vector3 speed_vector;
    [Header("Components")]
    [SerializeField]
    private GameObject model;
    public GameObject Model
    {
        get
        {
            return this.model;
        }
        set
        {
            this.model = value;
        }
    }
    [SerializeField]
    private Animator anim;
    public Animator Anim
    {
        get
        {
            return this.anim;
        }
        set
        {
            this.anim = value;
        }
    }
    [SerializeField]
    private Rigidbody rgbody;
    public Rigidbody Rgbody
    {
        get
        {
            return this.rgbody;
        }
        set
        {
            this.rgbody = value;
        }
    }
    [SerializeField]
    private AnimatorFollow a_follow;
    public AnimatorFollow A_follow
    {
        get
        {
            return this.a_follow;
        }
        set
        {
            this.a_follow = value;
        }
    }
    [SerializeField]
    private WeaponController wpController;
    public WeaponController WpController
    {
        get
        {
            return this.wpController;
        }
        set
        {
            this.wpController = value;
        }
    }
    [SerializeField]
    private PlayerController playerCtrl;
    public PlayerController PlayerCtrl
    {
        get
        {
            return this.playerCtrl;
        }
        set
        {
            this.playerCtrl = value;
        }
    }
    private float horizontal = 0f;
    public float Horizontal { get; set; }
    private float vertical = 0f;
    public float Vertical { get; set; }
    [Header("Informations")]
    [SerializeField]
    private Vector3 moveDirection;
    public Vector3 MoveDirection
    {
        get
        {
            return this.moveDirection;
        }
        set
        {
            this.moveDirection = value;
        }
    }
    [SerializeField]
    private bool sprint = false;
    public bool Sprint
    {
        get
        {
            return this.sprint;
        }
        set
        {
            this.sprint = value;
        }
    }
    [SerializeField]
    private bool rolling = false;
    public bool Rolling
    {
        get
        {
            return this.rolling;
        }
        set
        {
            this.rolling = value;
        }
    }
    [SerializeField]
    private bool attacking = false;
    public bool Attacking
    {
        get
        {
            return this.attacking;
        }
        set
        {
            this.attacking = value;
        }
    }
    [SerializeField]
    private bool inAction = false;
    public bool InAction
    {
        get
        {
            return this.inAction;
        }
        set
        {
            this.inAction = value;
        }
    }
    [SerializeField]
    private bool isAttacking = false;
    [SerializeField]
    private bool isRolling = false;
    public bool IsRolling
    {
        get { return isRolling; }
        set { isRolling = value; }
    }
    [SerializeField]
    private bool hasSword = false;
    public bool HasSword
    {
        get => hasSword;
        set => hasSword = value;
    }
    [SerializeField]
    private bool pickUpSword = false;
    public bool PickUpSword
    {
        get
        {
            return this.pickUpSword;
        }
        set
        {
            this.pickUpSword = value;
        }
    }
    [SerializeField]
    private bool dropSword = false;
    public bool DropSword
    {
        get
        {
            return this.dropSword;
        }
        set
        {
            this.dropSword = value;
        }
    }
    [SerializeField]
    private bool isPickingUpSword = false;
    public bool IsPickingUpSword
    {
        get
        {
            return this.isPickingUpSword;
        }
        set
        {
            this.isPickingUpSword = value;
        }
    }
    public bool hasBeenHit = false;
    private bool damageDone = false;
    public bool isDamaged = false;
    private float rollingInvincibilityTimer = 0f;
    [Header("Actions parameters")]
    [SerializeField]
    private float rollingInvincibilityTimeLimit = 0f;
    private bool IsRollingInvincible = true;
    [SerializeField]
    private float radiusForSwordSearch = 3f;
    public float RadiusForSwordSearch
    {
        get
        {
            return this.radiusForSwordSearch;
        }
        set
        {
            this.radiusForSwordSearch = value;
        }
    }
    [SerializeField]
    private float whenAttackDealsDamageFactor = 0.17f;
    public float WhenAttackDealsDamageFactor
    {
        get => whenAttackDealsDamageFactor;
        set => whenAttackDealsDamageFactor = value;
    }
    [SerializeField] private float whenDropSwordFactor = 0.5f;
    [SerializeField]
    private float startRollingInvincibilityFactor = 0.3f;
    [SerializeField]
    private float endRollingInvincibilityFactor = 0.8f;
    private float attackTimer = 0f;
    private float attackTimeLimit = 0f;
    [Header("Debug")]
    public bool dropSwordAfterAttack = true;
    private float distToGround = 0f;
    private bool falling = false;
    private float radius = 0f;
    private float height = 0f;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        Init();
    }
    
    /// <summary>
    /// Initialize the player and all his components.
    /// </summary>
    public void Init()
    {
        SetUp();
        Rgbody.angularDrag = 999;
        Rgbody.drag = 0;
        Rgbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | Rgbody.constraints;

        //A_follow.Init(this);
    }

    private void Start()
    {
        distToGround = GetComponent<CapsuleCollider>().bounds.extents.y;
        radius = GetComponent<CapsuleCollider>().radius;
        height = GetComponent<CapsuleCollider>().height / 2;
    }
    
    /// <summary>
    /// Gather all the components of the StateManager
    /// </summary>
    private void SetUp()
    {
        if (model == null)
        {
            Anim = GetComponentInChildren<Animator>();
            if (Anim == null)
            {
                Debug.Log("No Animator found");
            }
            else
            {
                model = Anim.gameObject;
            }
        }
        if (Anim == null)
        {
            Anim = model.GetComponent<Animator>();
        }

        rgbody = GetComponent<Rigidbody>();

        /*a_follow = model.GetComponent<AnimatorFollow>();
        if(a_follow == null)
        {
            Debug.Log("No AnimatorFollow found");
        }*/

        playerCtrl = this.GetComponent<PlayerController>();
        WpController = WeaponController.Instance;
        if(WpController == null)
        {
            WpController = GetComponent<WeaponController>();
        }
    }

    /// <summary>
    /// Function which check is the player is grounded.
    /// </summary>
    /// <param name="distance">The max distance between the player and the ground for considering him as grounded </param>
    /// <returns> The result of the test </returns>
    private bool isGrounded(float distance)
    {
        Ray ray = new Ray(transform.position + new Vector3(0, distToGround,0), -Vector3.up);
        return Physics.SphereCast(ray, radius,distance);
    }

    /// <summary>
    /// Update the player state and move him or perform an action.
    /// </summary>
    public void UpdatePlayer()
    {
        //Debug
        Anim.SetBool("dropAfterAttack", dropSwordAfterAttack);

        inAction = !Anim.GetBool("canMove");
        Anim.applyRootMotion = inAction;

        //When the player just get hit, start the animation
        if (hasBeenHit)
        {
            anim.SetTrigger("isDamaged");
            hasBeenHit = false;
            isDamaged = true;
        }
        //The player is stun
        if (isDamaged)
        {
            return;
        }
        //If falling, slow speed
        if(falling)
        {
            speed_vector = Vector3.Lerp(rgbody.velocity, new Vector3(0, rgbody.velocity.y, 0), 0.3f);
            Anim.SetFloat("Speed", 0);
            rgbody.velocity = speed_vector;
            if (isGrounded(distToGround + 0.1f))
                falling = false;
            return;
        }
        //Check if grounded
        if (!isGrounded(distToGround + 2f))
        {
            falling = true;
            return;
        }

        //If in action, perform this action
        if (inAction)
        {
            if (isAttacking)
            {
                attackAction();
            } else if (isPickingUpSword)
            {
                if (rolling)
                {
                    isPickingUpSword = false;
                    Anim.SetBool("interruptedPickUp", true);
                    transform.rotation = Quaternion.LookRotation(moveDirection);
                    DetectAction();
                    return;
                }
            } else if (isRolling)
            {
                RollingAction();
            }
            return;
        }

        //Detect if an input has been pressed
        DetectAction();

        //If still in action, return.
        if (inAction)
        {
            return;
        }

        //Not in action, so just move the player
        if(moveDirection.x != 0 || moveDirection.z != 0)
        {
            Anim.SetBool("IsMoving", true);
        } else
        {
            Anim.SetBool("IsMoving", false);
        }
        if (sprint || !HasSword)
        {
            sprinting();
        }
        else
        {
            walking();
        }
        Anim.SetFloat("Speed", speed_vector.magnitude);
        rgbody.velocity = speed_vector;

        //Rotate the player.
        if (rgbody.velocity.magnitude > 0.5f)
        {
            if (moveDirection != Vector3.zero)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(moveDirection), Time.deltaTime * characterRotationFact);//Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveDirection), characterRotationFact);
        }
    }

    /// <summary>
    /// Uses the input for detecting and performing special action as attacking or rolling.
    /// </summary>
    public void DetectAction()
    {
        if (rolling == false && attacking == false && pickUpSword == false && DropSword == false && hasBeenHit == false)
        {
            return;
        }
        Anim.speed = 1f;
        string targetAnimation = null;
        if (rolling)
        {
            rollingController();
            targetAnimation = "rolling";
        } else
        if (attacking)
        {
            isAttacking = true;
            damageDone = false;
            attackTimer = 0f;
            targetAnimation = "Attack";
            anim.speed = playerCtrl.AttackSpeed;
        } else if (pickUpSword)
        {
            if (PickUp())
                targetAnimation = "PickUpSword";
            else
                return;
        } else if (DropSword)
        {
            if (HasSword)
            {
                WpController.DropSword();
            }
            return;
        }

        Anim.SetBool("canMove", false);

        if (string.IsNullOrEmpty(targetAnimation) == false)
        {
            inAction = true;
            Anim.CrossFade(targetAnimation, crossFadeFact);
        }
    }

    /// <summary>
    /// Set the speed vector of the player to his walking speed.
    /// </summary>
    private void walking()
    {
        Vector3 newVelocity = playerSpeed * moveDirection;
        newVelocity.y = rgbody.velocity.y;
        speed_vector = Vector3.Lerp(rgbody.velocity,newVelocity,0.4f);
    }

    /// <summary>
    /// Set the speed vector of the player to his sprinting speed.
    /// </summary>
    private void sprinting()
    {
        Vector3 newVelocity = sprintSpeed * moveDirection;
        newVelocity.y = rgbody.velocity.y;
        speed_vector = Vector3.Lerp(rgbody.velocity, newVelocity, 0.1f);
    }

    /// <summary>
    /// Function handling attack action of the player. It checks when to start dealing damage or dropping sword.
    /// </summary>
    private void attackAction()
    {
        attackTimer += Time.deltaTime;
        attackTimeLimit = anim.GetCurrentAnimatorStateInfo(0).length;
        if (attackTimer >= whenAttackDealsDamageFactor * attackTimeLimit && !damageDone)
        {
            playerCtrl.Attack();
            damageDone = true;
        } else if ( attackTimer >= whenDropSwordFactor * attackTimeLimit)
        {
            if (dropSwordAfterAttack)
                WpController.DropSword();
            isAttacking = false;
        }
    }

    /// <summary>
    /// Function handling rolling action of the player and its invincibility.
    /// </summary>
    private void RollingAction()
    {
        if(IsRollingInvincible)
        {
            rollingInvincibilityTimer += Time.deltaTime;
            rollingInvincibilityTimeLimit = anim.GetCurrentAnimatorStateInfo(0).length;//Note 0.89 = duration of transition between rolling/Idle.
            if (rollingInvincibilityTimer >= startRollingInvincibilityFactor * rollingInvincibilityTimeLimit && rollingInvincibilityTimer <= endRollingInvincibilityFactor * rollingInvincibilityTimeLimit)
            {
                PlayerController.Instance?.SetInvincible(true);
            }
            else
            {
                PlayerController.Instance?.SetInvincible(false);
            }   
        }
    }

    /// <summary>
    /// Check the animation speed for the rolling action, considering if the player has or not the sword.
    /// </summary>
    private void rollingController()
    {
        if (HasSword)
        {
            Anim.speed = 0.5f;
        } else
        {
            Anim.speed = 1f;
        }
        IsRollingInvincible = !PlayerCtrl.IsInvincible; //If player is already invincible, dont start it.
        rollingInvincibilityTimer = 0f;
    }

    /// <summary>
    /// Function handling the pick up action of the player.
    /// </summary>
    /// <returns></returns>
    private bool PickUp()
    {
        //Searching for sword
        PickUpSword = false;
        if (WpController.SearchSword(this.transform.position, RadiusForSwordSearch))
        {
            isPickingUpSword = true;
            Vector3 target = new Vector3(WpController.Sword.transform.position.x, this.transform.position.y, WpController.Sword.transform.position.z);
            this.transform.LookAt(target);
            return true;
        }
        //No sword, no pick up
        else
        {
            Debug.Log("Sword isn't near player");
            return false;
        }
    }
}
