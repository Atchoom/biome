﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for devise pick up object. Not implemented in the game yet.
/// </summary>
public class DeviseScript : MonoBehaviour
{
    public int value;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0.5f,0.5f,0.5f));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerController>().Devise_count += value;
            Destroy(this.gameObject);
        }
    }
}
