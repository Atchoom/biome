﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCameraController : MonoBehaviour
{
    private CameraManager cameraManager;
    [SerializeField] private float movingSpeed = 13f;
    [SerializeField] private float maxMovingSpeed = 25f;
    [SerializeField] private float rotationSpeed = 1f;

    /// <summary>
    /// Boolean for setting speed of DebugCamera. If true, use maxMovingSpeed.
    /// </summary>
    private bool maxSpeed = false;

    // Start is called before the first frame update
    void Start()
    {
        cameraManager = CameraManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        //If camera is Debug, update its position/rotation
        if (cameraManager.CurrentCamera == CameraType.DEBUG)
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
                maxSpeed = !maxSpeed;
            MoveCamera(maxSpeed ? maxMovingSpeed : movingSpeed);
            RotateCamera();
        }
    }

    /// <summary>
    /// Move Debug camera according to user's input, at speed speed. 
    /// </summary>
    /// <param name="speed"> Speed of DebugCamera </param>
    void MoveCamera(float speed)
    {
        float deltaTime = Time.deltaTime;
        transform.position = transform.position
            + deltaTime * speed * Input.GetAxis("Vertical") * transform.forward
            + deltaTime * speed * Input.GetAxis("Horizontal") * transform.right
            + deltaTime * speed * Input.GetAxis("Jump") * Vector3.up
            - deltaTime * speed * Input.GetAxis("Crouch") * Vector3.up;
    }
    /// <summary>
    /// Rotate DebugCamera according to mouse Input.
    /// </summary>
    void RotateCamera()
    {
        float h = Input.GetAxis("Mouse X");
        float v = Input.GetAxis("Mouse Y");
        transform.Rotate(- v * rotationSpeed, 0, 0);
        transform.Rotate(0, h * rotationSpeed, 0, Space.World);
    }
}
