﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for managing debug's input.
/// </summary>
public class DebugInput : MonoBehaviour
{
    public KeyCode Invincibility;
    public KeyCode Respawn;
    public KeyCode GetSword;
    public KeyCode HideLogChat;
    public KeyCode TogglePauseKey;
    public KeyCode StopEnemies;

    private PlayerController playerController;
    private GameManager gameManager;
    private WeaponController weaponController;

    private bool setInvincible;
    private bool isInvincible;
    private bool respawnPlayer;
    private bool getSword;
    private bool getHideChat;
    private bool pauseTime;
    private bool enemiesStopped;



    public event EventHandler ToggleDebugUI;

    void Start()
    {
        playerController = PlayerController.Instance;
        gameManager = GameManager.Instance;
        weaponController = WeaponController.Instance;
        ToggleDebugUI += gameManager.OnToggleDebugUI;
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        //use input to do debug things
        if (setInvincible)
            playerController.SetInvincible(!playerController.IsInvincible);
        if (respawnPlayer)
            gameManager.RespawnPlayer();
        if (getSword)
            weaponController.SwordToHand();
        if (getHideChat)
            ToggleDebugUI?.Invoke(this, EventArgs.Empty);
        if (pauseTime)
            Time.timeScale = 1 - Time.timeScale;
        if (enemiesStopped)
            GameManager.Instance.GetComponent<EnemySpawnManager>().stopAllEnemies();
    }
    void GetInput()
    {
        setInvincible = Input.GetKeyDown(Invincibility);
        respawnPlayer = Input.GetKeyDown(Respawn);
        getSword = Input.GetKeyDown(GetSword);
        getHideChat = Input.GetKeyDown(HideLogChat);
        pauseTime = Input.GetKeyDown(TogglePauseKey);
        enemiesStopped = Input.GetKeyDown(StopEnemies);
    }
}
