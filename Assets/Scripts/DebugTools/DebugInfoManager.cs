﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugInfoManager : MonoBehaviour
{
    public int logLines = 4;
    private List<string> currentLines = new List<string>();
    private Text frameDeltaTime;
    private float[] frameDeltaTimes = new float[10]; // data are milliseconds
    private int frameDeltaTimesIndex = 0;
    private Text logging;

    /// <summary>
    /// Positive value
    /// </summary>
    [SerializeField] private int healthAdded = 10;
    /// <summary>
    /// Negative value
    /// </summary>
    [SerializeField] private int healthRemoved = -10;
    private Button buttonHealthAdded;
    private Button buttonHealthRemoved;
    private Button buttonForceAdapt;

    public event EventHandler<ModifyPlayerHealthArgs> ModifyHealth;
    public event EventHandler ForceAdaptEvent;
    public event EventHandler ForceSpawnEvent;

    // Start is called before the first frame update
    void Start()
    {
        frameDeltaTime = GameObject.Find("FPS").GetComponent<Text>();
        frameDeltaTime.text = "";
        logging = GameObject.Find("LoggingText").GetComponent<Text>();
        logging.text = "";
        buttonHealthAdded = GameObject.Find("Add Health").GetComponent<Button>();
        buttonHealthRemoved = GameObject.Find("Remove Health").GetComponent<Button>();
        buttonHealthAdded.GetComponentInChildren<Text>().text = "Add " + healthAdded + " health";
        buttonHealthRemoved.GetComponentInChildren<Text>().text = "Remove " + Math.Abs(healthRemoved) + " health";
    }

    // Update is called once per frame
    void Update()
    {
        frameDeltaTimes[frameDeltaTimesIndex] = Time.deltaTime * 1000;
        frameDeltaTime.text = "FPS: "+(1000 / frameDeltaTimes[frameDeltaTimesIndex]).ToString("0")+"\n"
                                  +(frameDeltaTimes[frameDeltaTimesIndex]).ToString("0") + "ms";
        frameDeltaTimesIndex = (frameDeltaTimesIndex+1)%frameDeltaTimes.Length;
    }

    public void ReplaceLog(string text)
    {
        logging.text = text;
    }

    public void AddToLog(string text)
    {
        currentLines.Add(text);
        if (logging == null)
            return;
        logging.text = "";
        for (int i = 0; i < logLines; i++)
        {
            if (i < currentLines.Count && currentLines.Count <= logLines)
                logging.text += currentLines[i] + "\n";
            if (i < currentLines.Count && currentLines.Count > logLines)
                logging.text += currentLines[currentLines.Count - logLines + i] + "\n";
        }
    }

    public void AddHealth()
    {
        ModifyHealth?.Invoke(this, new ModifyPlayerHealthArgs(healthAdded));
    }

    public void RemoveHealth()
    {
        ModifyHealth?.Invoke(this, new ModifyPlayerHealthArgs(healthRemoved));
    }

    public void ForceAdapt()
    {
        ForceAdaptEvent?.Invoke(this, EventArgs.Empty);
    }

    public void ForceSpawn()
    {
        ForceSpawnEvent?.Invoke(this, EventArgs.Empty);
    }
}
