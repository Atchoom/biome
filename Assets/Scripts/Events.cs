using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Event args used for an event thrown when Character has died, be it Player or Enemy
/// Note : the two are used in different events, but use the same argument type
/// </summary>
public class CharacterDeathEventArgs : EventArgs
{
    public CharacterController characterController { get; set; }

    public CharacterDeathEventArgs(CharacterController characterController)
    {
        this.characterController = characterController;
    }
}

/// <summary>
/// Event args used for an event thrown when Character has taken damages, be it Player or Enemy
/// Note : the two are used in different events, but use the same argument type
/// </summary>
public class CharacterDamagesEventArgs : EventArgs
{
    public CharacterController characterController { get; set; }
    public int attackDamages { get; set; }
    public int healthLeft { get; set; }
    public List<StatusEffect> statusEffects { get; set; }

    public CharacterDamagesEventArgs(CharacterController characterController, int attackDamages, int healthLeft,
        List<StatusEffect> statusEffects)
    {
        this.characterController = characterController;
        this.attackDamages = attackDamages;
        this.healthLeft = healthLeft;
        this.statusEffects = statusEffects;
    }
}

/// <summary>
/// Event args used for an event thrown when an enemy changes its state
/// </summary>
public class SwitchMoveActionEventArgs : EventArgs
{
    public MoveAction CurrentMoveAction { get; set; }

    public bool isStunned;

    public SwitchMoveActionEventArgs(MoveAction currentMoveAction, bool isStunned)
    {
        this.CurrentMoveAction = currentMoveAction;
        this.isStunned = isStunned;
    }
}

//TODO version générique avec un class DATA générique
/// <summary>
/// Event args used for an event thrown to send EnemyData
/// </summary>
public class SendEnemyDataEventArgs : EventArgs
{
    public EnemyData data;
    public int numberOfAdaptation;

    public SendEnemyDataEventArgs(EnemyData data, int numberOfAdaptation)
    {
        this.data = data;
        this.numberOfAdaptation = numberOfAdaptation;
    }
}



/***********************
*
*   Events sent by ui menu (quit, restart, volume etc.)
*
************************/

public class FOVArgs: EventArgs
{
    public MenuScript menuScript {get; set;}
    public float fov {get; set;}

    public FOVArgs(MenuScript menuScript, float fov)
    {
        this.menuScript = menuScript;
        this.fov = fov;
    }

} /* field of view */
public class RestartArgs: EventArgs
{
    public MenuScript menuScript {get; set;}

    public RestartArgs(MenuScript menuScript)
    {
        this.menuScript = menuScript;
    }

}
public class QuitArgs: EventArgs
{
    public MenuScript menuScript {get; set;}
    public QuitArgs(MenuScript menuScript)
    {
        this.menuScript = menuScript;
    }

}
public class SoundArgs: EventArgs
{
    public MenuScript menuScript {get; set;}
    public float sound {get; set;}
    public SoundArgs(MenuScript menuScript, float sound)
    {
        this.menuScript = menuScript;
        this.sound = sound;
    }

}
public class SensitivityArgs: EventArgs
{
    public MenuScript menuScript {get; set;}
    public float sensitivity {get; set;}
    public SensitivityArgs(MenuScript menuScript, float sensitivity)
    {
        this.menuScript = menuScript;
        this.sensitivity = sensitivity;
    }

}
public class DrawArgs: EventArgs
{
    public MenuScript menuScript {get; set;}
    public float draw {get; set;}
    public DrawArgs(MenuScript menuScript, float draw)
    {
        this.menuScript = menuScript;
        this.draw = draw;
    }

} /* draw distance */

/// <summary>
/// relativeHealthAdded : health added to the character, may be negative or positive
///
/// Event sent to make the player change its health
/// </summary>
public class ModifyPlayerHealthArgs : EventArgs
{
    public int relativeHealthAdded { get; set; }

    public ModifyPlayerHealthArgs(int relativeHealthAdded)
    {
        this.relativeHealthAdded = relativeHealthAdded;
    }
}

/// <summary>
/// relativeHealthAdded : health added to the character, may be negative or positive
///
/// Event sent for instance to update the UI
/// </summary>
public class PlayerHealthChangeArgs : EventArgs
{
    public int relativeHealthAdded { get; set; }
    public int totalHealth { get; set; }

    public PlayerHealthChangeArgs(int relativeHealthAdded, int totalHealth)
    {
        this.relativeHealthAdded = relativeHealthAdded;
        this.totalHealth = totalHealth;
    }
}
