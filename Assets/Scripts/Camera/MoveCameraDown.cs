﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraDown : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private bool moving = false;

    [SerializeField]
    private Transform targetPosition;

    [SerializeField]
    private uint stepNumber;

    [SerializeField]
    private float heightThreshold = 5f;

    private IEnumerator EndMovement()
    {
        // each step of the final movement separated by a yield return null;
        Vector3 initialPosition = transform.position;
        Quaternion initialRotation = transform.rotation;
        for (uint i = 0 ; i < stepNumber ; i++)
        {
            float lerpProgress = i / (float) stepNumber;
            Quaternion rotation =
                Quaternion.Lerp(initialRotation, targetPosition.rotation, lerpProgress);
            Vector3 position = Vector3.Lerp(initialPosition, targetPosition.position, lerpProgress);
            transform.SetPositionAndRotation(position, rotation);
            yield return null;
        }
    }
    
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.L))
            moving = true;
        if (moving)
        {
            if (transform.position.y <= heightThreshold)
            {
                moving = false;
                StartCoroutine(nameof(EndMovement));
            }
            Vector3 nextPos = transform.position - new Vector3(0, speed * Time.deltaTime, 0);
            transform.position = nextPos;
        }
    }
}
