﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// All the types of camera. MAIN is the camera of the player, DEBUG is a moving and controllable camera and GLOBAL a fix camera
/// </summary>
public enum CameraType {
    MAIN,
    DEBUG,
    GLOBAL
}

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance { get; private set; }

    [SerializeField]
    private GameObject debugCamera;

    [SerializeField]
    private GameObject mainCamera;

    [SerializeField]
    private GameObject globalDebugCamera;

    private CameraType currentCamera;
    public CameraType CurrentCamera => currentCamera;

    private bool wasCameraButtonPressed;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        currentCamera = CameraType.MAIN;
        wasCameraButtonPressed = false;
    }

    void Start()
    {
        debugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
        mainCamera.GetComponent<UnityEngine.Camera>().enabled = true;
        if(globalDebugCamera != null) globalDebugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        // we switch camera upon release
        if (wasCameraButtonPressed && Input.GetAxis("CameraSwitch") == 0) {
            SwitchCamera();
        }
        // ternary operator : if the button is pressed, then wasPressed = 1 basically
        wasCameraButtonPressed = Input.GetAxis("CameraSwitch") == 1 ? true : false;
    }

    /// <summary>
    /// Switch camera in this order : MAIN-DEBUG-GLOBAL
    /// </summary>
    void SwitchCamera()
    {
        switch (currentCamera) {
            case CameraType.MAIN:
                currentCamera = CameraType.DEBUG;
                mainCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                debugCamera.GetComponent<UnityEngine.Camera>().enabled = true;
                if (globalDebugCamera != null) globalDebugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                break;
            case CameraType.DEBUG:
                if (globalDebugCamera != null)
                {
                    currentCamera = CameraType.GLOBAL;
                    debugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                    mainCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                    globalDebugCamera.GetComponent<UnityEngine.Camera>().enabled = true;
                } else
                {
                    currentCamera = CameraType.MAIN;
                    mainCamera.GetComponent<UnityEngine.Camera>().enabled = true;
                    debugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                    if (globalDebugCamera != null) globalDebugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                    break;
                }
                break;
            case CameraType.GLOBAL:
                currentCamera = CameraType.MAIN;
                mainCamera.GetComponent<UnityEngine.Camera>().enabled = true;
                debugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                if (globalDebugCamera != null) globalDebugCamera.GetComponent<UnityEngine.Camera>().enabled = false;
                break;
        }
    }
}
