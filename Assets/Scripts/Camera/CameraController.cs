﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Contrôle de la caméra du joueur.
 **/

public class CameraController : MonoBehaviour
{    
    public static CameraController singleton;
    private void Awake()
    {
        singleton = this;
    }

    [Header("General")]
    public float mouseSpeed = 2f;
    public float cameraSpeed = 7f;
    public float followSpeed = 9f;
    public Transform target;
    [Header("Rotation")]
    public float smoothRotationCameraFact = 0.1f;
    public float minAngleY = -35f;
    public float maxAngleY = 35f;

    private Transform pivot;//Le pivot, qui permet les rotations verticales
    private Transform cameraTransform;//la position de la camera, qui permet les rotations horizontales
    [SerializeField]private float smoothX;
    [SerializeField] private float smoothY;
    [SerializeField] private float smoothXVelocity;
    [SerializeField] private float smoothYVelocity;
    [SerializeField] private float lookAngle;
    [SerializeField] private float tiltAngle;

    [Header("Zoom")]
    public float ZoomSpeed = 2f;
    [Range(0,180)]
    public float minZoom = 1f;
    [Range(0, 180)]
    public float maxZoom = 180f;

    /// <summary>
    /// Initialize the cameraController, with the target's transform.
    /// </summary>
    /// <param name="t"> Target's transform of the cameraController </param>
    public void Init(Transform t)
    {
        target = t;
        cameraTransform = Camera.main.transform;
        pivot = cameraTransform.parent;
        if(pivot == null)
        {
            Debug.Log("No pivot to camera");
        }
    }

    /// <summary>
    /// Camera update function. It takes mouse Input and update camera position, rotation and zoom
    /// </summary>
    /// <param name="d"> the Time.deltaTime </param>
    public void CameraUpdate(float d)
    {
        float h = Input.GetAxis("Mouse X");
        float v = Input.GetAxis("Mouse Y");
        float wheelRoll = Input.GetAxis("Mouse ScrollWheel");

        FollowTarget(d);
        FollowRotation(d, h, v);
        ZoomCamera(wheelRoll);
    }

    /// <summary>
    /// This function update the cameraController position in order to follow the target movements (the player)
    /// </summary>
    /// <param name="deltaT"> the Time.deltaTime </param>
    public void FollowTarget(float deltaT)
    {
        float speed = deltaT * followSpeed;
        Vector3 newPosition = Vector3.Lerp(transform.position, target.position, speed);
        transform.position = newPosition;
    }

    /// <summary>
    /// Function which enables the camera to follow the rotation of the mouse around the target (the player)
    /// </summary>
    /// <param name="deltaT"> the Time.deltaTime </param>
    /// <param name="h"> The mouse X input </param>
    /// <param name="v"> The mouse Y input </param>
    void FollowRotation(float deltaT, float h, float v)
    {
        smoothX = Mathf.SmoothDamp(smoothX, h, ref smoothXVelocity, smoothRotationCameraFact);
        smoothY = Mathf.SmoothDamp(smoothY, v, ref smoothYVelocity, smoothRotationCameraFact);

        lookAngle += smoothX * mouseSpeed;
        transform.rotation = Quaternion.Euler(0, lookAngle, 0);

        tiltAngle -= smoothY * mouseSpeed;
        tiltAngle = Mathf.Clamp(tiltAngle, minAngleY, maxAngleY);
        pivot.localRotation = Quaternion.Euler(tiltAngle, 0, 0);

    }

    /// <summary>
    /// Function for zooming camera
    /// </summary>
    /// <param name="scroll"> Zoom speed </param>
    void ZoomCamera(float scroll)
    {
        if(scroll != 0.0f)
        {
            Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView - scroll * ZoomSpeed,minZoom,maxZoom);
        }
    }
}
