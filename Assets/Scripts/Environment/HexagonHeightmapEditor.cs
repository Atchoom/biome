﻿#if UNITY_EDITOR // ignore on build
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HexagonHeightmap))]
 class HexagonHeightmapEditor : Editor
 {
     private Shader _shaderToApply = null;
     
     public override void OnInspectorGUI()
     {
         if (Application.isPlaying)
         {
             HexagonHeightmap hexagonHeightmap = (HexagonHeightmap) target;
             _shaderToApply = (Shader) EditorGUILayout.ObjectField(_shaderToApply, typeof(Shader), false);
             if (GUILayout.Button("Apply Shader"))
             {
                 if (_shaderToApply != null)
                     hexagonHeightmap.OnButtonApplyShader(_shaderToApply);
                 else
                     Debug.LogWarning("You must specify a shader");
             }
         }
         DrawDefaultInspector();
     }
 }
#endif
