﻿using System;
using System.Linq;
using UnityEngine;

public class AxialCoordinates
{
    protected int _q;
    protected int _r;

    public enum Directions
    {
        DOWN_LEFT,
        DOWN,
        DOWN_RIGHT,
        UP_RIGHT,
        UP,
        UP_LEFT
    };
    
    public static readonly AxialCoordinates Zero = new AxialCoordinates(0,0);
    public static readonly AxialCoordinates DownRight = new AxialCoordinates(1,0);
    public static readonly AxialCoordinates UpRight = new AxialCoordinates(1,1);
    public static readonly AxialCoordinates Up = new AxialCoordinates(0,1);
    public static readonly AxialCoordinates UpLeft = -DownRight;
    public static readonly AxialCoordinates DownLeft = -UpRight;
    public static readonly AxialCoordinates Down = -Up;

    public static readonly AxialCoordinates[] DirectionTab = {DownRight, UpRight, Up, UpLeft, DownLeft, Down};
    public static uint ModulusDirection(int direction) { return (uint) ((direction%6 + 6) % 6); }
    
    public int q
    {
        get => _q;
        set => _q = value;
    }

    public int r
    {
        get => _r;
        set => _r = value;
    }

    public int s
    {
        get => _r - _q;
    }

    public AxialCoordinates()
    {
        _q = 0;
        _r = 0;
    }
    
    public AxialCoordinates(int q, int r)
    {
        _q = q;
        _r = r;
    }

    public AxialCoordinates(AxialCoordinates other)
    {
        _q = other.q;
        _r = other.r;
    }
    
    // returns the coordinates in a 2D space in which q grows with the x axis and r with x and y (approx.)
    public Vector2 GetPlaneCoordinates(float scale, uint tileRadius = 0)
    {
        if (tileRadius == 0)
            return new Vector2(scale*1.5f*q, scale*(r+s));
        return GetTileCenter(tileRadius).GetPlaneCoordinates(scale);
    }

    public AxialCoordinates GetTileCenter(uint tileRadius)
    {
        if (tileRadius == 0)
            return this;
        return new AxialCoordinates(((int)tileRadius+1)*_q + (int)tileRadius*_r, -(int)tileRadius*_q + (2*(int)tileRadius+1)*_r);
    }

    // this function is highly inconsistent
    public AxialCoordinates GetTilePosition(uint tileRadius)
    {
        if (tileRadius == 0)
            return this;
        float quotient = 3 * (int) tileRadius * ((int) tileRadius + 1) + 1;
        int tile_q = Mathf.RoundToInt((2 + _q * (int) tileRadius + _q - _r * (int) tileRadius) / quotient);
        int tile_r = Mathf.RoundToInt(((int) tileRadius * (_q + _r) + _r) / quotient);
        // this is really not precised close to tile edges
        AxialCoordinates result_guess = new AxialCoordinates(tile_q, tile_r);
        AxialCoordinates localThis = this - result_guess.GetTileCenter(tileRadius);
        if (localThis.GetAxialRadius() <= tileRadius)
            return result_guess;
        for (uint i = 0; i < 6; i++)
        {
            localThis = this - (result_guess + DirectionTab[i]).GetTileCenter(tileRadius);
            if (localThis.GetAxialRadius() <= tileRadius)
                return result_guess+DirectionTab[i];
        }
        // should not be possible, but it happens
        return null;
    }

    public int GetAxialRadius()
    {
        return (Math.Abs(q) + Math.Abs(r) + Math.Abs(s)) / 2;
    }

    public float GetPlaneRadius(float scale)
    {
        return GetPlaneCoordinates(scale).magnitude;
    }

    public static AxialCoordinates operator+(AxialCoordinates a) => a;
    public static AxialCoordinates operator-(AxialCoordinates a) => new AxialCoordinates(-a.q,-a.r);
    public static AxialCoordinates operator+(AxialCoordinates a, AxialCoordinates b)
        => new AxialCoordinates(a.q + b.q, a.r+b.r);
    public static AxialCoordinates operator-(AxialCoordinates a, AxialCoordinates b)
        => a + (-b);

    public static AxialCoordinates operator *(AxialCoordinates a, float b)
    {
        AxialCoordinates c = new AxialCoordinates(a);
        c.q = (int) (c.q * b);
        c.r = (int) (c.r * b);
        return c;
    }

    public static AxialCoordinates operator /(AxialCoordinates a, float b)
    {
        return a * (1.0f / b);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(obj, null)) return false;
        if (obj.GetType() != GetType()) return false;
        AxialCoordinates other = (AxialCoordinates) obj;
        return q == other.q && r == other.r;
    }

    public static bool operator ==(AxialCoordinates a, AxialCoordinates b)
    {
        if (ReferenceEquals(a,b)) return true;
        if (ReferenceEquals(a,null)) return false;
        if (ReferenceEquals(b,null)) return false;
        return a.Equals(b);
    }
    public static bool operator !=(AxialCoordinates a, AxialCoordinates b)
        => !(a == b);

    // returns the axial coordinates of the hexagon containing the point at given plane coordinates
    public static AxialCoordinates PlaneToAxial(Vector2 plane, float scale, uint tileRadius = 0)
    {
        if (tileRadius == 0)
            return new AxialCoordinates(Mathf.RoundToInt(2*plane.x/(3*scale)), Mathf.RoundToInt((plane.x/3+plane.y/2)/scale));
        float divider = (3 * tileRadius * tileRadius + 3 * tileRadius + 1)*scale;
        return new AxialCoordinates(Mathf.RoundToInt(((plane.x*(3*tileRadius+2)/3) - plane.y*tileRadius/2)/divider), 
            Mathf.RoundToInt( (plane.x*(3*tileRadius+1)/3 + plane.y*(tileRadius+1)/2)  / divider));
    }

    // returns the plane coordinates of the center of the hexagon designated by the axial coordinates
    public static Vector2 AxialToPlane(AxialCoordinates axial, float scale)
    {
        return axial.GetPlaneCoordinates(scale);
    }

    public static int GetDistance(AxialCoordinates a, AxialCoordinates b)
    {
        return (a-b).GetAxialRadius();
    }

    /// <summary>
    /// Finds the neighbor of one hexagon according to a given direction.
    /// Used in the Ring and Spiral methods.
    /// </summary>
    /// <param name="dir"> 0<dir<5 </param>
    /// <returns></returns>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public AxialCoordinates Neighbor(uint dir)
    {
        switch (dir)
        {
            case 0:
                return this + DownLeft;
            case 1:
                return this + Down;
            case 2:
                return this + DownRight;
            case 3:
                return this + UpRight;
            case 4:
                return this + Up;
            case 5:
                return this + UpLeft;
            default:
                throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
        }
    }
    
    /// <summary>
    /// Computes coordinnates of the hexagons arond the center and distanced by the radius.
    /// Used in the Spiral method.
    /// <remarks>
    /// Does not work for radius = 0
    /// </remarks>
    /// </summary>
    /// <param name="center"></param>
    /// <param name="radius">have to be strictly positive</param>
    /// <returns>
    /// An array containing the coordinates of desired hexagons
    /// </returns>
    public static AxialCoordinates[] Ring(AxialCoordinates center, uint radius)
    {
        AxialCoordinates[] results = new AxialCoordinates[6*radius];
        AxialCoordinates current = center;
        
        for (uint j = 0; j < radius; ++j)
        {
            current = current.Neighbor(4);
        }

        for (uint i = 0; i < 6; ++i)
        {
            for (uint j = 0; j < radius; ++j)
            {
                results[i * radius + j] = current;
                current = current.Neighbor(i);
            }
        }

        return results;
    }

    /// <summary>
    /// Computes coordinnates of the hexagons around the center at a distance less than or equal to the radius.
    /// Used to find neighbors hexagons.
    /// </summary>
    /// <param name="center"></param>
    /// <param name="radius"></param>
    /// <returns>
    /// An array containing the coordinates of desired hexagons
    /// </returns>
    public static AxialCoordinates[] Spiral(AxialCoordinates center, uint radius)
    {
        AxialCoordinates[] results = {center};
        for (uint i = 1; i < radius+1; ++i)
        {
            results = results.Concat(Ring(center, i)).ToArray();
        }

        return results;
    }
}
