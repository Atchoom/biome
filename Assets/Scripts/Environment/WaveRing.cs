﻿using System;
using UnityEngine;

/// <summary>
/// This data class represent a biome with informations about relief, objects and enemies.
/// </summary>
[Serializable]
public class WaveRing
{
    // Environment
    public uint ringWidth;
    public float height;
    [Range(0,1f)]
    public float waveSmoothingRatio = 0.1f;
    [Range(0,1f)]
    public float depthSmoothingRatio = 0.1f;
    public Color color;
    public Wave[] waves;
    [NonSerialized]
    public uint ringRadius;
    
    // Objects
    public ObjectToGenerate[] biomeObjects;

    // Gameplay
    public EnemyBiomeData[] enemyTypes;
    public int biomeID;
    public String BiomeName;
}
