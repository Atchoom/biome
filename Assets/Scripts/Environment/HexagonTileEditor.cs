﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HexagonTile))]
 class HexagonTileEditor : Editor
 {
     public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Deactivate"))
            ((HexagonTile) target).SetActive(false);
        if (GUILayout.Button("Make flat"))
            ((HexagonTile) target).MakeFlat();
        if (GUILayout.Button("Elevate Tile Center"))
            ((HexagonTile) target).ElevateTileCenter();
        if (GUILayout.Button("Elevate Tile Border"))
            ((HexagonTile) target).ElevateTileBorder();
        if (GUILayout.Button("Temp Elevate Tile Center"))
            ((HexagonTile) target).TemporaryElevateTileCenter();
        DrawDefaultInspector();
    }
}

#endif
