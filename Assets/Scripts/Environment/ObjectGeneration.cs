﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// This class is used to determine object placement.
/// </summary>
public class ObjectGeneration : MonoBehaviour
{
    [SerializeField] private uint smoothLookingRange = 4;
    [SerializeField] private float smoothFlatWeight = 4;
    [SerializeField] private Wave[] waves;

    private HexagonHeightmap _hexagonHeightmap;
    
    /// <summary>
    /// Sets up the script by getting the instance of HexagonHeightmap.
    /// </summary>
    public void SetUp()
    {               
        _hexagonHeightmap = HexagonHeightmap.GetInstance();
    }

    /// <summary>
    /// Determines whether or not an object should be placed on the given hexagon.
    /// </summary>
    /// <param name="hexInfo"></param>
    /// <param name="tile"></param>
    /// <param name="tileScale"></param>
    public void Generate(HexInfo hexInfo, ref GameObject tile, float tileScale)
    {
        if(hexInfo.isEmpty>0) return;

        Vector3 position = hexInfo.GlobalCoordinates;

        if (Math.Abs(position.y) > 0.1)
        {
            AxialCoordinates center = hexInfo.GlobalAxialCoordinates;

            HexagonTile hexagonTile = tile.GetComponent<HexagonTile>();
            if (hexagonTile==null)
            {
                return;
            }
            
            float centerValue = GetNoise(center, tileScale);

            int range = _hexagonHeightmap.waveRings[hexInfo.biome].biomeObjects.Length;
            if (range == 0)
                return;

            uint totalWeight = 0;
            for(int i = 0; i < range; i++)
            {
                totalWeight += _hexagonHeightmap.waveRings[hexInfo.biome].biomeObjects[i].ProbabilityWeight;
            }

            int weightValue = Random.Range(0, (int)totalWeight);
            totalWeight = 0;
            int k = 0;
            while (k < range)
            {
                totalWeight += _hexagonHeightmap.waveRings[hexInfo.biome].biomeObjects[k].ProbabilityWeight;
                if (totalWeight > weightValue)
                    break;
                ++k;
            }
            
            ObjectToGenerate objectToGenerate = _hexagonHeightmap.waveRings[hexInfo.biome].biomeObjects[k];
            ObjectPrefabProperties objectPrefabProperties =
                objectToGenerate.Object.GetComponent<ObjectPrefabProperties>();
            uint lookingRadius = objectToGenerate.LookingRadius;
            uint smoothingRadius = objectPrefabProperties.smoothingRadius;


            AxialCoordinates[] neighbors = AxialCoordinates.Spiral(center, Math.Max(lookingRadius, smoothingRadius)).Skip(1).ToArray();

            foreach (var neighbor in neighbors)
            {
                if (hexagonTile.GetGlobalHex(neighbor).isEmpty==HexInfo.ObjectStatus.Empty)
                {
                    float neighborValue = GetNoise(neighbor, tileScale);
                    if (neighborValue > centerValue)
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            hexInfo.isEmpty = HexInfo.ObjectStatus.ObjectOnIt;
            hexInfo.objectOnIt = objectToGenerate.Object;
            hexInfo.ObjectAngle = Random.Range(-360,360);
            hexagonTile.SetGlobalHex(center, ref hexInfo);
            
            for (int i = 0; i < 3 * smoothingRadius * (smoothingRadius + 1); ++i)
            {
                HexInfo currentNeighbour = hexagonTile.GetGlobalHex(neighbors[i]);
                if (!objectPrefabProperties.isSmallObject)
                {
                    currentNeighbour.isEmpty = HexInfo.ObjectStatus.SmoothArea;
                    // get hexagon position relative to object
                    Vector3 neighbourCenterRelativeToRotatedObject =
                        (currentNeighbour.GlobalCoordinates - hexInfo.GlobalCoordinates);
                    // make sure height has no influence
                    neighbourCenterRelativeToRotatedObject.y = 0;
                    // rotate
                    neighbourCenterRelativeToRotatedObject =
                        Quaternion.AngleAxis(-hexInfo.ObjectAngle, Vector3.up) * neighbourCenterRelativeToRotatedObject;
                    if (objectPrefabProperties.IsPointInsideBoundaries(neighbourCenterRelativeToRotatedObject))
                    {
                        currentNeighbour.isEmpty = HexInfo.ObjectStatus.ObjectOnIt;
                        currentNeighbour.Height = hexInfo.Height;
                    }

                }
                else
                {
                    currentNeighbour.isEmpty = HexInfo.ObjectStatus.SmoothArea;
                }
                hexagonTile.SetGlobalHex(neighbors[i], ref currentNeighbour);
            }

            if (objectPrefabProperties.isSmallObject)
            {
                // do smooth
                for (int i = 0; i < 3 * smoothingRadius * (smoothingRadius + 1); ++i)
                {
                    SmoothHex(neighbors[i], ref hexagonTile);
                }
            }

            return;
        }
    }

    /// <summary>
    /// Smoothes the height of the hexagon corresponding to the coordinates by calculating the average of the heights of its neighbours.
    /// </summary>
    /// <param name="globalCoordinates"></param>
    /// <param name="hexagonTile"></param>
    private void SmoothHex(AxialCoordinates globalCoordinates, ref HexagonTile hexagonTile)
    {
        AxialCoordinates[] neighbors = AxialCoordinates.Spiral(globalCoordinates, smoothLookingRange);
        HexInfo h = hexagonTile.GetGlobalHex(globalCoordinates);
        float totalWeight = 0;
        if (h.isEmpty == HexInfo.ObjectStatus.SmoothArea)
        {
            float height = 0;
            foreach (AxialCoordinates neighbor in neighbors)
            {
                HexInfo neighborInfo = hexagonTile.GetGlobalHex(neighbor);
                if (neighborInfo.isEmpty == HexInfo.ObjectStatus.ObjectOnIt)
                {
                    totalWeight += smoothFlatWeight;
                    height += smoothFlatWeight * neighborInfo.Height;
                }
                else
                {
                    totalWeight += 1;
                    height += neighborInfo.Height;
                }
            }

            h.Height = height / totalWeight;
            hexagonTile.SetGlobalHex(h.GlobalAxialCoordinates, ref h);
        }
    }

    /// <summary>
    /// Computes Perlin noise according to the waves
    /// </summary>
    /// <param name="coordinates">Global position</param>
    /// <param name="tileScale"></param>
    /// <returns>
    /// A float reprensenting the value of noise at these coordinates
    /// </returns>
    private float GetNoise(AxialCoordinates coordinates, float tileScale)
    {
        float noise = 0f;
        float normalization = 0f;

        Vector2 vec = coordinates.GetPlaneCoordinates(tileScale);
        // Vector2 vec = new Vector2(coordinates.q, coordinates.r) / tileScale * mapScale;

        foreach (Wave wave in waves) {
            // generate noise value using PerlinNoise for a given Wave
            noise += wave.Amplitude * Mathf.PerlinNoise (vec.x * wave.Frequency + wave.Seed, vec.y * wave.Frequency + wave.Seed);
            normalization += wave.Amplitude;
        }

        return noise / normalization;
    }
}