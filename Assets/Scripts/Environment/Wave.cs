﻿using System;
using UnityEngine;


/// <summary>
/// This data class represents variation used in Perlin noise calculation
/// </summary>
[Serializable]
public class Wave
{
    [SerializeField]
    private float seed;
    public float Seed
    {
        get => seed;
        set => seed = value;
    }

    [SerializeField]
    private float frequency;

    public float Frequency
    {
        get => frequency;
        set => frequency = value;
    }

    [SerializeField]
    private float amplitude;
    
    public float Amplitude
    {
        get => amplitude;
        set => amplitude = value;
    }
}