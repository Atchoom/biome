﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;

// TODO : replace all transform.Find and transform.GetChild
public class HexagonHeightmap : MonoBehaviour
{
    // radius represents the number of hexagons below the one in the center for example
    [Header("General Settings")] [SerializeField]
    private Material material;

    [SerializeField] private GameObject followedObject;
    [SerializeField] private uint tilesAroundObject = 1;
    private AxialCoordinates followedObjectPosition = new AxialCoordinates();

    [SerializeField] private ObjectGeneration treeGeneration;
    [SerializeField] private int seed;

    [Header("Tile Settings")] [SerializeField] [Range(1, 134)]
    private uint tileRadius = 2;
    public uint TileRadius { get => tileRadius; }

    // scale can be set in order to set the object scale on x and z axis, but does not affect y scale
    [SerializeField] private float tileScale = 1;
    public float TileScale { get => tileScale; }
    private HexagonTile[][] _hexagonTiles;

    // this should be replaced later
    [Header("Map Settings")] [Tooltip("Total Map Radius")] [SerializeField]
    private uint mapRadius = 6;
    [Tooltip("Map Radius on Start")] [SerializeField]
    private uint initialMapRadius = 1;

    [SerializeField] private uint islandRadius = 1;
    public uint IslandRadius { get => islandRadius; }

    // Heightmap Generation
    [Header("Heightmap Generation Settings")] [SerializeField]
    private float heightMultiplier;
    public float HeightMultiplier { get => heightMultiplier; }

    [SerializeField] [Range(0.01f, 100f)] private float meanderingPercent = 10f;

    [SerializeField] private float mapScale;
    public float MapScale { get => mapScale; }

    [SerializeField] public WaveRing[] waveRings;

    private bool firstGeneration = false;

    public bool FirstGeneration
    {
        get => firstGeneration;
        private set => firstGeneration = value;
    }

    private static HexagonHeightmap _instance;

    public static HexagonHeightmap GetInstance()
    {
        return _instance;
    }

    public AxialCoordinates FollowedObjectPosition
    {
        get  => followedObjectPosition;
    }

    public uint TilesAroundObject
    {
        get => tilesAroundObject;
    }

    public event EventHandler FirstGenerationDone;

    private void Awake()
    {
        if (_instance==null)
            _instance = this;
        else
            Debug.LogError("HexagonHeightmap must be a singleton");
    }

    private void Start()
    {
        Debug.Assert(tileRadius > 0);
        _hexagonTiles = new HexagonTile[2 * mapRadius + 1][];
        for (uint q = 0; q < 2 * mapRadius + 1; q++)
        {
            uint rowHexNumber = (uint) (2 * mapRadius + 1 - Math.Abs(q - (int) mapRadius));
            _hexagonTiles[q] = new HexagonTile[rowHexNumber];
        }
        Debug.Assert(initialMapRadius <= mapRadius && initialMapRadius >= tilesAroundObject);
        OnButtonGenerate();
    }



    public HexagonTile GetTileAt(AxialCoordinates tilePosition)
    {
        if (tilePosition.GetAxialRadius() <= mapRadius)
        {
            uint array_q = (uint) (tilePosition.q + (int) mapRadius);
            uint array_r = (uint) (tilePosition.r + (int) mapRadius - Math.Max(0, tilePosition.q));
            if (ReferenceEquals(_hexagonTiles[array_q][array_r], null))
            {
                GameObject newTile = new GameObject();
                newTile.transform.parent = transform;
                newTile.layer = gameObject.layer;
                _hexagonTiles[array_q][array_r] = newTile.AddComponent<HexagonTile>();
                _hexagonTiles[array_q][array_r].treeGeneration = treeGeneration;
            }

            return _hexagonTiles[array_q][array_r];
        }
        // the tile will not be stored in the array and thus will be accessible by Find
        var tileGameObject = transform.Find(GetChildName(tilePosition));
        if (ReferenceEquals(tileGameObject, null))
        {
            GameObject newTile = new GameObject();
            // we want to be sure to do this before everything else, since we use the name to find it
            // otherwise, we could generate the same tile several times
            newTile.name = GetChildName(tilePosition);
            newTile.transform.parent = transform;
            newTile.layer = gameObject.layer;
            HexagonTile tile = newTile.AddComponent<HexagonTile>();
            tile.treeGeneration = treeGeneration;
            return tile;
        }

        return tileGameObject.GetComponent<HexagonTile>();
    }

    // this function should not be used, since the GetTilePosition it uses is really inconsistent
    public HexInfo GetInfoAt(AxialCoordinates globalPosition)
    {
        AxialCoordinates tileCoordinates = globalPosition.GetTilePosition(tileRadius);
        HexagonTile tile = GetTileAt(tileCoordinates);
        if (ReferenceEquals(tile, null)) return new HexInfo();
        return tile.GetLocalHex(globalPosition - tileCoordinates.GetTileCenter(tileRadius));
    }

    public HexInfo GetInfoAt(AxialCoordinates tileCoordinates, AxialCoordinates hexagonLocalCoordinates)
    {
        HexagonTile tile = GetTileAt(tileCoordinates);
        if (ReferenceEquals(tile, null)) return new HexInfo();
        return tile.GetLocalHex(hexagonLocalCoordinates);
    }

    public void SetInfoAt(AxialCoordinates tileCoordinates, AxialCoordinates hexagonLocalCoordinates, ref HexInfo hexInfo)
    {
        if (tileCoordinates.GetAxialRadius() <= mapRadius)
        {
            uint array_q = (uint) (tileCoordinates.q + (int) mapRadius);
            uint array_r = (uint) (tileCoordinates.r + (int) mapRadius - Math.Max(0, tileCoordinates.q));
            if (!ReferenceEquals(_hexagonTiles[array_q][array_r], null))
            {
                _hexagonTiles[array_q][array_r].SetLocalHex(hexagonLocalCoordinates, ref hexInfo);
            }
        }
        else
        {
            var tileGameObject = transform.Find(GetChildName(tileCoordinates));
            if (!ReferenceEquals(tileGameObject, null))
            {
                HexagonTile tile = tileGameObject.GetComponent<HexagonTile>();
                if (!ReferenceEquals(tile, null))
                {
                    tile.SetLocalHex(hexagonLocalCoordinates, ref hexInfo);
                }
            }
        }
    }

    private void Update()
    {
        if(firstGeneration)
            UpdateFollow();
    }

    public static string GetChildName(AxialCoordinates coordinates)
    {
        return "Tile (" + coordinates.q + ", " + coordinates.r + ")";
    }

    private void ValidateHeightMapData()
    {
        ComputeRingsRadius();
        Debug.AssertFormat(waveRings[waveRings.Length - 1].ringRadius >= islandRadius,
            "Your rings do not cover the whole island ({0}/{1})", waveRings[waveRings.Length - 1].ringRadius,
            islandRadius);
        Debug.Assert(waveRings.Length > 0, "There has to be at least one wave ring");
        for (uint i = 0; i < waveRings.Length - 1; i++)
        {
            Debug.AssertFormat(waveRings[i].waves.Length > 0, "Ring {0} has no waves defined", i);
            Debug.AssertFormat(waveRings[i].ringRadius <= islandRadius,
                "Ring {0} goes beyond the end of the island ({1}/{2})",
                i, waveRings[i].ringRadius, islandRadius);
        }
    }

    private void ComputeRingsRadius()
    {
        uint ringRadius = 0;
        foreach (WaveRing waveRing in waveRings)
        {
            ringRadius += waveRing.ringWidth;
            waveRing.ringRadius = ringRadius;
        }

        islandRadius = waveRings[waveRings.Length - 1].ringRadius;
    }

    public void DestroyTiles()
    {
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    private HexagonTile[] GetNeighbours(AxialCoordinates position)
    {
        HexagonTile[] neighbours = new HexagonTile[6];
        for (uint i = 0; i < 6; i++)
            neighbours[i] = GetTileAt(position + AxialCoordinates.DirectionTab[i]);
        return neighbours;
    }

    private void MeshSetupTileAt(AxialCoordinates position)
    {
        HexagonTile[] neighbours = GetNeighbours(position);
        for (uint i = 0; i < 6; i++)
        {
            if (!ReferenceEquals(neighbours[i], null) && !neighbours[i].IsSetup)
            {
                neighbours[i].Setup(position + AxialCoordinates.DirectionTab[i], waveRings);
            }
        }

        HexagonTile tile = GetTileAt(position);
        if (!tile.IsSetup)
        {
            tile.Setup(position, waveRings);
        }

        tile.ObjectSetup();
        tile.MeshSetup(ref material);
    }

    private IEnumerator WaitEndOfInitialGeneration()
    {
        bool hasFinished;
        AxialCoordinates[] initialGenerationDiskCoordinates = AxialCoordinates.Spiral(AxialCoordinates.Zero, initialMapRadius);
        HexagonTile[] initialGenerationDiskTiles = new HexagonTile[initialGenerationDiskCoordinates.Length];
        for (uint i = 0; i < initialGenerationDiskCoordinates.Length; i++)
        {
            initialGenerationDiskTiles[i] = GetTileAt(initialGenerationDiskCoordinates[i]);
        }
        do
        {
            yield return null;
            hasFinished = true;
            foreach (var tile in initialGenerationDiskTiles)
            {
                if (!tile.IsMeshSetup)
                    hasFinished = false;
            }
        } while (!hasFinished);
        
        InitiateFollow();
        firstGeneration = true;
        FirstGenerationDone?.Invoke(this, EventArgs.Empty);
    }
    
    public IEnumerator GenerateTiles()
    {
        Shader.SetGlobalFloat("_IslandRadius", islandRadius);
        Shader.SetGlobalFloat("_SafeRadius", waveRings[0].ringRadius);
        treeGeneration.SetUp();
        Random.InitState(seed);
        AxialCoordinates[] initialGenerationDiskCoordinates = AxialCoordinates.Spiral(AxialCoordinates.Zero, initialMapRadius+1);
        HexagonTile[] initialGenerationDiskTiles = new HexagonTile[initialGenerationDiskCoordinates.Length];
        initialGenerationDiskTiles[0] = GetTileAt(initialGenerationDiskCoordinates[0]);
        initialGenerationDiskTiles[0].Setup(initialGenerationDiskCoordinates[0], waveRings);
        for (int i = 0; i <= initialMapRadius; i++)
        {
            // Setup all tiles of next ring
            for (int j = 1+3*i*(i+1) ; j < 1+3*(i+1)*(i+2) ; j++)
            {
                initialGenerationDiskTiles[j] = GetTileAt(initialGenerationDiskCoordinates[j]);
                initialGenerationDiskTiles[j].Setup(initialGenerationDiskCoordinates[j], waveRings);
                yield return null;
            }
            //Object Setup all tiles of this ring
            if (i == 0)
            {
                initialGenerationDiskTiles[0].ObjectSetup();
            }
            for (int j = 1+3*(i-1)*i ; j < 1+3*i*(i+1) ; j++)
            {
                initialGenerationDiskTiles[j].ObjectSetup();
                yield return null;
            }
        }

        for (uint i = 0 ; i < 1+3*initialMapRadius*(initialMapRadius+1) ; i++)
        {
            initialGenerationDiskTiles[i].MeshSetup(ref material);
            yield return null;
        }

        var coroutine = WaitEndOfInitialGeneration();
        StartCoroutine(coroutine);
    }

    public void ActivateTileAt(AxialCoordinates position)
    {
        HexagonTile tile = GetTileAt(position);
        if ((!tile.IsMeshSetup) || (!tile.IsMeshUpToDate && !tile.IsActive))
        {
            MeshSetupTileAt(position);
        }
        tile.SetActive(true);
    }

    public void ActivateTileAt(Vector2 position)
    {
        AxialCoordinates tileCoordinates = AxialCoordinates.PlaneToAxial(position, tileScale, tileRadius);
        ActivateTileAt(tileCoordinates);
    }

    public void ActivateTileDisk(AxialCoordinates diskCenter, uint ringRadius)
    {
        ActivateTileAt(diskCenter);
        AxialCoordinates currentCoordinates = diskCenter + AxialCoordinates.DownLeft;
        for (uint radius = 1; radius <= ringRadius; radius++)
        {
            // for each 6 directions
            for (uint i = 0; i < 6; i++)
            {
                // one side has as many hexagons as the radius
                for (uint j = 0; j < radius; j++)
                {
                    ActivateTileAt(currentCoordinates);
                    currentCoordinates += AxialCoordinates.DirectionTab[i];
                }
            }
            // get to the next ring
            currentCoordinates += AxialCoordinates.DownLeft;
        }
    }

    private void InitiateFollow()
    {
        if (ReferenceEquals(followedObject, null)) return;
        followedObjectPosition = AxialCoordinates.PlaneToAxial(followedObject.transform.position, tileScale, tileRadius);
        ActivateTileDisk(followedObjectPosition, tilesAroundObject);
    }

    private void UpdateFollow()
    {
        if (ReferenceEquals(followedObject, null)) return;
        Vector2 position = new Vector2(followedObject.transform.position.x, followedObject.transform.position.z);
        AxialCoordinates newPosition =
            AxialCoordinates.PlaneToAxial(position, tileScale, tileRadius);
        if (newPosition != followedObjectPosition)
        {
            followedObjectPosition = newPosition;
            ActivateTileDisk(followedObjectPosition, tilesAroundObject);
        }
    }

    public void OnButtonGenerate()
    {
        ValidateHeightMapData();
        DestroyTiles();
        StartCoroutine(nameof(GenerateTiles));
    }

    public void OnButtonApplyShader(Shader shader)
    {
        foreach (Transform child in transform)
        {
            MeshRenderer meshRenderer;
            child.TryGetComponent(out meshRenderer);
            if (!ReferenceEquals(meshRenderer, null))
                meshRenderer.material.shader = shader;
        }
    }
}
