﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWireframe : MonoBehaviour
{
    [SerializeField]
    private bool renderWireframe = false;

    private void OnPreRender()
    {
        GL.wireframe = renderWireframe;
    }

    private void OnPostRender()
    {
        GL.wireframe = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
