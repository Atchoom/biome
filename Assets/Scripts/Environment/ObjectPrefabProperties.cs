﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPrefabProperties : MonoBehaviour
{
    public enum BoundariesShape
    {
        Rectangle,
        Circle
    }
    public uint smoothingRadius;
    public bool isSmallObject;
    public BoundariesShape boundariesShape;
    public Vector3 circleCenter;
    public float circleRadius;
    public Vector3 rectPoint1;
    public Vector3 rectPoint2;
    public Color sphereColor = Color.red;
    public Color wireSphereColor = Color.red;

    private void OnDrawGizmosSelected()
    {
        switch (boundariesShape)
        {
            case BoundariesShape.Circle:
                DrawCircle();
                Gizmos.color = sphereColor;
                Gizmos.DrawSphere(transform.position, circleRadius);
                Gizmos.color = wireSphereColor;
                Gizmos.DrawWireSphere(transform.position, circleRadius);
                break;
            case BoundariesShape.Rectangle:
                DrawRectangle();
                break;
        }
    }

    private void DrawCircle()
    {
        Gizmos.color = Color.white;
        float theta = 0;
        float x = circleRadius*Mathf.Cos(theta);
        float z = circleRadius*Mathf.Sin(theta);
        Vector3 pos = circleCenter + new Vector3(x, 0, z);
        Vector3 newPos = pos;
        Vector3 lastPos = pos;
        for(theta = 0.1f ; theta<Mathf.PI*2 ; theta += 0.1f)
        {
             x = circleRadius*Mathf.Cos(theta);
             z = circleRadius*Mathf.Sin(theta);
             newPos = circleCenter + new Vector3(x,0,z);
             Gizmos.DrawLine(pos,newPos);
             pos = newPos;
        }
        Gizmos.DrawLine(pos,lastPos); 
    }

    private void DrawRectangle()
    {
        Gizmos.color = Color.white;
        Vector3 rectPoint3 = new Vector3(rectPoint1.x, 0, rectPoint2.z);
        Vector3 rectPoint4 = new Vector3(rectPoint2.x, 0, rectPoint1.z);
        Gizmos.DrawLine(rectPoint1, rectPoint3);
        Gizmos.DrawLine(rectPoint3, rectPoint2);
        Gizmos.DrawLine(rectPoint2, rectPoint4);
        Gizmos.DrawLine(rectPoint4, rectPoint1);
    }

    public bool IsPointInsideBoundaries(Vector3 point)
    {
        switch (boundariesShape)
        {
            case BoundariesShape.Circle:
                point.y = circleCenter.y;
                return (point - circleCenter).magnitude <= circleRadius;
            case BoundariesShape.Rectangle:
                return (point.x >= Mathf.Min(rectPoint1.x, rectPoint2.x) &&
                        point.x <= Mathf.Max(rectPoint1.x, rectPoint2.x) &&
                        point.z >= Mathf.Min(rectPoint1.z, rectPoint2.z) &&
                        point.z <= Mathf.Max(rectPoint1.z, rectPoint2.z));
            default:
                Debug.LogErrorFormat("No calculation defined for shape {0}", boundariesShape);
                return false;
        }
    }
}
