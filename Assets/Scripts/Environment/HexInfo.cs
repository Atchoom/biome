﻿using UnityEngine;

/// <summary>
/// Yhis data class stores inforamtion about an hexagon :
/// * coordinates
/// * object data
/// * associated vertice
/// </summary>
public class HexInfo
{
    public enum ObjectStatus
    {
        Empty,
        ObjectOnIt,
        SmoothArea
    }
    
    private Vector3 _coordinates;
    private Vector3 _globalCoordinates;
    private AxialCoordinates _globalAxialCoordinates;
    private GameObject _objectOnIt;
    private int _objectAngle;
    public int[] _indiceVertice;

    public int biome { get; set; }
    public ObjectStatus isEmpty { get; set; }

    public GameObject objectOnIt
    {
        get => _objectOnIt;
        set => _objectOnIt = value;
    }

    public int ObjectAngle
    {
        get => _objectAngle;
        set => _objectAngle = value;
    }

    public float Height
    {
        get => _coordinates.y;
        set
        { 
            _coordinates.y = value;
            _globalCoordinates.y = value;
        }
    }

    public Vector3 Coordinates
    {
        get => _coordinates;
        set => _coordinates = value;
    }
    
    public Vector3 GlobalCoordinates
    {
        get => _globalCoordinates;
        set => _globalCoordinates = value;
    }
    
    public AxialCoordinates GlobalAxialCoordinates
    {
        get => _globalAxialCoordinates;
        set => _globalAxialCoordinates = value;
    }

    public HexInfo()
    {
        _coordinates = Vector3.zero;
        _indiceVertice = new int[6];
        isEmpty = ObjectStatus.Empty;
        _objectOnIt = null;
    }
    
    /// <summary>
    /// Changes some of the stored data.
    /// Used during object generation.
    /// </summary>
    /// <param name="hexInfo"></param>
    public void SetInfo(ref HexInfo hexInfo)
    {
        isEmpty = hexInfo.isEmpty;
        objectOnIt = hexInfo.objectOnIt;
        Height = hexInfo.Height;
        _objectAngle = hexInfo.ObjectAngle;
    }
}