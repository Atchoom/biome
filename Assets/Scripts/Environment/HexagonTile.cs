using System;
using System.Collections;
using UnityEngine;
using System.Linq;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

public class HexagonTile : MonoBehaviour
{
    public AxialCoordinates _axialCoordinates;
    private uint _radius;
    private uint _islandRadius;
    private float _mapScale;
    private float _heightMultiplier;
    private WaveRing[] _waveRings;
    private float _tileScale;
    private HexInfo[][] _hexagons;
    private Vector3[] _vertices;
    private int[] _triangles;
    private bool hasBegunSetup = false;
    private bool isSetup = false;
    private bool hasBegunObjectSetup = false;
    private bool isObjectSetup = false;
    private bool hasBegunMeshSetup = false;
    private bool isMeshSetup = false;
    private bool isMeshUpToDate = false;
    public ObjectGeneration treeGeneration;
    private bool isActive = true;
    private Material _material;

    public uint NumberOfHexagons
    {
        get => 1 + 6 * _radius * (_radius + 1) / 2;
    }

    public bool IsSetup
    {
        get => isSetup;
    }

    public bool IsMeshSetup
    {
        get => isMeshSetup;
    }
    
    public bool IsMeshUpToDate
    {
        get => isMeshUpToDate;
    }
    
    public bool IsActive { get => isActive; }

    public uint Radius
    {
        get => _radius;
        set => _radius = value;
    }

    public uint IslandRadius
    {
        get => _islandRadius;
        set => _islandRadius = value;
    }

    public float MapScale
    {
        get => _mapScale;
        set => _mapScale = value;
    }

    public float HeightMultiplier
    {
        get => _heightMultiplier;
        set => _heightMultiplier = value;
    }

    public WaveRing[] WaveRings
    {
        get => _waveRings;
        set => _waveRings = value;
    }

    public float TileScale
    {
        get => _tileScale;
        set => _tileScale = value;
    }

    public bool isShaderSetup { get; set; }

    // private Shader _shader;
    // [SerializeField] private Texture _texture;
    // [SerializeField] private Color _color1;
    // [SerializeField] private Color _colorWater;


    private void Awake()
    {
        HexagonHeightmap heightmap = HexagonHeightmap.GetInstance();
        _radius = heightmap.TileRadius;
        _heightMultiplier = heightmap.HeightMultiplier;
        _islandRadius = heightmap.IslandRadius;
        _mapScale = heightmap.MapScale;
        _tileScale = heightmap.TileScale;
    }
    
    private void Update()
    {
        if (!isActive)
            return;
        HexagonHeightmap heightmap = HexagonHeightmap.GetInstance();
        if (heightmap.FirstGeneration && isSetup)
        {
            if (AxialCoordinates.GetDistance(heightmap.FollowedObjectPosition, _axialCoordinates) >
                (heightmap.TilesAroundObject + 1))
            {
                SetActive(false);
            }
        }
    }

    public void SetActive(bool active)
    {
        if (TryGetComponent(out MeshRenderer meshRenderer))
        {
            meshRenderer.enabled = active;
        }

        if (TryGetComponent(out MeshCollider meshCollider))
        {
            meshCollider.enabled = active;
        }

        if (TryGetComponent(out Rigidbody rigidbody))
        {
            rigidbody.detectCollisions = active;
        }
        
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(active);
        }

        isActive = active;
    }

    /*
     * creates a vertice at the correct position for the current hexagon
     * its height is the mean of the three hexagons sharing this point
     *
     * q, r : axial coordinates of current hexagon
     * point : index of the point being created (0 being the left point, and then turning clockwise)
     * matchHexagonVertices : a list which stores for each Hexagon q,r the index of the vertice in vertices corresponding to point
     * currentVerticeNumber : the index of the vertice about to be created
     */
    private void addVertice(AxialCoordinates coordinates, int point, ref int currentVerticeNumber,
        ref HexagonHeightmap heightmap)
    {
        Vector3 pointPosition = GetLocalHex(coordinates).Coordinates;
        HexInfo downLeftHex = GetLocalHex(coordinates + AxialCoordinates.DownLeft);
        HexInfo upLeftHex = GetLocalHex(coordinates + AxialCoordinates.UpLeft);
        HexInfo upHex = GetLocalHex(coordinates + AxialCoordinates.Up);
        HexInfo upRightHex = GetLocalHex(coordinates + AxialCoordinates.UpRight);
        HexInfo downRightHex = GetLocalHex(coordinates + AxialCoordinates.DownRight);
        HexInfo downHex = GetLocalHex(coordinates + AxialCoordinates.Down);

        switch (point)
        {
            case 0:
                pointPosition.x -= _tileScale;
                // if neighbours hexagon in this tile
                pointPosition.y += downLeftHex.Coordinates.y + upLeftHex.Coordinates.y;
                break;
            case 1:
                pointPosition.x -= _tileScale / 2;
                pointPosition.z += _tileScale;
                pointPosition.y += upLeftHex.Coordinates.y + upHex.Coordinates.y;
                break;
            case 2:
                pointPosition.x += _tileScale / 2;
                pointPosition.z += _tileScale;
                pointPosition.y += upHex.Coordinates.y + upRightHex.Coordinates.y;
                break;
            case 3:
                pointPosition.x += _tileScale;
                pointPosition.y += upRightHex.Coordinates.y + downRightHex.Coordinates.y;
                break;
            case 4:
                pointPosition.x += _tileScale / 2;
                pointPosition.z -= _tileScale;
                pointPosition.y += downRightHex.Coordinates.y + downHex.Coordinates.y;
                break;
            case 5:
                pointPosition.x -= _tileScale / 2;
                pointPosition.z -= _tileScale;
                pointPosition.y += downHex.Coordinates.y + downLeftHex.Coordinates.y;
                break;
        }

        pointPosition.y /= 3;
        _vertices[currentVerticeNumber] = pointPosition;
        GetLocalHex(coordinates)._indiceVertice[point] = currentVerticeNumber;
        currentVerticeNumber++;
    }

    private IEnumerator CoroutineSetup()
    {
        name = HexagonHeightmap.GetChildName(_axialCoordinates);
        _hexagons = new HexInfo[2 * _radius + 1][];
        for (uint q = 0; q < 2 * _radius + 1; q++)
        {
            uint rowHexNumber = (uint) (2 * _radius + 1 - Math.Abs(q - (int) _radius));
            _hexagons[q] = new HexInfo[rowHexNumber];
            for (uint r = 0; r < rowHexNumber; r++)
            {
                _hexagons[q][r] = new HexInfo();
                if (HexagonHeightmap.GetInstance().FirstGeneration)
                    yield return null;
            }
        }
        
        Vector2 tileCenterOffset = _axialCoordinates.GetPlaneCoordinates(_tileScale, _radius);
        float tileCenterOffsetX = tileCenterOffset.x;
        float tileCenterOffsetZ = tileCenterOffset.y;
        transform.position = new Vector3(tileCenterOffsetX, 0, tileCenterOffsetZ);
        tag = "Terrain";

        AxialCoordinates currentHexagon = new AxialCoordinates();
        for (currentHexagon.q = -(int) _radius; currentHexagon.q <= _radius; currentHexagon.q++)
        {
            int rowHexNumber = 2 * (int) _radius + 1 - Math.Abs(currentHexagon.q); // number of hexagons in the row q
            currentHexagon.r = -(int) _radius + Math.Max(0, currentHexagon.q);
            for (int i = 0; i < rowHexNumber; i++)
            {
                SetHeight(currentHexagon, tileCenterOffsetX, tileCenterOffsetZ);
                
                currentHexagon.r++;
            }
        }
        isSetup = true;
        hasBegunSetup = false;
        if (isMeshSetup)
            isMeshUpToDate = false;
    }

    public void Setup(AxialCoordinates tilePosition, WaveRing[] waveRings)
    {
        if (hasBegunSetup)
            return;
        hasBegunSetup = true;
        _axialCoordinates = tilePosition;
        WaveRings = waveRings;

        StartCoroutine(nameof(CoroutineSetup));
    }

    /// <summary>
    /// assigns the points of the four triangles of the hexagon currently made, with vertices being a list of vertex indexes
    /// </summary>
    /// <param name="vertices"></param>
    /// <param name="triangles"></param>
    /// <param name="currentPointNumber"></param>
    private void createTriangles(ref int[] vertices, ref int[] triangles, ref int currentPointNumber)
    {
        //region triangles
        triangles[currentPointNumber + 0] = vertices[1];
        triangles[currentPointNumber + 1] = vertices[5];
        triangles[currentPointNumber + 2] = vertices[0];

        triangles[currentPointNumber + 3] = vertices[1];
        triangles[currentPointNumber + 4] = vertices[4];
        triangles[currentPointNumber + 5] = vertices[5];

        triangles[currentPointNumber + 6] = vertices[1];
        triangles[currentPointNumber + 7] = vertices[2];
        triangles[currentPointNumber + 8] = vertices[4];

        triangles[currentPointNumber + 9] = vertices[2];
        triangles[currentPointNumber + 10] = vertices[3];
        triangles[currentPointNumber + 11] = vertices[4];
        currentPointNumber += 12;
    }

    private void ComputeMesh()
    {
        HexagonHeightmap heightmap = HexagonHeightmap.GetInstance();
        AxialCoordinates currentCoordinates = new AxialCoordinates(0, 0);

        int rowHexNumber;

        _vertices = new Vector3[6 * NumberOfHexagons];
        int currentVerticeNumber = 0;
        _triangles = new int[3 * 4 * NumberOfHexagons];
        int currentPointNumber = 0;

        // first column : no vertice has been created

        // first hexagon :
        currentCoordinates.q = -(int) _radius;
        currentCoordinates.r = Math.Max(0, currentCoordinates.q) - (int) _radius;
        for (int i = 0; i < 6; i++)
            addVertice(currentCoordinates, i, ref currentVerticeNumber, ref heightmap);

        createTriangles(ref GetLocalHex(currentCoordinates)._indiceVertice, ref _triangles, ref currentPointNumber);

        rowHexNumber = 2 * (int) _radius + 1 - Math.Abs(currentCoordinates.q); // number of hexagons in the row q
        for (int i = 1; i < rowHexNumber; i++) // first hexagon already done
        {
            currentCoordinates.r = Math.Max(0, currentCoordinates.q) - (int) _radius + i;
            for (int j = 0; j < 4; j++)
                addVertice(currentCoordinates, j, ref currentVerticeNumber, ref heightmap);
            GetLocalHex(currentCoordinates)._indiceVertice[4]
                = GetLocalHex(currentCoordinates + AxialCoordinates.Down)._indiceVertice[2];
            GetLocalHex(currentCoordinates)._indiceVertice[5]
                = GetLocalHex(currentCoordinates + AxialCoordinates.Down)._indiceVertice[1];
            createTriangles(ref GetLocalHex(currentCoordinates)._indiceVertice, ref _triangles, ref currentPointNumber);
        }

        // all lasting rows

        for (currentCoordinates.q++; currentCoordinates.q <= _radius; currentCoordinates.q++)
        {
            rowHexNumber = 2 * (int) _radius + 1 - Math.Abs(currentCoordinates.q); // number of hexagons in the row q
            // first hexagon of the row
            currentCoordinates.r = Math.Max(0, currentCoordinates.q) - (int) _radius;
            for (int j = 2; j < 5; j++)
                addVertice(currentCoordinates, j, ref currentVerticeNumber, ref heightmap);
            GetLocalHex(currentCoordinates)._indiceVertice[0]
                = GetLocalHex(currentCoordinates + AxialCoordinates.UpLeft)._indiceVertice[4]; // 0
            GetLocalHex(currentCoordinates)._indiceVertice[1]
                = GetLocalHex(currentCoordinates + AxialCoordinates.UpLeft)._indiceVertice[3]; // 0

            if (currentCoordinates.q <= 0) // we must create the point 5
            {
                addVertice(currentCoordinates, 5, ref currentVerticeNumber, ref heightmap);
            }
            else // the point 5 has already been created
            {
                GetLocalHex(currentCoordinates)._indiceVertice[5]
                    = GetLocalHex(currentCoordinates + AxialCoordinates.DownLeft)._indiceVertice[3];
            }

            createTriangles(ref GetLocalHex(currentCoordinates)._indiceVertice, ref _triangles, ref currentPointNumber);

            for (int i = 1; i < rowHexNumber; i++) // the first hexagon is done
            {
                currentCoordinates.r = Math.Max(0, currentCoordinates.q) - (int) _radius + i;
                for (int j = 2; j < 4; j++)
                    addVertice(currentCoordinates, j, ref currentVerticeNumber, ref heightmap);

                GetLocalHex(currentCoordinates)._indiceVertice[0]
                    = GetLocalHex(currentCoordinates + AxialCoordinates.DownLeft)._indiceVertice[2];
                GetLocalHex(currentCoordinates)._indiceVertice[4]
                    = GetLocalHex(currentCoordinates + AxialCoordinates.Down)._indiceVertice[2];
                GetLocalHex(currentCoordinates)._indiceVertice[5]
                    = GetLocalHex(currentCoordinates + AxialCoordinates.Down)._indiceVertice[1];
                if (currentCoordinates.q <= 0) // we must create point 1
                {
                    addVertice(currentCoordinates, 1, ref currentVerticeNumber, ref heightmap);
                }
                else
                {
                    GetLocalHex(currentCoordinates)._indiceVertice[1]
                        = GetLocalHex(currentCoordinates + AxialCoordinates.UpLeft)._indiceVertice[3];
                }

                createTriangles(ref GetLocalHex(currentCoordinates)._indiceVertice, ref _triangles,
                    ref currentPointNumber);
            }
        }
        isMeshUpToDate = true;
    }

    private void AdaptMesh()
    {
        HexagonHeightmap heightmap = HexagonHeightmap.GetInstance();
        bool hadToAdapt = false;
        for (uint dir = 0; dir < 6; dir++)
        {
            HexagonTile neighbour = heightmap.GetTileAt(_axialCoordinates + AxialCoordinates.DirectionTab[dir]);
            if (neighbour.IsMeshSetup && !neighbour.IsMeshUpToDate)
            {
                if (!neighbour.isActive)
                {
                    neighbour.MeshSetup(ref _material);
                }
                else
                {
                    // we have to adapt our mesh to this neighbour
                    // for each Hexagon on the border
                    AxialCoordinates currentHexagon = AxialCoordinates.DirectionTab[dir] * _radius;
                    AxialCoordinates incrementDirection = AxialCoordinates.DirectionTab[AxialCoordinates.ModulusDirection((int)dir-2)];
                    for (uint i = 0 ; i <= _radius ; i++)
                    {
                        uint vertex1Our = AxialCoordinates.ModulusDirection(3 - (int)dir);
                        uint vertex2Our = AxialCoordinates.ModulusDirection(4 - (int)dir);
                        uint vertex1Their = AxialCoordinates.ModulusDirection(1 - (int)dir);
                        uint vertex2Their = AxialCoordinates.ModulusDirection(0 - (int)dir);

                        _vertices[GetLocalHex(currentHexagon)._indiceVertice[vertex1Our]].y = neighbour._vertices[
                            neighbour.GetGlobalHex(currentHexagon + _axialCoordinates.GetTileCenter(_radius) + AxialCoordinates.DirectionTab[dir])
                                ._indiceVertice[vertex1Their]].y;
                        _vertices[GetLocalHex(currentHexagon)._indiceVertice[vertex2Our]].y = neighbour._vertices[
                            neighbour.GetGlobalHex(currentHexagon + _axialCoordinates.GetTileCenter(_radius) + AxialCoordinates.DirectionTab[dir])
                                ._indiceVertice[vertex2Their]].y;
                        currentHexagon += incrementDirection;
                    }
                    hadToAdapt = true;
                }
            }
        }

        isMeshUpToDate = !hadToAdapt;
    }

    private void ApplyMesh()
    {
        //add a mesh filter to the GO the script is attached to; cache it for later
        MeshFilter meshFilter;
        if (!gameObject.TryGetComponent(out meshFilter))
            meshFilter = gameObject.AddComponent<MeshFilter>();
        //add a mesh renderer to the GO the script is attached to; cache it for later
        MeshRenderer meshRenderer;
        if (!gameObject.TryGetComponent(out meshRenderer))
            meshRenderer = gameObject.AddComponent<MeshRenderer>();
        //add a mesh collider to the GO the script is attached to
        MeshCollider meshCollider;
        if (!gameObject.TryGetComponent(out meshCollider))
            meshCollider = gameObject.AddComponent<MeshCollider>();
        //add a rigidbody to the GO the script is attached to
        Rigidbody rigidbody;
        if (!gameObject.TryGetComponent(out rigidbody))
            rigidbody = gameObject.AddComponent<Rigidbody>();

        //create a mesh object to pass our data into
        Mesh mesh = new Mesh();

        //add our vertices to the mesh
        mesh.vertices = _vertices.ToArray();
        //add our triangles to the mesh
        mesh.triangles = _triangles.ToArray();

        int mapDimension = _vertices.Length;

        // color map
        // Color[] colorMap = new Color[mapDimension];
        // for (int i = 0; i < mapDimension; i++)
        // {
        //     float distanceFromOrigin = new Vector2(tileCenterOffsetX + _vertices[i].x, tileCenterOffsetZ + _vertices[i].z).magnitude;
        //     
        //     foreach (var ring in _waveRings)
        //     {
        //         if (distanceFromOrigin < ring.ringRadius)
        //         {
        //             colorMap[i] = ring.color;
        //             break;
        //         }
        //     }
        // }
        // mesh.colors = colorMap;
        
        // test color for generation object
        Color[] colorMap = new Color[mapDimension];
        for (int i = 0; i < mapDimension; i++)
        {
            colorMap[i] = Color.green;
        }

        foreach (var h in _hexagons)
        {
            foreach (var hexInfo in h)
            {
                if (hexInfo.isEmpty == HexInfo.ObjectStatus.ObjectOnIt)
                {
                    foreach (var i in hexInfo._indiceVertice)
                    {
                        colorMap[i] = Color.red;
                    }
                }
                else if (hexInfo.isEmpty == HexInfo.ObjectStatus.SmoothArea)
                {
                    foreach (var i in hexInfo._indiceVertice)
                    {
                        if (colorMap[i] == Color.green)
                            colorMap[i] = Color.blue;
                    }
                }
            }
        }

        mesh.colors = colorMap;
        

        //make it play nicely with lighting
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.Optimize();


        //set the GO's meshes to be the one we just made
        meshFilter.mesh = mesh;
        meshCollider.sharedMesh = mesh;

        
        // set the right shader according to which biome is on the tile
        if (!isShaderSetup)
        {
            meshRenderer.material = _material;
            
            
            int[] biomeNumbers = new int[6];

            for (int i = 0; i < 6; i++)
            {
                biomeNumbers[i] = GetLocalHex(AxialCoordinates.DirectionTab[i] * _radius).biome;
            }

            int firstNumber = Mathf.Min(biomeNumbers);
            int lastNumber = Mathf.Max(biomeNumbers);

            Debug.Assert(lastNumber - firstNumber <= 1, "A tile is on three biomes");
            
            String shaderNumber = firstNumber + "_" + lastNumber;
            float transitionRadius = _waveRings[firstNumber%_waveRings.Length].ringRadius;

            meshRenderer.material.shader = Resources.Load<Shader>("Shaders/Terrain_" + shaderNumber);
            meshRenderer.material.SetFloat("_TransitionRadius", transitionRadius);
            meshRenderer.material.SetFloat("_TransitionAttenuation", 0.1f);
            isShaderSetup = true;
        }

        rigidbody.useGravity = false;
        rigidbody.isKinematic = true;
    }

    private IEnumerator CoroutineMeshSetup()
    {
        // wait for ObjectSetup to end
        while (!isObjectSetup) yield return null;
        
        // set vertices and triangles
        ComputeMesh();
        AdaptMesh();
        ApplyMesh();
        //spawn objects
        ObjectGeneration();

        isMeshSetup = true;
        hasBegunMeshSetup = false;
    }
    
    // neighbours have to be all surrounding tiles from the up one, clockwise
    public void MeshSetup(ref Material material)
    {
        if (hasBegunMeshSetup)
            return;
        hasBegunMeshSetup = true;
        _material = material;
        StartCoroutine(nameof(CoroutineMeshSetup));
    }

    /// <summary>
    /// Moves through the haxagons and call objectGeneration.Generate to potentially place objets
    /// </summary>
    private IEnumerator CoroutineObjectSetup()
    {
        HexagonHeightmap heightmap = HexagonHeightmap.GetInstance();
        AxialCoordinates[] neighbourDiskCoordinates = AxialCoordinates.Spiral(_axialCoordinates, 1);
        HexagonTile[] neighbourDisk = new HexagonTile[neighbourDiskCoordinates.Length];
        for (uint i = 0; i < neighbourDiskCoordinates.Length; i++)
            neighbourDisk[i] = heightmap.GetTileAt(neighbourDiskCoordinates[i]);
        bool everythingSetup = false;
        do
        {
            everythingSetup = true;
            for (uint i = 0; i < neighbourDiskCoordinates.Length; i++)
                if (!neighbourDisk[i].isSetup)
                    everythingSetup = false;
            yield return null;
        } while (!everythingSetup);        
        
        GameObject thisGameObject = gameObject;
        
        for (uint i = 0; i < _hexagons.Length; ++i)
        {
            for (uint j = 0; j < _hexagons[i].Length; ++j)
            {
                treeGeneration.Generate(_hexagons[i][j], ref thisGameObject, _tileScale);
            }
        }
        isObjectSetup = true;
        hasBegunObjectSetup = false;
    }
    
    /// <summary>
    /// Launches the object setup coroutine
    /// </summary>
    public void ObjectSetup()
    {
        if (hasBegunObjectSetup)
            return;
        hasBegunObjectSetup = true;
        StartCoroutine(nameof(CoroutineObjectSetup));
    }

    /// <summary>
    /// Move through the hexagons and instanciate the stored objects
    /// </summary>
    public void ObjectGeneration()
    {
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
        for (uint i = 0; i < _hexagons.Length; ++i)
        {
            for (uint j = 0; j < _hexagons[i].Length; ++j)
            {
                if (_hexagons[i][j].objectOnIt != null)
                {
                    float angle = Random.Range(0f, 360f);
                    float scale = Random.Range(0.8f, 1.2f);
                    float randomRange = _tileScale / 3f;
                    Vector3 position = _hexagons[i][j].GlobalCoordinates;
                    Vector3 pose = new Vector3(position.x + Random.Range(-randomRange,randomRange), position.y-0.1f, position.z + Random.Range(-randomRange,randomRange));
                    GameObject gameObjectI = Instantiate(_hexagons[i][j].objectOnIt, pose, Quaternion.identity, transform);
                    gameObjectI.transform.Rotate(Vector3.up, _hexagons[i][j].ObjectAngle);
                    // gameObjectI.transform.localScale *= scale;
                }
            }
        }
    }

    public HexInfo GetLocalHex(AxialCoordinates coordinates)
    {
        if (coordinates.GetAxialRadius() <= _radius)
        {
            return _hexagons[coordinates.q + _radius][coordinates.r + _radius - Math.Max(0, coordinates.q)];
        }

        HexagonHeightmap heightmap = HexagonHeightmap.GetInstance();
        AxialCoordinates hexagonGlobalCoordinates = _axialCoordinates.GetTileCenter(_radius) + coordinates;
        for (uint i = 0; i < 6; i++)
        {
            AxialCoordinates neighbourTile,hexagonNeighbourLocalCoordinates;
            neighbourTile = _axialCoordinates + AxialCoordinates.DirectionTab[i];
            hexagonNeighbourLocalCoordinates = hexagonGlobalCoordinates - neighbourTile.GetTileCenter(_radius);
            if (hexagonNeighbourLocalCoordinates.GetAxialRadius() <= _radius)
            {
                return heightmap.GetInfoAt(neighbourTile, hexagonNeighbourLocalCoordinates);
            }
        }
        Debug.LogWarning("GetLocalHex failed");
        return new HexInfo();
    }

    public HexInfo GetGlobalHex(AxialCoordinates coordinates)
    {
        return GetLocalHex(coordinates - _axialCoordinates.GetTileCenter(_radius));
    }
    
    public void SetLocalHex(AxialCoordinates coordinates, ref HexInfo hexInfo)
    {
        HexagonHeightmap heightmap = HexagonHeightmap.GetInstance();
        if (coordinates.GetAxialRadius() <= _radius)
        {
            _hexagons[coordinates.q + _radius][coordinates.r + _radius - Math.Max(0, coordinates.q)]
                .SetInfo(ref hexInfo);
            isMeshUpToDate = false;
            if (coordinates.GetAxialRadius() == _radius)
            {
                // we have to tell the neighbour tile(s) the mesh is not up to date anymore
                if (coordinates.q == _radius)
                    heightmap.GetTileAt(_axialCoordinates + AxialCoordinates.UpRight).isMeshUpToDate = false;
                else if (coordinates.q == -(int)_radius)
                    heightmap.GetTileAt(_axialCoordinates + AxialCoordinates.DownLeft).isMeshUpToDate = false;
                if (coordinates.r == _radius)
                    heightmap.GetTileAt(_axialCoordinates + AxialCoordinates.Up).isMeshUpToDate = false;
                else if (coordinates.r == -(int)_radius)
                    heightmap.GetTileAt(_axialCoordinates + AxialCoordinates.Down).isMeshUpToDate = false;
                if (coordinates.s == _radius)
                    heightmap.GetTileAt(_axialCoordinates + AxialCoordinates.UpLeft).isMeshUpToDate = false;
                else if (coordinates.s == -(int)_radius)
                    heightmap.GetTileAt(_axialCoordinates + AxialCoordinates.DownRight).isMeshUpToDate = false;
            }
            return;
        }

        AxialCoordinates hexagonGlobalCoordinates = _axialCoordinates.GetTileCenter(_radius) + coordinates;
        for (uint i = 0; i < 6; i++)
        {
            AxialCoordinates neighbourTile, hexagonNeighbourLocalCoordinates;
            neighbourTile = _axialCoordinates + AxialCoordinates.DirectionTab[i];
            hexagonNeighbourLocalCoordinates = hexagonGlobalCoordinates - neighbourTile.GetTileCenter(_radius);
            if (hexagonNeighbourLocalCoordinates.GetAxialRadius() <= _radius)
            {
                heightmap.SetInfoAt(neighbourTile, hexagonNeighbourLocalCoordinates, ref hexInfo);
                return;
            }
        }
        
        Debug.LogWarning("setLocalHex failed");
    }

    public void SetGlobalHex(AxialCoordinates coordinates, ref HexInfo hexInfo)
    {
        SetLocalHex(coordinates - _axialCoordinates.GetTileCenter(_radius), ref hexInfo);
    }


    private void SetHeight(AxialCoordinates currentHexagon, float tileCenterOffsetX, float tileCenterOffsetZ)
    {
        Vector2 position = AxialCoordinates.AxialToPlane(currentHexagon, _tileScale);
        AxialCoordinates coordinates = currentHexagon + _axialCoordinates.GetTileCenter(_radius);
        // float height = SetHeight(globalHexagon);
        
        
        
        float distanceFromOrigin = coordinates.GetPlaneRadius(_tileScale);

        if (distanceFromOrigin > _islandRadius)
        {
            GetLocalHex(currentHexagon).Coordinates = new Vector3(position.x, 0, position.y);
            GetLocalHex(currentHexagon).GlobalCoordinates = new Vector3(position.x + tileCenterOffsetX, 0,
                position.y + tileCenterOffsetZ);
            GetLocalHex(currentHexagon).GlobalAxialCoordinates = coordinates;
            GetLocalHex(currentHexagon).biome = _waveRings.Length;
            return;
        }


        float factor = 1f;
        if (distanceFromOrigin > _islandRadius - _waveRings[_waveRings.Length - 1].ringWidth)
        {
            factor = 1 - ((distanceFromOrigin - (_islandRadius - _waveRings[_waveRings.Length - 1].ringWidth)) /
                          _waveRings[_waveRings.Length - 1].ringWidth);
        }

        uint currentWave = 0;
        while (distanceFromOrigin > _waveRings[currentWave].ringRadius &&
               _waveRings[currentWave].ringRadius < _islandRadius)
            currentWave++;

        // the total range of smoothing of the ring (one half at the beginning, one half at the end
        float meanderingRange = _waveRings[currentWave].waveSmoothingRatio * _waveRings[currentWave].ringWidth;

        float sampleX = coordinates.q / _mapScale;
        float sampleZ = coordinates.r / _mapScale;

        float noise = 0f;
        float normalization = 0f;

        foreach (Wave wave in _waveRings[currentWave].waves)
        {
            // generate noise value using PerlinNoise for a given Wave
            noise += wave.Amplitude *
                     Mathf.PerlinNoise(sampleX * wave.Frequency + wave.Seed, sampleZ * wave.Frequency + wave.Seed);
            normalization += wave.Amplitude;
        }

        // if the hexagon is close to the outer border of the ring, we smooth with the next ring
        if (currentWave < _waveRings.Length - 1 && _waveRings[currentWave].ringRadius - distanceFromOrigin > 0 &&
            _waveRings[currentWave].ringRadius - distanceFromOrigin <= meanderingRange / 2)
        {
            // this factor is at 0 on ringRadius - minderingRange/2 and at 1 on ringRadius (which means the next ring has the same importance as the current one)
            float smoothFactor = 1 - ((_waveRings[currentWave].ringRadius - distanceFromOrigin) /
                                      _waveRings[currentWave].ringRadius) * 2 / meanderingRange;
            foreach (Wave wave in _waveRings[currentWave + 1].waves)
            {
                // generate noise value using PerlinNoise for a given Wave
                noise += smoothFactor * wave.Amplitude * Mathf.PerlinNoise(sampleX * wave.Frequency + wave.Seed,
                             sampleZ * wave.Frequency + wave.Seed);
                normalization += smoothFactor * wave.Amplitude;
            }
        }
        // if it is close to the inner part, we smooth with the previous ring
        else if (currentWave > 0 && distanceFromOrigin - _waveRings[currentWave - 1].ringRadius > 0 &&
                 distanceFromOrigin - _waveRings[currentWave - 1].ringRadius <= meanderingRange / 2)
        {
            float smoothFactor = 1 - ((distanceFromOrigin - _waveRings[currentWave].ringRadius) /
                                      distanceFromOrigin) * 2 / meanderingRange;
            foreach (Wave wave in _waveRings[currentWave - 1].waves)
            {
                // generate noise value using PerlinNoise for a given Wave
                noise += smoothFactor * wave.Amplitude * Mathf.PerlinNoise(sampleX * wave.Frequency + wave.Seed,
                             sampleZ * wave.Frequency + wave.Seed);
                normalization += smoothFactor * wave.Amplitude;
            }
        }

        // normalize the noise value so that it is within 0 and 1
        noise /= normalization;

        float height = (noise * _heightMultiplier * factor) + GetHeightModifier(distanceFromOrigin);
        
        GetLocalHex(currentHexagon).Coordinates = new Vector3(position.x, height, position.y);
        GetLocalHex(currentHexagon).GlobalCoordinates = new Vector3(position.x + tileCenterOffsetX, height,
            position.y + tileCenterOffsetZ);
        GetLocalHex(currentHexagon).GlobalAxialCoordinates = coordinates;
        GetLocalHex(currentHexagon).biome = (int)currentWave;
    }

    private float GetLinearAtPoint(Vector2 originPoint, Vector2 targetPoint, float pointX)
    {
        float a = (targetPoint.y - originPoint.y) / (targetPoint.x - originPoint.x);
        float b = targetPoint.y - a * targetPoint.x;
        float restult = a * pointX + b;
        return a * pointX + b;
    }

    private float GetHeightModifier(float distanceFromOrigin)
    {
        uint currentWave = 0;
        while (_waveRings[currentWave].ringRadius < _islandRadius &&
               distanceFromOrigin > _waveRings[currentWave].ringRadius) currentWave++;
        float meanderingRange = _waveRings[currentWave].depthSmoothingRatio * _waveRings[currentWave].ringWidth;
//        float a = 0;
//        float b = _waveRings[currentWave].depth;
        // the last ring has to end above the water and is the shore
        if (currentWave == _waveRings.Length - 1)
        {
            float shoreLength = _islandRadius - _waveRings[currentWave - 1].ringRadius;
            if (distanceFromOrigin > _islandRadius - shoreLength / 3)
            {
                // beach descending towards water
//                a = -3 * _waveRings[currentWave].depth / shoreLength;
//                b = -a * _islandRadius;
                return GetLinearAtPoint(new Vector2(_islandRadius - shoreLength / 3, _waveRings[currentWave].height),
                    new Vector2(_islandRadius, 0), distanceFromOrigin);
            }
            else if (distanceFromOrigin > _islandRadius - 2 * shoreLength / 3)
            {
                // small flat land
//                a = 0;
//                b = _waveRings[currentWave].depth;
                return GetLinearAtPoint(
                    new Vector2(_islandRadius - 2 * shoreLength / 3, _waveRings[currentWave].height),
                    new Vector2(_islandRadius - shoreLength / 3, _waveRings[currentWave].height), distanceFromOrigin);
            }
            else
            {
                // transition from last ring to shore height
//                a = 3 * (_waveRings[currentWave].depth - _waveRings[currentWave - 1].depth) / (2 * shoreLength);
//                b = (_waveRings[currentWave].depth + _waveRings[currentWave - 1].depth) / 2 -
//                    a * _waveRings[currentWave-1].ringRadius;
                return GetLinearAtPoint(
                    new Vector2(_waveRings[currentWave - 1].ringRadius,
                        (_waveRings[currentWave].height + _waveRings[currentWave - 1].height) / 2),
                    new Vector2(_waveRings[currentWave - 1].ringRadius + shoreLength / 3,
                        _waveRings[currentWave].height), distanceFromOrigin);
            }
        }
        else if (_waveRings[currentWave].ringRadius - distanceFromOrigin >= 0 &&
                 _waveRings[currentWave].ringRadius - distanceFromOrigin <= meanderingRange / 2)
        {
//            a = (_waveRings[currentWave + 1].depth - _waveRings[currentWave].depth) / meanderingRange;
//            b = (_waveRings[currentWave + 1].depth + _waveRings[currentWave].depth) / 2 -
//                a * _waveRings[currentWave].ringRadius;
            return GetLinearAtPoint(
                new Vector2(_waveRings[currentWave].ringRadius - meanderingRange / 2, _waveRings[currentWave].height),
                new Vector2(_waveRings[currentWave].ringRadius,
                    (_waveRings[currentWave].height + _waveRings[currentWave + 1].height) / 2), distanceFromOrigin);
        }
        else if (currentWave > 0 && distanceFromOrigin - _waveRings[currentWave - 1].ringRadius >= 0 &&
                 distanceFromOrigin - _waveRings[currentWave - 1].ringRadius <= meanderingRange / 2)
        {
//            a = (_waveRings[currentWave].depth - _waveRings[currentWave-1].depth) / meanderingRange;
//            b = (_waveRings[currentWave].depth + _waveRings[currentWave-1].depth) / 2 -
//                a * _waveRings[currentWave-1].ringRadius;
            return GetLinearAtPoint(
                new Vector2(_waveRings[currentWave - 1].ringRadius,
                    (_waveRings[currentWave - 1].height + _waveRings[currentWave].height) / 2),
                new Vector2(_waveRings[currentWave - 1].ringRadius + meanderingRange / 2,
                    _waveRings[currentWave].height), distanceFromOrigin);
        }

        return _waveRings[currentWave].height;
    }
    
    public void MakeFlat()
    {
        for (uint q = 0; q < 2 * _radius + 1; q++)
        {
            uint rowHexNumber = (uint) (2 * _radius + 1 - Math.Abs(q - (int) _radius));
            for (uint r = 0; r < rowHexNumber; r++)
            {
                HexInfo info = _hexagons[q][r];
                info.Height = 0;
                SetGlobalHex(info.GlobalAxialCoordinates, ref info);
            }
        }

        isMeshUpToDate = false;
        for (int i = 0; i < 6; i++)
        {
            var tile = HexagonHeightmap.GetInstance().GetTileAt(_axialCoordinates+AxialCoordinates.DirectionTab[i]);
            tile.isMeshUpToDate = false;
        }
    }

    public void ElevateTileCenter()
    {
        HexInfo nul = GetLocalHex(AxialCoordinates.Zero);
        nul.Height += 10f;
        SetLocalHex(AxialCoordinates.Zero, ref nul);
    }
    
    public void ElevateTileBorder()
    {
        uint i = 5;
        AxialCoordinates coord = AxialCoordinates.DirectionTab[i]*_radius;
        HexInfo nul = GetLocalHex(coord);
        nul.Height += 10f;
        SetLocalHex(coord, ref nul);
    }

    public void TemporaryElevateTileCenter()
    {
        for (uint i = 0; i < 6; i++)
        {
            _vertices[_hexagons[_radius][_radius]._indiceVertice[i]].y += 1;
        }
        ApplyMesh();
        isMeshUpToDate = false;
    }
}