﻿using System;
using UnityEngine;

public class Heightmap
{
    private uint _radius;
    private float _mapScale;
    private float _heightMultiplier;
    private Wave[] _waves;

    public uint Radius
    {
        get => _radius;
        set => _radius = value;
    }

    public float MapScale
    {
        get => _mapScale;
        set => _mapScale = value;
    }

    public float HeightMultiplier
    {
        get => _heightMultiplier;
        set => _heightMultiplier = value;
    }

    public Wave[] Waves
    {
        get => _waves;
        set => _waves = value;
    }
    
    public float GetHeight(int q, int r)
    {
        float sampleX = q / _mapScale;
        float sampleZ = r / _mapScale;
                
        float noise = 0f;
        float normalization = 0f;
                
        foreach (Wave wave in _waves) {
            // generate noise value using PerlinNoise for a given Wave
            noise += wave.Amplitude * Mathf.PerlinNoise (sampleX * wave.Frequency + wave.Seed, sampleZ * wave.Frequency + wave.Seed);
            normalization += wave.Amplitude;
        }
        // normalize the noise value so that it is within 0 and 1
        noise /= normalization;

        return noise * _heightMultiplier;
    }
}
