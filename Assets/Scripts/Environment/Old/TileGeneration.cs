﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGeneration : MonoBehaviour
{
    [SerializeField] private MeshRenderer tileRenderer;
 
    [SerializeField] private MeshFilter meshFilter;
 
    [SerializeField] private MeshCollider meshCollider;
 
    [SerializeField] private float mapScale;
    
    [SerializeField] private float mapRadius;
    
    [SerializeField] private float heightMultiplier;
    
    [SerializeField] private int numberOfBiome;
    
    [SerializeField] private Color waterColor;

    [System.Serializable] public class Wave
    {
        public float seed;
        public float frequency;
        public float amplitude;
    }
    
    [SerializeField] private Wave[] waves;

    [System.Serializable] public class Biome
    {
        public string name;
        public Color color;
    }

    [SerializeField] private Biome[] biomes;
    
    void Start() {
        GenerateTile ();
    }
 
    void GenerateTile() {
        // calculate tile depth and width based on the mesh vertices
        Vector3[] meshVertices = this.meshFilter.mesh.vertices;
        int mapDepth = (int)Mathf.Sqrt (meshVertices.Length);
        int mapWidth = mapDepth;
 
        // calculate the offsets based on the tile position
        float offsetX = -this.gameObject.transform.position.x;
        float offsetZ = -this.gameObject.transform.position.z;
        
        
        Color[] colorMap = new Color[mapDepth * mapWidth];

        int vertexIndex = 0;
        for (int zIndex = 0; zIndex < mapDepth; zIndex ++) {
            for (int xIndex = 0; xIndex < mapWidth; xIndex++) {
                // calculate sample indices based on the coordinates and the scale
                float sampleX = (xIndex + offsetX) / mapScale;
                float sampleZ = (zIndex + offsetZ) / mapScale;


                float norm = Mathf.Sqrt(Mathf.Pow(sampleX, 2) + Mathf.Pow(sampleZ, 2) );

                float noise = 0f;
                float normalization = 0f;
                
                foreach (Wave wave in waves) {
                    // generate noise value using PerlinNoise for a given Wave
                    noise += wave.amplitude * Mathf.PerlinNoise (sampleX * wave.frequency + wave.seed, sampleZ * wave.frequency + wave.seed);
                    normalization += wave.amplitude;
                }
                // normalize the noise value so that it is within 0 and 1
                noise /= normalization;
                

                if (norm >= mapRadius)
                    noise *= 0.5f;

                int colorIndex = zIndex * mapWidth + xIndex;
                float temp = norm / (mapRadius / numberOfBiome);
                int index = Mathf.FloorToInt(temp) % biomes.Length;
                // assign as color
                colorMap [colorIndex] = norm < mapRadius ? biomes[index].color : waterColor;
                
                
                Vector3 vertex = meshVertices [vertexIndex];
                // change the vertex Y coordinate, proportional to the height value
                meshVertices[vertexIndex] = new Vector3(vertex.x, noise* this.heightMultiplier, vertex.z);
 
                vertexIndex++;
            }
        }

        // create a new texture and set its pixel colors
//        Texture2D tileTexture = new Texture2D (mapWidth, mapDepth);
//        tileTexture.wrapMode = TextureWrapMode.Clamp;
//        tileTexture.SetPixels (colorMap);
//        tileTexture.Apply ();
//        this.tileRenderer.material.mainTexture = tileTexture;
        meshFilter.mesh.colors = colorMap;

        // update the vertices in the mesh and update its properties
        this.meshFilter.mesh.vertices = meshVertices;
        this.meshFilter.mesh.RecalculateBounds ();
        this.meshFilter.mesh.RecalculateNormals ();
        // update the mesh collider
        this.meshCollider.sharedMesh = this.meshFilter.mesh;
    }
}
