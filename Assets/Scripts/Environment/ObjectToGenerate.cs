﻿using System;
using UnityEngine;

/// <summary>
/// This data class is used to store inforamtion about object which can be placed during the generation
/// </summary>
[Serializable]
public class ObjectToGenerate
{
    public GameObject Object;
    public uint LookingRadius;
    public uint ProbabilityWeight;
}
