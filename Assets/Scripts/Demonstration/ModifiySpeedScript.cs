﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifiySpeedScript : MonoBehaviour
{
    public GameObject Skeleton;
    public float new_cd;
    public float old_cd;
    public float new_speed;
    public float old_speed;
    public KeyCode plus;
    public KeyCode minus;

    private EnemyController enemy;

    private void Start()
    {
        enemy = Skeleton.GetComponent<EnemyController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(plus))
        {
            enemy.AttackSpeed = new_speed;
            enemy.AttackCooldown = new_cd;
        }
        if (Input.GetKeyDown(minus))
        {
            enemy.AttackSpeed = old_speed;
            enemy.AttackCooldown = old_cd;
        }
    }
}
