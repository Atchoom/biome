﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyForVideo : MonoBehaviour
{
    public KeyCode spawnEnemy;
    public GameObject en;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(spawnEnemy))
        {
            en.SetActive(!en.activeSelf);
        }
    }
}
