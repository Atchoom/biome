﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoScript : MonoBehaviour
{
    public GameObject enemyPrefab;
    public Transform player;
    public KeyCode spawnKey;
    public KeyCode moveKey;
    public KeyCode stopMobSpawn;

    EnemyController controller;
    void Update()
    {
        if(Input.GetKeyDown(spawnKey))
        {
            Vector3 enPos;
            enPos = player.position + new Vector3(0, 10, 5);
            if(controller != null)
            {
                Destroy(controller.gameObject);
            }
            GameObject res = Instantiate(enemyPrefab);
            res.transform.position = enPos;
            controller = res.GetComponent<EnemyController>();
            controller.CurrentMoveActionIndex = 3;
        }

        if (Input.GetKeyDown(moveKey))
        {
            if(controller != null)
            {
                if (controller.CurrentMoveActionIndex == 3)
                    controller.CurrentMoveActionIndex = 0;
                else
                    controller.CurrentMoveActionIndex = 3;
                controller.SendEvent();
            }
        }

        if(Input.GetKeyDown(stopMobSpawn))
        {
            EnemySpawnManager spawn = GameManager.Instance.GetComponent<EnemySpawnManager>();
            if (spawn.numberOfEnemyAroundPlayer != 0)
                spawn.numberOfEnemyAroundPlayer = 0;
            else
                spawn.numberOfEnemyAroundPlayer = 5;
        }
    }
}
