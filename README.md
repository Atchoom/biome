# Biome

A school project done during a semester.

## Objective

The initial idea for this project was to develop a 3D rogue-like game, where the users play a lonely swordman which explores a world divided in concentric ring shaped areas called biomes, facing monsters with their gigantic sword.
As the development went on, we decided to focus more on technical parts than design parts, accordingly to our formation and the time we had.
The goal became to produce a technical demonstration, with some serious gameplay basis, that could then be adapted into a real game.

## Work achieved

### Gameplay

The player has a basic moveset :
- move
- attack
- dodge

The sword can be dropped, preventing the player from attacking, but increasing the movement and dodge speed.
When attacking, the player sticks the sword into the ground, and has to wait some time to recover it.

### Enemies

Enemies are handled depending on the biome they spawn in. The enemy manager creates generation of enemies, which collect information about their interaction with the player. When they disappear, they send the information to the manager, which then adapts some parameters accordingly.
The parameters that can be modified are :
- move speed
- damage
- max health

These parameters are modified in order to adapt the difficulty to the player, making it harder or easier depending on the data collected.

### Terrain

The terrain is created using a heightmap. The heigth are calculated using several perlin noises with different scales and offsets. Then, depending on the biome, the medium heigth is adjusted, in order to create a curved terrain.
The height data is then used to create the actual terrain, which is composed of several hexagon tiles.
The terrain can then be changed during the game, making it possible to adapt the terrain to the player's behaviour.

### Visual aspect

The terrain is textured using shaders, which can adapt depending on the sizes of each biome easily.
A fog has been placed to match the ambience we wanted, and to prevent the player from seing too far where the terrain is not loaded.
A minimalist interface has been designed, and a begining of an intradiegetic interface was implemented, the health of the player being represented as the condition of its cloak.
